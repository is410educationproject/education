<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = true;
$page_url = 'view_departments.php?';

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/department.php';
include_once 'objects/user.php';
include_once 'objects/term.php';
include_once 'objects/documentation.php';

// instantiate database and objects
$database = new Database();
$db = $database->getConnection();

$department = new Department($db);
$userobject = new UserObject($db);
$term = new Term($db);

$department->activeuserID = $activeuserID;

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';
$activetermDesc = $userobject->readActiveTerm();

// get documentation
$documentation = new Documentation($page_url);

// header settings
$page_title = "View Departments";
include_once "includes/header.php";

// add the term selector to the page
include_once 'includes/termselect.inc.php';

// query departments
$stmtDepartment = $department->readAll();
$num = $stmtDepartment->rowCount();

include_once "includes/departments_main.inc.php";

include_once "includes/footer.php";
?>