<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$page_url = 'update_contactevent.php?';

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/member.php';
include_once 'objects/contactevent.php';
include_once 'objects/contactmethod.php';
include_once 'objects/status.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';
include_once 'objects/term.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare objects
$member = new Member($db);
$contact = new ContactEvent($db);
$method = new ContactMethod($db);
$status = new Status($db);
$userobject = new UserObject($db);
$term = new Term($db);

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';
$activetermDesc = $userobject->readActiveTerm();

$memberID = isset($_GET['memberID']) ? $_GET['memberID'] : 1;
$contacteventID = isset($_GET['contacteventID']) ? $_GET['contacteventID'] : 1;

// get documentation
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Update Contact Event";
include_once "includes/header.php";

include_once "includes/contactevents_update.inc.php";

include_once "includes/footer.php";
?>