<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$globalpage = true;
$page_url = "view_familypositions.php?";

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/familyposition.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';
include_once 'objects/term.php';

// instantiate database and familyposition.php object
$database = new Database();
$db = $database->getConnection();

$familyposition = new FamilyPosition($db);
$userobject = new UserObject($db);
$term = new Term($db);

// get documentation
$documentation = new Documentation($page_url);

// header settings
$page_title = "View Family Positions";
include_once "includes/header_param.php";

// query familypositions
$stmt = $familyposition->readAll();
$num = $stmt->rowCount();

include_once "includes/familypositions_main.inc.php";

include_once "includes/footer.php";
?>