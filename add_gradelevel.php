<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$page_url = 'add_gradelevel.php?';

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/gradelevel.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';
include_once 'objects/term.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate gradelevel.php object
$gradelevel = new gradelevel($db);
$userobject = new UserObject($db);
$term = new Term($db);

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';

//get documentation 
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Add Grade Level";
include_once "includes/header_param.php";

include_once "includes/gradelevels_add.inc.php";

include_once "includes/footer.php";
?>