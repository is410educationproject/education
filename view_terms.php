<?php
// check for term
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = true;
$termselector = false;
$globalpage = true;
$page_url = "view_terms.php?";

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/term.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';


// instantiate database and term.php object
$database = new Database();
$db = $database->getConnection();


$userobject = new User($db);
$term = new Term($db);


// set documentation
$documentation = new Documentation($page_url);

// header settings
$page_title = "View Terms";
include_once "includes/header_param.php";


// query terms
$stmt = $term->readAll();
$num = $stmt->rowCount();

include_once "includes/terms_main.inc.php";

include_once "includes/footer.php";
?>