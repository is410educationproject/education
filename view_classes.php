<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = true;
$termselector = true;
$page_url = 'view_classes.php?';

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/class.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';
include_once 'objects/term.php';

// instantiate database and class object
$database = new Database();
$db = $database->getConnection();

$class = new Classes($db);
$documentation = new Documentation($page_url);
$userobject = new UserObject($db);
$term = new Term($db);

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';
$activetermDesc = $userobject->readActiveTerm();

// get documentation
$documentation = new Documentation($page_url);

// header settings
$page_title = "View Classes";
include_once "includes/header.php";

include_once "helpers/pagination_config.php";

// query classes
$stmt = $class->readAll($from_record_num, $records_per_page);
$num = $stmt->rowCount();

include_once "includes/classes_main.inc.php";

include_once "includes/footer.php";
?>