<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$page_url = "view_department_info.php?";

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

$departmentID = isset($_GET['departmentID']) ? $_GET['departmentID'] : 1;

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/documentation.php';
include_once 'objects/staffassign.php';
include_once 'objects/staffposition.php';
include_once 'objects/department.php';
include_once 'objects/user.php';
include_once 'objects/term.php';
include_once 'objects/class.php';

// instantiate database and member object
$database = new Database();
$db = $database->getConnection();

$staffassign = new StaffAssignment($db);
$position = new StaffPosition($db);
$department = new Department($db);
$userobject = new UserObject($db);
$term = new Term($db);
$class = new Classes($db);

$class->activeuserID = $activeuserID;
$class->departmentID = $departmentID;
$position->activeuserID = $activeuserID;
$position->departmentID = $departmentID;

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';
$activetermDesc = $userobject->readActiveTerm();

// get documentation
$documentation = new Documentation($page_url);

// header settings
$page_title = "View Department Profile";
include_once "includes/header.php";

// add the term selector to the page
include_once 'includes/termselect.inc.php';

include_once "helpers/pagination_config.php";

// query staff position
$stmtPO = $position->readAll_Positions();
$numPO = $stmtPO->rowCount();


// query classes
$stmt = $class->readAll();
$num = $stmt->rowCount();



include_once "includes/departments_view_info.inc.php";

include_once "includes/footer.php";
?>