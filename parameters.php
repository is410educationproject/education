<?php
	session_start();
    include_once 'includes/login/class.user.php';
    $user = new User();

    $userID = $_SESSION['activeuserID'];

    if (!$user->get_session()){
       header("location:login.php");
    }

    if (isset($_GET['q'])){
        $user->user_logout();
        header("location:landing.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!--<meta charset="utf-8">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="css/site.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/hover.css">
    <link rel="stylesheet" href="css/custom.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <!-- some custom CSS -->
    <style>
    /* Remove the jumbotron's default bottom margin + leather*/ 
     .jumbotron {
      margin-bottom: 0;
	  background-image:url("../live12-11/images/leather.jpg");
	  background-repeat: repeat;
	  font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
	  font-weight: 300;
	  color:white; 
    }
	
    body{
		font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
		font-weight: 300;
    	/*font-family:Arial, Helvetica, sans-serif;*/
    }
	
    h1{
		font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
		font-weight: 300;
    	/*font-family:'Georgia', Times New Roman, serif;*/
    }
	</style>
    <title>Parameters</title>
</head>

<body>
<!-- Bootstrap Grid Version -->
<div class="jumbotron">
    <div class="container text-center">
        <h1><?php $user->get_firstname($userID); ?>, tap or click an icon to get started.</h1>      
        <p>When you are finished, tap or click the Home icon to return to the dashboard.</p>
    </div>
</div>
<div class="container"><center>    
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 hovereffect">
        <div class="panel panel" align="center" onclick="location.href='view_terms.php';" style="cursor: pointer">
            <div class="panel-heading">View Terms</div>
            <div class="panel-body"><img src="images/grey-icons/calendar-7-128.png" alt="Terms"></a></div>
            <div class="panel-footer">View and edit the list of class terms to be ascribed to classes.</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 hovereffect"> 
        <div class="panel panel" align="center" onclick="location.href='view_times.php';" style="cursor: pointer">
            <div class="panel-heading">View Times</div>
            <div class="panel-body"><img src="images/grey-icons/clock-3-128.png" alt="Times"></a></div>
            <div class="panel-footer">View and edit the list of individual times to be ascribed to classes.</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 hovereffect"> 
        <div class="panel panel" align="center" onclick="location.href='view_contactmethods.php';" style="cursor: pointer">
            <div class="panel-heading">View Contact Methods</div>
            <div class="panel-body"><img src="images/grey-icons/phone-70-128.png" alt="Contact Methods"></a></div>
            <div class="panel-footer">View and edit the list of contact methods to be selected.</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 hovereffect"> 
        <div class="panel panel" align="center" onclick="location.href='view_statuses.php';" style="cursor: pointer">
            <div class="panel-heading">View Statuses</div>
            <div class="panel-body"><img src="images/grey-icons/edit-property-128.png" alt="Statuses"></a></div>
            <div class="panel-footer">View and edit the list of statuses to be ascribed to members.</div>
        </div>
    </div>   
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-centered hovereffect">
        <div class="panel panel" align="center" onclick="location.href='view_familypositions.php';" style="cursor: pointer">
            <div class="panel-heading">View Family Positions</div>
            <div class="panel-body"><img src="images/grey-icons/conference-2-128.png" alt="Family Positions"></a></div>
            <div class="panel-footer">View and edit the list of family positions to be ascribed to members.</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-centered hovereffect">
        <div class="panel panel" align="center" onclick="location.href='view_gradelevels.php';" style="cursor: pointer">
            <div class="panel-heading">View Grade Levels</div>
            <div class="panel-body"><img src="images/grey-icons/list-ingredients-128.png" alt="Grade Levels"></a></div>
            <div class="panel-footer">View and edit the list of grade levels to be ascribed to classes within departments.</div>
        </div>
       </div>
   <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-centered hovereffect">
        <div class="panel panel" align="center" onclick="location.href='view_staffpositions.php';" style="cursor: pointer">
            <div class="panel-heading">View Staff Positions</div>
            <div class="panel-body"><img src="images/grey-icons/manager-128.png" alt="Staff Positions"></a></div>
            <div class="panel-footer">View and edit the list of staff positions to be ascribed to members.</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-centered hovereffect"> 
        <div class="panel panel" align="center" onclick="location.href='dashboard.php';" style="cursor: pointer">
            <div class="panel-heading">Home</div>
            <div class="panel-body"><img src="images/grey-icons/home-2-128.png" alt="Home"></a></div>
            <div class="panel-footer">When you feel you are finished, please click or tap here to return to the Dashboard.</div>
        </div>
    </div>
</center></div><br><br>
</body>
</html>