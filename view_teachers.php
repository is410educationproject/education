<?php session_start();

//Check whether the session variable SESS_MEMBER_ID is present or not
if(!isset($_SESSION['sess_user_id']) || (trim($_SESSION['sess_user_id']) == '')) {
	header("location: notautho.html");
	exit();
}
	include "db_connect.php"; // Links the current page to the db_connect page
?>

<!--
	Education Module
	Spring 2014			Anthony Barkley, Andrew Emerson, Kelsi Thomas
	Revised Fall 2014	Morgan House, Nellie Raihala, Gib Smith, Alex Tebo
-->

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" type="text/css" href="style.css" />
	<title>Church Management System - Education </title>
	<script src="view_teachers_deactivate_button.js"></script>
	<script src="view_teachers_activate_button.js"></script>
	<script src="view_teachers_remove_button.js"></script>
	<script src="edit_teachers_button.js"></script>
	<script src="jquery.js"></script>
	<script language="JavaScript">
	function toggle(source) {
		checkboxes = document.getElementsByName('foo');
		for(var i=0, n=checkboxes.length;i<n;i++) {
			checkboxes[i].checked = source.checked;
  }
}
</script>
</head>

<body>

<!-------------------- Navigation -------------------->	
<div id='cssmenu'>
	<ul>
		<li class='active'><a href="education.php"><span>Home</span></a></li>
		<li class='has-sub'><a href='#'><span>Class</span></a>
			<ul>
				<li><a href="add_class.php"><span>Add Class</span></a></li>
				<li><a href="view_class.php"><span>View Classes</span></a></li>
			</ul>
		</li>
		<li class='has-sub'><a href='#'><span>Student</span></a>
			<ul>
				<li><a href="add_student.php"><span>Add Student</span></a></li>
				<li><a href="view_students.php"><span>View Students</span></a></li>
			</ul>
		</li>
		<li class='has-sub'><a href='#'><span>Staff</span></a>
			<ul>
				<li><a href="add_teacher.php"><span>Add Staff</span></a></li>
				<li><a href="view_teachers.php"><span>View Staff</span></a></li>
			</ul>
		</li>
		<li class='has-sub'><a href='#'><span>Reports</span></a>
			<ul>
				<li><a href="class_report.php"><span>Class Reports</span></a></li>
				<li><a href="student_report.php"><span>Student Reports</span></a></li>
				<li><a href="teacher_report.php"><span>Staff Reports</span></a></li>
			</ul>
		</li>
<!--		<li class='has-sub'><a href='#'><span>History</span></a>
			<ul>
				<li><a href="class_history.php"><span>Class History</span></a></li>
				<li><a href="teacher_history.php"><span>Staff History</span></a></li>
			</ul>
		</li>-->
	</ul>
	<ul2>
		<li2><a href="logout.php"><span>Logout</span></a></li2>
	</ul2>
</div>
<!-------------------- End Navigation -------------------->

<!-------------------- End View Staff Entry Form -------------------->
<div id="viewingwrapper">
	<br>
	<h1>View Staff</h1>
	<section>
		<hr>
		
		<form id="view_teacher" name="view_teacher" method="post" >
	
			<table>	
					<td>Position: </td>
					<td>
						<select name="position">
							<option value = "All">Select All</option>
							<option value="Teacher">Teacher</option>
							<option value="Assistant">Assistant</option>
							<option value="Substitute">Substitute</option>
							<option value="Coordinator">Coordinator</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>Status: </td>
					<td>
						<select id="TeacherStatus" name="TeacherStatus">
							<option value="All">Select All</option>
							<option value="Active">Active</option>
							<option value="Inactive">Inactive</option>
						</select>
					</td>
				</tr>
			</table>
		
			<h2>
			<input type="submit" name="submit" value="Submit">
			</h2>
			<hr>
			<br>
			
		</form>
		
	</section>
	<br>
	<br>
	<br>
	<section>
	<h1> Options </h1>
	<hr>
		<h2>
		<a href ="add_teacher.php"><input type="button" value="Add"/></a>
        <button onclick="remove_teachers()">Remove</button>

		<br><br>
        <button onclick="activate_teachers()">Activate</button>
		<button onclick="deactivate_teachers()">Deactivate</button>
		<br><br>
		<input type="checkbox" onClick="toggle(this)" /> Select All<br/>		
		</h2>
	</section>
</div>

<!-------------------- End of View Staff Entry Form -------------------->

<!-------------------- View Staff Table -------------------->
<div id="viewingwrapperstaff"><p id ="viewingwrapperstaff">
	<section>
	<br>
<?php
$Position=(isset($_POST['position']) ? $_POST['position'] : null);
/* $GradeLevel=(isset($_POST['T']) ? $_POST['gradelevel'] : null); */
$ViewStatus=(isset($_POST['TeacherStatus']) ? $_POST['TeacherStatus'] : null);

/* $Status=(isset($_POST['status']) ? $_POST['status'] : null); */

	if($db_found)
	{
	//search for staff based on position with ALL Status
 	if ($ViewStatus == "All" && $Position == "Teacher")
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute, IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsTeacher = 'Yes'
							ORDER BY TeacherLast");
	}
	 else if ($ViewStatus == "All" && $Position == "Assistant")
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute,  IsCoordinator,TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsAssistant = 'Yes'
							ORDER BY TeacherLast");
	}
	 else if ($ViewStatus == "All" && $Position == "Substitute")
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute, IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsSubstitute = 'Yes'
							ORDER BY TeacherLast");
	}
	 else if ($ViewStatus == "All" && $Position == "Coordinator")
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute, IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsCoordinator = 'Yes'
							ORDER BY TeacherLast");
	}	
	//Active Staff by Position
 	else if ($ViewStatus == "Active" && $Position == "Teacher")
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute,  IsCoordinator,TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsTeacher = 'Yes' && TeacherStatus = 'Active'
							ORDER BY TeacherLast");
	}
	 else if ($ViewStatus == "Active" && $Position == "Assistant")
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute, IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsAssistant = 'Yes' && TeacherStatus = 'Active'
							ORDER BY TeacherLast");
	}
	 else if ($ViewStatus == "Active" && $Position == "Substitute")
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute, IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsSubstitute = 'Yes' && TeacherStatus = 'Active'
							ORDER BY TeacherLast");
	}
	 else if ($ViewStatus == "Active" && $Position == "Coordinator")	
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute, IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsCoordinator = 'Yes' && TeacherStatus = 'Active'
							ORDER BY TeacherLast");
	}
	 else if ($ViewStatus == "Active" && $Position == "All")	
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute, IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE TeacherStatus = 'Active'
							ORDER BY TeacherLast");
	}	
	//Not Active staff by Position						
	 else if ($ViewStatus == "Inactive" && $Position == "Teacher")
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute, IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsTeacher = 'Yes' && TeacherStatus = 'Inactive'
							ORDER BY TeacherLast");
	}
	 else if ($ViewStatus == "Inactive" && $Position == "Assistant")
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute,IsCoordinator,TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsAssistant = 'Yes' && TeacherStatus = 'Inactive'
							ORDER BY TeacherLast");
	}
	 else if ($ViewStatus == "Inactive" && $Position == "Substitute")
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute,IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsSubstitute = 'Yes' && TeacherStatus = 'Inactive'
							ORDER BY TeacherLast");						
	}
	 else if ($ViewStatus == "Inactive" && $Position == "Coordinator")
	{
	$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute,IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE IsCoordinator = 'Yes' && TeacherStatus = 'Inactive'
							ORDER BY TeacherLast");						
	}
	 else if ($ViewStatus == "Inactive" && $Position == "All")
	{
	$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute,IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							WHERE TeacherStatus = 'Inactive'
							ORDER BY TeacherLast");						
	}	
	else
	{
		$sql = mysql_query("SELECT TeacherID, TeacherFirst, TeacherLast, IsTeacher, IsAssistant, IsSubstitute, IsCoordinator, TeacherPhone, TeacherEmail, TeacherNotes, Availability, VBSAvailability
							FROM teacher
							ORDER BY TeacherLast");
	}	
		
		echo '<div align="center">';
		echo '<table border = 1px>'; 

		/* table headers */
		echo "<tr>
				<th>Select </th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Teacher</th>
				<th>Assistant</th>
				<th>Substitute</th>
				<th>Coordinator</th>
				<th>Availability</th>
				<th>VBS</th>
				<th>Email</th>
				<th>Notes</th>
			</tr>";

		if($sql == FALSE)
		{
		die(mysql_error());
		}
		
		
		//while loop to print all the rows in a table
		while ($teacher_data = mysql_fetch_array($sql))			//Error location!!!
		{
			//$TeacherID = $teacher_data['TeacherID'];
			echo '<tr>';
				echo "<td align = center>  <a href=''> <input name='foo' type='checkbox' id=\"checkbox".$teacher_data['TeacherID']."\"> </a></td>";
				echo '<td>' .$teacher_data['TeacherFirst']. '</td>';
				echo '<td>' .$teacher_data['TeacherLast']. '</td>'; 
				echo '<td>' .$teacher_data['IsTeacher']. '</td>';
				echo '<td>' .$teacher_data['IsAssistant']. '</td>';
				echo '<td>' .$teacher_data['IsSubstitute']. '</td>';
				echo '<td>' .$teacher_data['IsCoordinator']. '</td>';
				echo '<td>' .$teacher_data['Availability']. '</td>';
				echo '<td>' .$teacher_data['VBSAvailability']. '</td>';
				echo '<td>' .$teacher_data['TeacherEmail']. '</td>';
				echo '<td>' .$teacher_data['TeacherNotes']. '</td>';
				echo "<td>  <button> <a href='edit_teacherstest2.php?id=".$teacher_data['TeacherID']."\"> <input type='button' value='Edit'> Edit</button> </a> </td>";
			echo '</tr>';		
		}
	echo '</table>';
	
		
}
else 
{
	print "Database NOT Found";
	mysql_close($db_connection);
}
?>
<hr>
<br>
</section>
</div>
<!-------------------- End of View Staff Table -------------------->

</body>
</html>