<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();

if (!$user->logged_in()){
	header("location:login.php");
}

$pagination = true;

// set page url
$page_url = 'search_classes.php?';

// include database and object files
include_once 'helpers/config.php';
include_once "helpers/pagination_config.php";
include_once 'includes/db_connect.php';
include_once 'objects/class.php';
include_once 'objects/term.php';
include_once 'objects/time.php';
include_once 'objects/documentation.php';

// instantiate database and member object
$database = new Database();
$db = $database->getConnection();

$class = new Classes($db);
$time = new Time($db);
$term = new Term($db);
$documentation = new Documentation($page_url);

// get search term
$search_term=isset($_GET['s']) ? $_GET['s'] : '';

$page_title = "You searched for \"{$search_term}\"";
include_once "includes/header.php";

// query members
$stmt = $class->search($search_term, $from_record_num, $records_per_page);
$num = $stmt->rowCount();

$page_url="search_classes.php?s={$search_term}&";
include_once "includes/classes_main.inc.php";

include_once "includes/footer.php";
?>