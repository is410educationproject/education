<?php
include_once 'includes/login/class.user.php';
$user = new User();

if (isset($_GET['q'])){
        $user->user_logout();
        header("location:index.php");
    }
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable= no">
    <!-- bootstrap css --> 
    <link href="css/bootstrap.css" rel="stylesheet" />
    <!-- Hover CSS -->
    <link rel="stylesheet" href ="css/hover2.css">
	<style>
	html {
		background:url(images/churchimage.jpg) no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/churchimage.jpg', sizingMethod='scale')";
  		filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/churchimage.jpg', sizingMethod='scale');
	}
	body {
		background-color: none;	
	}
	</style>
<title>Welcome</title>
</head>

<body onClick="location.href='login.php';" style="cursor:pointer">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="hovereffect">
        <div class="overlay">
           <h2>Welcome to College Church Education Site</h2>
           <a class="info">Click anywhere to log in</a>
        </div>
    </div>
</div>
</body>
</html>
