<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();

if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = true;
$termselector = false;
$page_url = "search_staffassignments.php?";

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

$route = isset($_GET['route']) ? $_GET['route'] : 1;
$positionID = isset($_GET['positionID']) ? $_GET['positionID'] : 1;
$departmentID = isset($_GET['departmentID']) ? $_GET['departmentID'] : 1;
$search = isset($_GET['search']) ? $_GET['search'] : 1;

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/staffassign.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';
include_once 'objects/term.php';


// instantiate database and member object
$database = new Database();
$db = $database->getConnection();

$assignment = new StaffAssignment($db);
$documentation = new Documentation($page_url);
$userobject = new UserObject($db);
$term = new Term($db);

$assignment->activeuserID = $activeuserID;
$assignment->positionID = $positionID;
$assignment->departmentID = $departmentID;

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';

// get search term
$search_term = isset($_GET['s']) ? $_GET['s'] : '';

$page_title = "You searched for \"{$search_term}\"";
include_once "includes/header.php";

include_once "helpers/pagination_config.php";

// query members
$stmt = $assignment->search($search_term, $from_record_num, $records_per_page);
$num = $stmt->rowCount();

$page_url="search_staffassignments.php?s={$search_term}&";
include_once "includes/staffassignments_main.inc.php";

include_once "includes/footer.php";
?>