<?php

    include_once 'includes/login/class.user.php';
    $user = new User();


	if (isset($_SESSION['login'])){
		header("location:registration.php");
    }

    /*// Checking for user logged in or not
    if (!$user->get_session())
    {
       header("location:login.php");
    }*/
	
    if (isset($_REQUEST['submit'])){
        extract($_REQUEST);
        $register = $user->reg_user($fname,$lname,$username,$password,$email);
        if ($register) {
            // Registration Success
            $success = true;
        } else {
            // Registration Failed
            echo 'Registration failed. Email or Username already exits please try again';
        }
    }
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        	<!-- bootstrap css --> 
            <link href="css/bootstrap.css" rel="stylesheet" media="screen" />
            <<link rel="stylesheet" href="css/bootstrap-switch.css" />
            <!-- jquery ui css
            <link rel="stylesheet" href="jquery-ui.min.css" /> -->
            <link rel="stylesheet" href="css/custom.css" />
            <!-- custom site css -->
            <link rel="stylesheet" type="text/css" href="css/site.css">
            <link rel="stylesheet" href="css/custom.css" />
            <!-- some custom CSS -->
			<style>
            .padding-bottom-2em{
                padding-bottom:2em;
            }
            
            .width-30-pct{
                width:30%;
            }
            
            .width-40-pct{
                width:40%;
            }
            
            .overflow-hidden{
                overflow:hidden;
            }
            
            .margin-right-1em{
                margin-right:1em;
            }
            
            .margin-left-1em{
                margin-left:1em;
            }
            
            .right-margin{
                margin:0 .5em 0 0;
            }
            
            .margin-bottom-1em {
                margin-bottom:1em;
            }
            
            .margin-zero{
                margin:0;
            }
            
            .text-align-center{
                text-align:center;
            }
            </style>
      
        <title>Register</title>
		<style>
            #container{width:400px; margin: 0 auto;}
		</style>
    </head>
    <body>
    	<div id ="outercontainer" class="container outercontainer">
            <div "col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h1>Registration</h1>
                <form action="registration.php" method="post" name="reg">
                    <div class="form-group">
                    <label for="fname">First Name:</label>
                    <input type="text" class="form-control" name="fname" placeholder="Enter first name..." required>
                  </div>
                  <div class="form-group">
                    <label for="lname">Last Name:</label>
                    <input type="text" class="form-control" name="lname" placeholder="Enter last name..." required>
                  </div>
                  <div class="form-group">
                    <label for="username">username:</label>
                    <input type="text" class="form-control" name="username" placeholder="Enter username..." required>
                  </div>
                  <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" class="form-control" name="email" placeholder="Enter email address..." required>
                  </div>
                  <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control" name="password" placeholder="Enter password..." required>
                  </div>
                    <input type="submit" name="submit" value ="Register" class="btn btn-default">&nbsp;
                    <a href='dashboard.php' class="btn btn-default"><span class="glyphicon glyphicon-home"></span>&nbsp;Home</a>
                 </form>
                 <?php 
                 if (isset($success) == true) { 
                    echo 'Registration successful <a href="login.php">Click here</a> to login';
                }
                ?>
            </div>
        </div>
    </body>
</html>
