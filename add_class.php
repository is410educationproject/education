<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$page_url = "add_class.php?";

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

$departmentID = isset($_GET['departmentID']) ? $_GET['departmentID'] : 1;

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/class.php';
include_once 'objects/time.php';
include_once 'objects/gradelevel.php';
include_once 'objects/term.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate class.php object
$class = new Classes($db);
$time = new Time($db);
$term = new Term($db);
$gradelevel = new GradeLevel($db);
$userobject = new UserObject($db);

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';

// get documentation
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Add Class";
include_once "includes/header.php";

include_once "includes/classes_add.inc.php";

include_once "includes/footer.php";
?>