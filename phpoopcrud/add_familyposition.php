<?php
$pagination = false;

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/familyposition.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate familyposition.php object
$familyposition = new FamilyPosition($db);

// set page headers
$page_title = "Add Family Position";
include_once "includes/header.php";

include_once "includes/familypositions_add.inc.php";

include_once "includes/footer.php";
?>