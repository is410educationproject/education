<?php
$pagination = false;

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/session.php';

// instantiate database and session.php object
$database = new Database();
$db = $database->getConnection();

$session = new Session($db);

// header settings
$page_title = "View Sessions";
include_once "includes/header.php";

$page_url="view_sessions.php?";

// query sessions
$stmt = $session->readAll();
$num = $stmt->rowCount();

include_once "includes/sessions_main.inc.php";

include_once "includes/footer.php";
?>