<?php
$pagination = false;

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/member.php';
include_once 'objects/contactevent.php';
include_once 'objects/familyposition.php';

// instantiate database and member object
$database = new Database();
$db = $database->getConnection();

$member = new Member($db);
$contact = new ContactEvent($db);
$familyposition = new FamilyPosition($db);

// header settings
$page_title = "View Contact Events";
include_once "includes/header.php";

$page_url="view_member_info.php?";
include_once "helpers/pagination_config.php";

$memberID = isset($_GET['memberID']) ? $_GET['memberID'] : 1;

// query members
$stmt = $contact->readAll($memberID);
$num = $stmt->rowCount();

include_once "includes/members_view_info.inc.php";

include_once "includes/footer.php";
?>