<?php
class Classes{
	
	// database connection and table name
	private $conn;
	private $table_name = "classes";
	
	// object properties
	public $classID;
	public $classname;
	public $gradelevel;
	public $room;
	public $timeID;
	public $sessionID;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	// count records in date ranges
	/*public function countSearchByDateRange($date_from, $date_to){
		
		// query to count records in date ranges
		$query = "SELECT COUNT(*) as total_rows 
					FROM members 
					WHERE 
						created BETWEEN :date_from AND :date_to 
						OR created LIKE :date_from_for_query 
						OR created LIKE :date_to_for_query";

		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// bind date variables
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);
		
		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		// execute query and get total rows
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}*/
	
	public function searchByDateRange($date_from, $date_to, $from_record_num, $records_per_page){

		//select all data
		$query = "SELECT m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc as f_familypositiondesc 
					FROM " . $this->table_name . " m 
						LEFT JOIN familypositions f 
							ON m.familypositionID=f.familypositionID 
					ORDER BY memberID ASC
					LIMIT :from_record_num, :records_per_page";
					/*WHERE 
						p.created BETWEEN :date_from AND :date_to 
						OR p.created LIKE :date_from_for_query 
						OR p.created LIKE :date_to_for_query*/
						
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);

		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();
		
		return $stmt;
	}
	
	// used to export records to csv
	public function export_CSV(){
		
		//select all data
		//$query = "SELECT memberID, fname, lname, phone, created, modified FROM products";
		$query = "SELECT classID, classname, gradelevel, room, timeID, sessionID FROM classes";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		//this is how to get number of rows returned
		$num = $stmt->rowCount();

		$out = "ClassID,Classname,Grade Level,Room Number,TimeID,SessionID\n";

		if($num>0){
			//retrieve our table contents
			//fetch() is faster than fetchAll()
			//http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				//extract row
				//this will make $row['name'] to
				//just $name only
				extract($row);
				$out.="{$classID},\"{$classname}\",\"{$gradelevel}\",{$room},\"{timeID}\",\"{sessionID}\n";
			}
		}
		
		return $out;
	}
	
//NEED TO DO THIS!!!	
	
	// view classes by search term
	public function search($search_term, $from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					t.timeDesc as t_timeDesc, s.sessionDesc as s_sessionDesc, c.classID, c.classname, c.gradelevel, c.room, c.timeID, c.sessionID 
				FROM 
					" . $this->table_name . " c
					LEFT JOIN 
						time t
							ON c.timeID = t.timeID
					LEFT JOIN 
						session s
							ON c.sessionID = s.sessionID
				WHERE 
					c.classname LIKE ? OR t.timeDesc LIKE ? OR s.sessionDesc LIKE ? 
				ORDER BY 
					c.classname ASC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		$stmt->bindParam(2, $search_term);
		$stmt->bindParam(3, $search_term);
		$stmt->bindParam(4, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(5, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	public function countAll_BySearch($search_term){

		// select query
		$query = "SELECT 
					COUNT(*) as total_rows 
				FROM 
					" . $this->table_name . " c
					LEFT JOIN 
						time t
							ON c.timeID = t.timeID
					LEFT JOIN 
						session s
							ON c.sessionID = s.sessionID
				WHERE 
					c.classname LIKE ? OR t.timeDesc LIKE ? OR s.sessionDesc LIKE ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		$stmt->bindParam(2, $search_term);
		$stmt->bindParam(3, $search_term);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// add class
	public function add(){
		
		// to get time-stamp for 'created' field
		//$this->getTimestamp();
		
		//write query
		$query = "INSERT INTO 
					" . $this->table_name . " 
				SET 
					classname = ?, gradelevel = ?, room = ?, timeID = ?, sessionID = ?";
		
		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(1, $this->classname);
		$stmt->bindParam(2, $this->gradelevel);
		$stmt->bindParam(3, $this->room);
		$stmt->bindParam(4, $this->timeID);
		$stmt->bindParam(5, $this->sessionID);
		
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
	
	// view classes with field sorting
	public function readAll_WithSorting($from_record_num, $records_per_page, $field, $order){

		$query = "SELECT m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc 
					FROM members m 
						LEFT JOIN familypositions f 
							ON m.familypositionID = f.familypositionID 
					ORDER BY {$field} {$order}
					LIMIT :from_record_num, :records_per_page";

		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();

		// return values from database
		return $stmt;
	}
	
	// view classes
	public function readAll($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					c.classID, c.classname, c.gradelevel, c.room, t.timeDesc AS t_timeDesc, s.sessionDesc AS s_sessionDesc 
				FROM 
					" . $this->table_name . " c
					LEFT JOIN 
						time t
							ON c.timeID = t.timeID
					LEFT JOIN 
						session s
							ON c.sessionID = s.sessionID
				ORDER BY 
					c.classname ASC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// view members
	public function readAll_ByCategory($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc  
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
				WHERE 
					f.familypositionID=?
				ORDER BY 
					m.memberID ASC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $this->familypositionID);
		$stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// view members
	public function countAll_ByCategory(){

		// select query
		$query = "SELECT 
					COUNT(*) as total_rows 
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
				WHERE 
					m.memberID=?";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->familypositionID);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used for paging member list with field sorting
	public function countAll_WithSorting($field, $order){
		// for now countAll() is used
	}
	
	// used for paging classes
	public function countAll(){
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
		
		$stmt = $this->conn->prepare( $query );
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used when filling up the update class form
	public function readOne(){
		
		$query = "SELECT 
					classname, gradelevel, room, timeID, sessionID 
				FROM 
					" . $this->table_name . " 
				WHERE 
					classID = ? 
				LIMIT 
					0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->classID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->classname = $row['classname'];
		$this->gradelevel = $row['gradelevel'];
		$this->room = $row['room'];
		$this->timeID = $row['timeID'];
		$this->sessionID = $row['sessionID'];
	}
	
	// update the class
	public function update(){

		$query = "UPDATE 
					" . $this->table_name . " 
				SET 
					classname = :classname, 
					gradelevel = :gradelevel, 
					room = :room, 
					timeID = :timeID,
					sessionID = :sessionID
				WHERE
					classID = :classID";

		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(':classname', $this->classname);
		$stmt->bindParam(':gradelevel', $this->gradelevel);
		$stmt->bindParam(':room', $this->room);
		$stmt->bindParam(':timeID', $this->timeID);
		$stmt->bindParam(':sessionID', $this->sessionID);
		$stmt->bindParam(':classID', $this->classID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete the class
	public function delete(){
	
		$query = "DELETE FROM " . $this->table_name . " WHERE classID = ?";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(1, $this->classID);

		if($result = $stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete selected classes
	public function deleteSelected($classIDs){
		
		$in_classIDs = str_repeat('?,', count($classIDs) - 1) . '?';
		
		// query to delete multiple records
		$query = "DELETE FROM " . $this->table_name . " WHERE classID IN ({$in_classIDs})";
		 
		$stmt = $this->conn->prepare($query);
	 
		if($stmt->execute($classIDs)){
			return true;
		}else{
			return false;
		}
	}
	
	// used for the 'created' field when creating a member
	/*public function getTimestamp(){
		date_default_timezone_set('Asia/Manila');
		$this->timestamp = date('Y-m-d H:i:s');
	}*/
}
?>