<?php
class User{
	
	// database connection and table name
	private $conn;
	private $table_name = "users";
	
	// object properties
	public $userID;
	public $fname;
	public $lname;
	public $email;
	public $salt;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	public function emailcheck(){
		// check if the email is already registered
		// select single record query
		$query = "SELECT 1 
				FROM " . $this->table_name . "  
				WHERE email = ? 
				LIMIT 0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->email);

		// execute the query
		$stmt->execute();

		// get record details
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// assign values to object properties
		$this->email = $row['email'];
	}
	
	
	
	public function readOne(){
		// read the details of session to be edited
		// select single record query
		$query = "SELECT sessionID, sessionDesc 
				FROM " . $this->table_name . "  
				WHERE sessionID = ? 
				LIMIT 0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->sessionID);

		// execute the query
		$stmt->execute();

		// get record details
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// assign values to object properties
		$this->sessionDesc = $row['sessionDesc'];
	}
	
	public function update(){
		// update the session
		$query = "UPDATE " . $this->table_name . "   
				SET sessionDesc = :sessionDesc
				WHERE sessionID = :sessionID";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(':sessionDesc', $this->sessionDesc);
		$stmt->bindParam(':sessionID', $this->sessionID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function delete(){
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE sessionID = ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);
			
		// bind record id
		$stmt->bindParam(1, $this->sessionID);

		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function add(){
		// add the session
		// insert query
		$query = "INSERT INTO users  
				SET fname = ?, lname = ?, email = ?, password = ?, salt = ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(1, $this->fname);
		$stmt->bindParam(2, $this->lname);
		$stmt->bindParam(3, $this->email);
		$stmt->bindParam(4, $this->password);
		$stmt->bindParam(5, $this->salt);
		
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function searchAll($search_term, $from_record_num, $records_per_page){
		// search sessions based on search term
		// search query
		$query = "SELECT sessionID, sessionDesc 
				FROM " . $this->table_name . " 
				WHERE sessionDesc LIKE ? 
				ORDER BY sessionID ASC 
				LIMIT ?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind  variables
		$query_search_term = "%{$search_term}%";

		$stmt->bindParam(1, $query_search_term);
		$stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();	

		return $stmt;
	}
	
	// count all familypositions
	public function countAll(){
		// query to count all data
		$query = "SELECT COUNT(*) as total_rows FROM familypositions";
	 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		return $total_rows;
	}
	
	// count all sessions with search term
	public function countAll_WithSearch($search_term){
		// search query
		$query = "SELECT COUNT(*) as total_rows FROM session WHERE sessionDesc LIKE ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// bind search term
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		return $total_rows;
	}
	
	// read all with paging
	public function readAll_WithPaging($from_record_num, $records_per_page){
		// read all sessions from the database
		$query = "SELECT sessionID, sessionDesc 
				FROM " . $this->table_name . " 
				ORDER BY sessionID DESC  
				LIMIT ?, ?";	

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind values
		$stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();
		
		return $stmt;
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					sessionID, sessionDesc 
				FROM 
					" . $this->table_name . " 
				ORDER BY 
					sessionID ASC";	
		
		$stmt = $this->conn->prepare( $query );		
		$stmt->execute();
		
		return $stmt;	
	}
	
	// view sessions
	public function readAll(){

		// select query
		$query = "SELECT 
					sessionID, sessionDesc 
				FROM 
					" . $this->table_name . "
				ORDER BY 
					sessionID ASC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// used to read session description by its ID
	function readNameById(){
		
		$query = "SELECT sessionDesc FROM " . $this->table_name . " WHERE sessionID = ? limit 0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->sessionID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->sessionDesc = $row['sessionDesc'];
	}
}
?>