<?php
class ContactMethod{
	
	// database connection and table name
	private $conn;
	private $table_name = "contactmethods";
	
	// object properties
	public $methodID;
	public $methodDesc;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	public function readOne(){
		// read the details of family position to be edited
		// select single record query
		$query = "SELECT methodID, methodDesc 
				FROM " . $this->table_name . "  
				WHERE methodID = ? 
				LIMIT 0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->methodID);

		// execute the query
		$stmt->execute();

		// get record details
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// assign values to object properties
		$this->methodDesc = $row['methodDesc'];
	}
	
	public function update(){
		// update the method
		$query = "UPDATE " . $this->table_name . "   
				SET methodDesc = :methodDesc
				WHERE methodID = :methodID";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(':methodDesc', $this->methodDesc);
		$stmt->bindParam(':methodID', $this->methodID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function delete(){
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE methodID = ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);
			
		// bind record id
		$stmt->bindParam(1, $this->methodID);

		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function add(){
		// add the family position
		// insert query
		$query = "INSERT INTO " . $this->table_name . "  
				SET methodDesc = ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(1, $this->methodDesc);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					methodID, methodDesc 
				FROM 
					" . $this->table_name . " 
				ORDER BY 
					methodID";	
		
		$stmt = $this->conn->prepare( $query );		
		$stmt->execute();
		
		return $stmt;	
	}
	
	// view family positions
	public function readAll(){

		// select query
		$query = "SELECT 
					methodID, methodDesc 
				FROM 
					" . $this->table_name . "
				ORDER BY 
					methodID ASC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// used to read family position description by its ID
	function readNameById(){
		
		$query = "SELECT methodDesc FROM " . $this->table_name . " WHERE methodID = ? limit 0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->methodID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->methodDesc = $row['methodDesc'];
	}
}
?>