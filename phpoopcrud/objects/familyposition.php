<?php
class FamilyPosition{
	
	// database connection and table name
	private $conn;
	private $table_name = "familypositions";
	
	// object properties
	public $familypositionID;
	public $familypositiondesc;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	public function readOne(){
		// read the details of family position to be edited
		// select single record query
		$query = "SELECT familypositionID, familypositiondesc 
				FROM " . $this->table_name . "  
				WHERE familypositionID = ? 
				LIMIT 0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->familypositionID);

		// execute the query
		$stmt->execute();

		// get record details
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// assign values to object properties
		$this->familypositiondesc = $row['familypositiondesc'];
	}
	
	public function update(){
		// update the category
		$query = "UPDATE " . $this->table_name . "   
				SET familypositiondesc = :familypositiondesc
				WHERE familypositionID = :familypositionID";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(':familypositiondesc', $this->familypositiondesc);
		$stmt->bindParam(':familypositionID', $this->familypositionID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function delete(){
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE familypositionID = ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);
			
		// bind record id
		$stmt->bindParam(1, $this->familypositionID);

		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function add(){
		// add the family position
		// insert query
		$query = "INSERT INTO familypositions  
				SET familypositiondesc = ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(1, $this->familypositiondesc);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function searchAll($search_term, $from_record_num, $records_per_page){
		// search family positions based on search term
		// search query
		$query = "SELECT familypositionID, familypositiondesc 
				FROM " . $this->table_name . " 
				WHERE familypositiondesc LIKE ? 
				ORDER BY familypositionID ASC 
				LIMIT ?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind  variables
		$query_search_term = "%{$search_term}%";

		$stmt->bindParam(1, $query_search_term);
		$stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();	

		return $stmt;
	}
	
	// count all familypositions
	public function countAll(){
		// query to count all data
		$query = "SELECT COUNT(*) as total_rows FROM familypositions";
	 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		return $total_rows;
	}
	
	// count all family positions with search term
	public function countAll_WithSearch($search_term){
		// search query
		$query = "SELECT COUNT(*) as total_rows FROM familypositions WHERE familypositiondesc LIKE ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// bind search term
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		return $total_rows;
	}
	
	// read all with paging
	public function readAll_WithPaging($from_record_num, $records_per_page){
		// read all family positions from the database
		$query = "SELECT familypositionID, familypositiondesc 
				FROM " . $this->table_name . " 
				ORDER BY familypositionID DESC  
				LIMIT ?, ?";	

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind values
		$stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();
		
		return $stmt;
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					familypositionID, familypositiondesc 
				FROM 
					" . $this->table_name . " 
				ORDER BY 
					familypositionID";	
		
		$stmt = $this->conn->prepare( $query );		
		$stmt->execute();
		
		return $stmt;	
	}
	
	// view family positions
	public function readAll($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					familypositionID, familypositiondesc 
				FROM 
					" . $this->table_name . "
				ORDER BY 
					familypositionID ASC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// used to read family position description by its ID
	function readNameById(){
		
		$query = "SELECT familypositiondesc FROM " . $this->table_name . " WHERE familypositionID = ? limit 0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->familypositionID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->familypositiondesc = $row['familypositiondesc'];
	}
}
?>