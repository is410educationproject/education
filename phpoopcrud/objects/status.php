<?php
class Status{
	
	// database connection and table name
	private $conn;
	private $table_name = "status";
	
	// object properties
	public $statusID;
	public $statusDesc;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	public function readOne(){
		// read the details of family position to be edited
		// select single record query
		$query = "SELECT statusID, statusDesc 
				FROM " . $this->table_name . "  
				WHERE statusID = ? 
				LIMIT 0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->statusID);

		// execute the query
		$stmt->execute();

		// get record details
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// assign values to object properties
		$this->statusDesc = $row['statusDesc'];
	}
	
	public function update(){
		// update the status
		$query = "UPDATE " . $this->table_name . "   
				SET statusDesc = :statusDesc
				WHERE statusID = :statusID";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(':statusDesc', $this->statusDesc);
		$stmt->bindParam(':statusID', $this->statusID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function delete(){
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE statusID = ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);
			
		// bind record id
		$stmt->bindParam(1, $this->statusID);

		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function add(){
		// add the family position
		// insert query
		$query = "INSERT INTO " . $this->table_name . "  
				SET statusDesc = ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(1, $this->statusDesc);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					statusID, statusDesc 
				FROM 
					" . $this->table_name . " 
				ORDER BY 
					statusID";	
		
		$stmt = $this->conn->prepare( $query );		
		$stmt->execute();
		
		return $stmt;	
	}
	
	// view family positions
	public function readAll(){

		// select query
		$query = "SELECT 
					statusID, statusDesc 
				FROM 
					" . $this->table_name . "
				ORDER BY 
					statusID ASC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// used to read family position description by its ID
	function readNameById(){
		
		$query = "SELECT statusDesc FROM " . $this->table_name . " WHERE statusID = ? limit 0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->statusID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->statusDesc = $row['statusDesc'];
	}
}
?>