<?php
class Member{
	
	// database connection and table name
	private $conn;
	private $table_name = "members";
	
	// object properties
	public $memberID;
	public $fname;
	public $lname;
	public $phone;
	public $familypositionID;
	public $email;
	public $phcell;
	public $phwork;
	public $isTeacher;
	public $isSub;
	public $isHelper;
	//public $timestamp;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	// count records in date ranges
	/*public function countSearchByDateRange($date_from, $date_to){
		
		// query to count records in date ranges
		$query = "SELECT COUNT(*) as total_rows 
					FROM members 
					WHERE 
						created BETWEEN :date_from AND :date_to 
						OR created LIKE :date_from_for_query 
						OR created LIKE :date_to_for_query";

		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// bind date variables
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);
		
		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		// execute query and get total rows
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}*/
	
	public function searchByDateRange($date_from, $date_to, $from_record_num, $records_per_page){

		//select all data
		$query = "SELECT m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc as f_familypositiondesc 
					FROM " . $this->table_name . " m 
						LEFT JOIN familypositions f 
							ON m.familypositionID=f.familypositionID 
					ORDER BY memberID ASC
					LIMIT :from_record_num, :records_per_page";
					/*WHERE 
						p.created BETWEEN :date_from AND :date_to 
						OR p.created LIKE :date_from_for_query 
						OR p.created LIKE :date_to_for_query*/
						
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);

		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();
		
		return $stmt;
	}
	
	// used to export records to csv
	public function export_CSV(){
		
		//select all data
		//$query = "SELECT memberID, fname, lname, phone, created, modified FROM products";
		$query = "SELECT memberID, fname, lname, phone, email, phcell, phwork FROM members
		ORDER BY lname ASC, fname ASC";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		//this is how to get number of rows returned
		$num = $stmt->rowCount();

		$out = "MemberID,First Name,Last Name,Phone,Email,Cell Phone,Work Phone\n";

		if($num>0){
			//retrieve our table contents
			//fetch() is faster than fetchAll()
			//http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				//extract row
				//this will make $row['name'] to
				//just $name only
				extract($row);
				$out.="{$memberID},\"{$fname}\",\"{$lname}\",\"{$phone}\",\"{$email}\",\"{$phcell}\",\"{$phwork}\"\n";
			}
		}
		
		return $out;
	}
	
//NEED TO DO THIS!!!	
	
	// view members by search term
	public function search($search_term, $from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					m.memberID, m.fname, m.lname, m.phone, m.email, m.phcell, m.phwork, f.familypositiondesc AS f_familypositiondesc 
				FROM 
					" . $this->table_name . " m
					LEFT JOIN 
						familypositions f
							ON m.familypositionID = f.familypositionID
				WHERE 
					m.fname LIKE ? OR m.lname LIKE ? OR m.phone LIKE ? OR m.email LIKE ? OR m.phcell LIKE ? OR m.phwork LIKE ?
				ORDER BY 
					m.lname ASC, m.fname ASC
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		$stmt->bindParam(2, $search_term);
		$stmt->bindParam(3, $search_term);
		$stmt->bindParam(4, $search_term);
		$stmt->bindParam(5, $search_term);
		$stmt->bindParam(6, $search_term);
		$stmt->bindParam(7, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(8, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	public function countAll_BySearch($search_term){

		// select query
		$query = "SELECT 
					COUNT(*) as total_rows 
				FROM 
					" . $this->table_name . " m
				WHERE 
					m.fname LIKE ? OR m.lname LIKE ? OR m.phone LIKE ? OR m.email LIKE ? OR m.phcell LIKE ? OR m.phwork LIKE ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		$stmt->bindParam(2, $search_term);
		$stmt->bindParam(3, $search_term);
		$stmt->bindParam(4, $search_term);
		$stmt->bindParam(5, $search_term);
		$stmt->bindParam(6, $search_term);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// add member
	public function add(){
		
		// to get time-stamp for 'created' field
		//$this->getTimestamp();
		
		//write query
		$query = "INSERT INTO 
					" . $this->table_name . " 
				SET 
					fname = ?, lname = ?, phone = ?, familypositionID = ?, email = ?, phcell = ?, phwork = ?";
		
		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(1, $this->fname);
		$stmt->bindParam(2, $this->lname);
		$stmt->bindParam(3, $this->phone);
		$stmt->bindParam(4, $this->familypositionID);
		$stmt->bindParam(5, $this->email);
		$stmt->bindParam(6, $this->phcell);
		$stmt->bindParam(7, $this->phwork);
		
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
	
	// view members with field sorting
	public function readAll_WithSorting($from_record_num, $records_per_page, $field, $order){

		$query = "SELECT m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc, m.phcell, m.phwork 
					FROM members m 
						LEFT JOIN familypositions f 
							ON m.familypositionID = f.familypositionID 
					ORDER BY {$field} {$order}
					LIMIT :from_record_num, :records_per_page";

		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();

		// return values from database
		return $stmt;
	}
	
	// view members
	public function readAll($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc, m.email, m.phcell, m.phwork 
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
				ORDER BY 
					m.lname ASC, m.fname ASC
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// view members
	public function readAll_ByCategory($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc  
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
				WHERE 
					f.familypositionID=?
				ORDER BY 
					m.memberID ASC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $this->familypositionID);
		$stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// view members
	public function countAll_ByCategory(){

		// select query
		$query = "SELECT 
					COUNT(*) as total_rows 
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
				WHERE 
					m.memberID=?";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->familypositionID);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used for paging member list with field sorting
	public function countAll_WithSorting($field, $order){
		// for now countAll() is used
	}
	
	// used for paging members
	public function countAll(){
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
		
		$stmt = $this->conn->prepare( $query );
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used when filling up the update member form
	public function readOne(){
		
		$query = "SELECT 
					m.fname, m.lname, m.phone, m.familypositionID, f.familypositiondesc AS f_familypositiondesc, m.email, m.phcell, m.phwork
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
				WHERE 
					memberID = ? 
				LIMIT 
					0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->memberID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->fname = $row['fname'];
		$this->lname = $row['lname'];
		$this->phone = $row['phone'];
		$this->familypositionID = $row['familypositionID'];
		$this->f_familypositiondesc = $row['f_familypositiondesc'];
		$this->email = $row['email'];
		$this->phcell = $row['phcell'];
		$this->phwork = $row['phwork'];
	}
	
	// update the member
	public function update(){

		$query = "UPDATE 
					" . $this->table_name . " 
				SET 
					fname = :fname, 
					lname = :lname, 
					phone = :phone, 
					familypositionID = :familypositionID
					email = :email
					phcell = :phcell
					phwork = :phwork
					isTeacher = :isTeacher
					isSub = :isSub
					isHelper = :isHelper
				WHERE
					memberID = :memberID";

		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(':fname', $this->fname);
		$stmt->bindParam(':lname', $this->lname);
		$stmt->bindParam(':phone', $this->phone);
		$stmt->bindParam(':familypositionID', $this->familypositionID);
		$stmt->bindParam(':email', $this->email);
		$stmt->bindParam(':phcell', $this->phcell);
		$stmt->bindParam(':phwork', $this->phwork);
		$stmt->bindParam(':isTeacher', $this->teachercheck);
		$stmt->bindParam(':isSub', $this->subcheck);
		$stmt->bindParam(':isHelper', $this->helpercheck);
		$stmt->bindParam(':memberID', $this->memberID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete the member
	public function delete(){
	
		$query = "DELETE FROM " . $this->table_name . " WHERE memberID = ?";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(1, $this->memberID);

		if($result = $stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete selected members
	public function deleteSelected($memberIDs){
		
		$in_memberIDs = str_repeat('?,', count($memberIDs) - 1) . '?';
		
		// query to delete multiple records
		$query = "DELETE FROM " . $this->table_name . " WHERE memberID IN ({$in_memberIDs})";
		 
		$stmt = $this->conn->prepare($query);
	 
		if($stmt->execute($memberIDs)){
			return true;
		}else{
			return false;
		}
	}
	
	// used for the 'created' field when creating a member
	/*public function getTimestamp(){
		date_default_timezone_set('Asia/Manila');
		$this->timestamp = date('Y-m-d H:i:s');
	}*/
}
?>