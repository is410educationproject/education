<?php
class Product{
	
	// database connection and table name
	private $conn;
	private $table_name = "products";
	
	// object properties
	public $id;
	public $name;
	public $price;
	public $description;
	public $category_id;
	public $timestamp;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	// count records in date ranges
	public function countSearchByDateRange($date_from, $date_to){
		
		// query to count records in date ranges
		$query = "SELECT COUNT(*) as total_rows 
					FROM products 
					WHERE 
						created BETWEEN :date_from AND :date_to 
						OR created LIKE :date_from_for_query 
						OR created LIKE :date_to_for_query";

		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// bind date variables
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);
		
		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		// execute query and get total rows
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	public function searchByDateRange($date_from, $date_to, $from_record_num, $records_per_page){

		//select all data
		$query = "SELECT p.id, p.name, p.description, p.price, c.name as category_name, p.created 
					FROM " . $this->table_name . " p 
						LEFT JOIN categories c 
							ON p.category_id=c.id 
					WHERE 
						p.created BETWEEN :date_from AND :date_to 
						OR p.created LIKE :date_from_for_query 
						OR p.created LIKE :date_to_for_query
					ORDER BY created DESC
					LIMIT :from_record_num, :records_per_page";

		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);

		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();
		
		return $stmt;
	}
	
	// used to export records to csv
	public function export_CSV(){
		
		//select all data
		$query = "SELECT id, name, description, price, created, modified FROM products";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		//this is how to get number of rows returned
		$num = $stmt->rowCount();

		$out = "ID,Name,Description,Price,Created,Modified\n";

		if($num>0){
			//retrieve our table contents
			//fetch() is faster than fetchAll()
			//http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				//extract row
				//this will make $row['name'] to
				//just $name only
				extract($row);
				$out.="{$id},\"{$name}\",\"{$description}\",{$price},{$created},{$modified}\n";
			}
		}
		
		return $out;
	}
	
	// read products by search term
	public function search($search_term, $from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created 
				FROM 
					" . $this->table_name . " p
					LEFT JOIN 
						categories c
							ON p.category_id = c.id
				WHERE 
					p.name LIKE ? OR p.description LIKE ? 
				ORDER BY 
					p.name ASC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		$stmt->bindParam(2, $search_term);
		$stmt->bindParam(3, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(4, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	public function countAll_BySearch($search_term){

		// select query
		$query = "SELECT 
					COUNT(*) as total_rows 
				FROM 
					" . $this->table_name . " p
					LEFT JOIN 
						categories c
							ON p.category_id = c.id
				WHERE 
					p.name LIKE ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// create product
	public function create(){
		
		// to get time-stamp for 'created' field
		$this->getTimestamp();
		
		//write query
		$query = "INSERT INTO 
					" . $this->table_name . " 
				SET 
					name = ?, price = ?, description = ?, category_id = ?, created = ?";
		
		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(1, $this->name);
		$stmt->bindParam(2, $this->price);
		$stmt->bindParam(3, $this->description);
		$stmt->bindParam(4, $this->category_id);
		$stmt->bindParam(5, $this->timestamp);
		
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
	
	// read products with field sorting
	public function readAll_WithSorting($from_record_num, $records_per_page, $field, $order){

		$query = "SELECT p.id, p.name, p.description, p.price, c.name as category_name, p.created 
					FROM products p 
						LEFT JOIN categories c 
							ON p.category_id=c.id 
					ORDER BY {$field} {$order}
					LIMIT :from_record_num, :records_per_page";

		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();

		// return values from database
		return $stmt;
	}
	
	// read products
	public function readAll($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created 
				FROM 
					" . $this->table_name . " p
					LEFT JOIN 
						categories c
							ON p.category_id = c.id 
				ORDER BY 
					p.created DESC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// read products
	public function readAll_ByCategory($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					c.name as category_name, p.id, p.name, p.description, p.price, p.category_id, p.created  
				FROM 
					" . $this->table_name . " p
					LEFT JOIN 
						categories c
							ON p.category_id = c.id 
				WHERE 
					p.category_id=?
				ORDER BY 
					p.name ASC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $this->category_id);
		$stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// read products
	public function countAll_ByCategory(){

		// select query
		$query = "SELECT 
					COUNT(*) as total_rows 
				FROM 
					" . $this->table_name . " p
					LEFT JOIN 
						categories c
							ON p.category_id = c.id 
				WHERE 
					p.category_id=?";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->category_id);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used for paging product list with field sorting
	public function countAll_WithSorting($field, $order){
		// for now countAll() is used
	}
	
	// used for paging products
	public function countAll(){
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
		
		$stmt = $this->conn->prepare( $query );
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used when filling up the update product form
	public function readOne(){
		
		$query = "SELECT 
					name, price, description, category_id 
				FROM 
					" . $this->table_name . " 
				WHERE 
					id = ? 
				LIMIT 
					0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->id);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->name = $row['name'];
		$this->price = $row['price'];
		$this->description = $row['description'];
		$this->category_id = $row['category_id'];
	}
	
	// update the product
	public function update(){

		$query = "UPDATE 
					" . $this->table_name . " 
				SET 
					name = :name, 
					price = :price, 
					description = :description, 
					category_id  = :category_id
				WHERE
					id = :id";

		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(':name', $this->name);
		$stmt->bindParam(':price', $this->price);
		$stmt->bindParam(':description', $this->description);
		$stmt->bindParam(':category_id', $this->category_id);
		$stmt->bindParam(':id', $this->id);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete the product
	public function delete(){
	
		$query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(1, $this->id);

		if($result = $stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete selected products
	public function deleteSelected($ids){
		
		$in_ids = str_repeat('?,', count($ids) - 1) . '?';
		
		// query to delete multiple records
		$query = "DELETE FROM " . $this->table_name . " WHERE id IN ({$in_ids})";
		 
		$stmt = $this->conn->prepare($query);
	 
		if($stmt->execute($ids)){
			return true;
		}else{
			return false;
		}
	}
	
	// used for the 'created' field when creating a product
	public function getTimestamp(){
		date_default_timezone_set('Asia/Manila');
		$this->timestamp = date('Y-m-d H:i:s');
	}
}
?>