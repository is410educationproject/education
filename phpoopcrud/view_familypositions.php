<?php
$pagination = true;

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/familyposition.php';

// instantiate database and familyposition.php object
$database = new Database();
$db = $database->getConnection();

$familyposition = new FamilyPosition($db);

// header settings
$page_title = "View Family Positions";
include_once "includes/header.php";

$page_url="view_familypositions.php?";
include_once "helpers/pagination_config.php";

// query familypositions
$stmt = $familyposition->readAll($from_record_num, $records_per_page);
$num = $stmt->rowCount();

include_once "includes/familypositions_main.inc.php";

include_once "includes/footer.php";
?>