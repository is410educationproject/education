<?php
$pagination = false;

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/member.php';
include_once 'objects/contactevent.php';
include_once 'objects/contactmethod.php';
include_once 'objects/status.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate objects
$member = new Member($db);
$contact = new ContactEvent($db);
$method = new ContactMethod($db);
$status = new Status($db);

$memberID = isset($_GET['memberID']) ? $_GET['memberID'] : 1;

// set page headers
$page_title = "Add Contact Event";
include_once "includes/header.php";

include_once "includes/contactevents_add.inc.php";

include_once "includes/footer.php";
?>