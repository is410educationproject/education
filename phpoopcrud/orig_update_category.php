<?php
// include database and object files
include_once 'config/core.php';
include_once 'config/database.php';
include_once 'objects/product.php';
include_once 'objects/category.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

$product = new Product($db);
$category = new Category($db);

// get ID of the product to be edited
$id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: missing ID.');
$category->id=$id;
		
// set page title
$page_title = "Update Category";

// include page header HTML
include_once "layout_header.php";

// read category button
echo "<div class='overflow-hidden margin-bottom-1em'>";
	echo "<a href='read_categories.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> Read Categories";
	echo "</a>";
echo "</div>";

// if the form was submitted
if($_POST){

	// set category property values
	$name = $_POST['name'];
	$description = $_POST['description'];
	
	if(empty($name)){ 
		echo "<div class='alert alert-danger'>Name cannot be empty.</div>";
	}
	
	else if(empty($description)){
		echo "<div class='alert alert-danger'>Description cannot be empty.</div>";
	}
	
	else{

		// bind values
		$category->name=$name;
		$category->description=$description;
		
		// execute the query
		if($category->update()){
			
			echo "<div class=\"alert alert-success alert-dismissable\">";
				echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
				echo "Category was updated.";
			echo "</div>";
		}
		
		// if unable to update the category, tell the user
		else{
			echo "<div class=\"alert alert-danger alert-dismissable\">";
				echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
				echo "Unable to update category.";
			echo "</div>";
		}
	}
}

$category->readOne();

// assign values to object properties
$name = $category->name;
$description = $category->description;
?>
	
<!-- HTML form for updating a product -->
<form action='update_category.php?id=<?php echo $id; ?>' method='post'>
 
	<table class='table table-hover table-responsive table-bordered'>
	 
		<tr>
			<td>Name</td>
			<td><input type='text' name='name' value='<?php echo $name; ?>' class='form-control' required></td>
		</tr>
		 
		<tr>
			<td>Description</td>
			<td><textarea name='description' class='form-control'><?php echo $description; ?></textarea></td>
		</tr>
		
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update
				</button>
			</td>
		</tr>
		 
	</table>
</form>
             
<?php
// include page footer HTML
include_once "layout_footer.php";
?>