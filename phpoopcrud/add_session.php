<?php
$pagination = false;

//set page URL
$page_url = 'add_session.php?';

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/session.php';
include_once 'objects/documentation.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate session.php object
$session = new Session($db);

// set documentation
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Add Session";
include_once "includes/header.php";

include_once "includes/sessions_add.inc.php";

include_once "includes/footer.php";
?>