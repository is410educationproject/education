<!DOCTYPE html>
<html lang="en">
<head>
 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     
    <title><?php echo $page_title; ?></title>
	
    <!-- Bootstrap CSS -->
	<link href="libs/js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" media="screen" />

	<!-- jquery ui css -->
	<link rel="stylesheet" href="libs/js/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
	
	<!-- some custom CSS -->
	<style>
	.padding-bottom-2em{
		padding-bottom:2em;
	}
	
	.width-30-pct{
		width:30%;
	}
	
	.width-40-pct{
		width:40%;
	}
	
	.overflow-hidden{
		overflow:hidden;
	}
	
	.margin-right-1em{
		margin-right:1em;
	}
	
	.right-margin{
		margin:0 .5em 0 0;
	}
	
	.margin-bottom-1em {
		margin-bottom:1em;
	}
	
	.margin-zero{
		margin:0;
	}
	
	.text-align-center{
		text-align:center;
	}
	</style>
	
</head>
<body>

	<?php include_once "navigation.php"; ?>
	
    <!-- container -->
    <div class="container">
	
		<?php
		// show page header
		echo "<div class='page-header'>";
			echo "<h1>{$page_title}</h1>";
		echo "</div>";
		?>