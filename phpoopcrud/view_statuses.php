<?php


// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/status.php';

// instantiate database and status.php object
$database = new Database();
$db = $database->getConnection();

$status = new Status($db);

// header settings
$page_title = "View statuses";
include_once "includes/header.php";

$page_url="view_statuses.php?";


// query statuses
$stmt = $status->readAll();
$num = $stmt->rowCount();

include_once "includes/statuses_main.inc.php";

include_once "includes/footer.php";
?>