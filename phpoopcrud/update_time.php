<?php
$pagination = false;

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/time.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare time object
$time = new Time($db);

// set page headers
$page_title = "Update Class Times";
include_once "includes/header.php";

include_once "includes/times_update.inc.php";

include_once "includes/footer.php";
?>