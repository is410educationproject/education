<?php
$pagination = false;

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/contactmethod.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate contactmethod.php object
$contactmethod = new ContactMethod($db);

// set page headers
$page_title = "Add Contact method";
include_once "includes/header.php";

include_once "includes/contactmethods_add.inc.php";

include_once "includes/footer.php";
?>