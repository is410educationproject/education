<?php
$pagination = false;

$page_url = 'add_class.php?';

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/class.php';
include_once 'objects/time.php';
include_once 'objects/session.php';
include_once 'objects/documentation.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate class.php object
$class = new Classes($db);
$time = new Time($db);
$session = new Session($db);
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Add Class";
include_once "includes/header.php";

include_once "includes/classes_add.inc.php";

include_once "includes/footer.php";
?>