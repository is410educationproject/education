<?php
// include database and object files
include_once 'config/core.php';
include_once 'config/database.php';

include_once 'objects/product.php';
include_once 'objects/category.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

$product = new Product($db);
$category = new Category($db);

// get search term
$search_term=isset($_GET['s']) ? $_GET['s'] : '';

$page_title = "You searched for \"{$search_term}\"";
include_once "includes/header.php";

// query products
$stmt = $product->search($search_term, $from_record_num, $records_per_page);
$num = $stmt->rowCount();

$page_url="search_products.php?s={$search_term}&";
include_once "helpers/templates_members.php";

include_once "includes/footer.php";
?>