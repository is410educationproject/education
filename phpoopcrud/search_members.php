<?php
$pagination = true;
// get search term
$search_term=isset($_GET['s']) ? $_GET['s'] : '';
$page_url="search_members.php?s={$search_term}&";
// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/member.php';
include_once 'objects/familyposition.php';
include_once 'objects/documentation.php';

// instantiate database and member object
$database = new Database();
$db = $database->getConnection();

$member = new Member($db);
$familyposition = new FamilyPosition($db);
$documentation = new Documentation($page_url);




$page_title = "You searched for \"{$search_term}\"";
include_once "includes/header.php";
include_once "helpers/pagination_config.php";

// query members
$stmt = $member->search($search_term, $from_record_num, $records_per_page);
$num = $stmt->rowCount();


include_once "includes/members_main.inc.php";

include_once "includes/footer.php";
?>