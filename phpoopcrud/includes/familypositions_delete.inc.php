<?php
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/familyposition.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare familyposition.php object
	$familyposition = new FamilyPosition($db);
	
	// set familypositionID to be deleted
	$familyposition->familypositionID = $_POST['object_id'];
	
	// delete the family position
	if($familyposition->delete()){
		echo "The family position was deleted.";
	}
	
	// if unable to delete the family position
	else{
		echo "Unable to delete the family position.";	
	}
}
?>