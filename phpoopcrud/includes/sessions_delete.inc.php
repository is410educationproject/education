<?php
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/session.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare session.php object
	$session = new Session($db);
	
	// set sessionID to be deleted
	$session->sessionID = $_POST['object_id'];
	
	// delete the session
	if($session->delete()){
		echo "The session was deleted.";
	}
	
	// if unable to delete the session
	else{
		echo "Unable to delete the session.";	
	}
}
?>