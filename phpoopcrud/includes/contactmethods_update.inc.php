<?php
// view contactmethods button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='view_contactmethods.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> View contact methods";
	echo "</a>";
echo "</div>";

// get ID of the contactmethod to be edited
$methodID = isset($_GET['methodID']) ? $_GET['methodID'] : die('ERROR: missing ID.');

// set ID property of contactmethod to be edited
$contactmethod->methodID = $methodID;

// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['methodDesc'])){ 
			echo "<div class='alert alert-danger'>The contact method description cannot be empty.</div>";
		}
		
		else{
			
			// set contactmethod property values
			$contactmethod->methodDesc = $_POST['methodDesc'];

			// update the class contactmethod
			if($contactmethod->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The contact method was updated.";
				echo "</div>";
			}
			
			// if unable to update the contact method, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the contact method.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$methodDesc = $_POST['methodDesc'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of contactmethod to be edited
	$contactmethod->readOne();
}
?>
	
<!-- HTML form for updating a contactmethod -->
<form action='update_contactmethod.php?methodID=<?php echo $methodID; ?>' method='post'>
 
	<table class='table table-hover table-responsive table-bordered'>
	 
		<tr>
			<td>Contact method description</td>
			<td><input type='text' name='methodDesc' value="<?php echo htmlspecialchars($contactmethod->methodDesc, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update contact method
				</button>
			</td>
		</tr>
		 
	</table>
</form>