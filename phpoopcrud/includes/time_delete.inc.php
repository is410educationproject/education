<?php
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/time.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare time.php object
	$time = new Time($db);
	
	// set timeID to be deleted
	$time->timeID = $_POST['object_id'];
	
	// delete the time
	if($time->delete()){
		echo "The time was deleted.";
	}
	
	// if unable to delete the time
	else{
		echo "Unable to delete the time.";	
	}
}
?>