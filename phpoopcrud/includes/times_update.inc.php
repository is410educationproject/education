<?php
// view times button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='view_times.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> View times";
	echo "</a>";
echo "</div>";

// get ID of the time to be edited
$timeID = isset($_GET['timeID']) ? $_GET['timeID'] : die('ERROR: missing ID.');

// set ID property of time to be edited
$time->timeID = $timeID;

// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['timeDesc'])){ 
			echo "<div class='alert alert-danger'>The class time description cannot be empty.</div>";
		}
		
		else{
			
			// set time property values
			$time->timeDesc = $_POST['timeDesc'];

			// update the class time
			if($time->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The class time was updated.";
				echo "</div>";
			}
			
			// if unable to update the time, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the class time.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$timeDesc = $_POST['timeDesc'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of time to be edited
	$time->readOne();
}
?>
	
<!-- HTML form for updating a time -->
<form action='update_time.php?timeID=<?php echo $timeID; ?>' method='post'>
 
	<table class='table table-hover table-responsive table-bordered'>
	 
		<tr>
			<td>Class Time description</td>
			<td><input type='text' name='timeDesc' value="<?php echo htmlspecialchars($time->timeDesc, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update class time
				</button>
			</td>
		</tr>
		 
	</table>
</form>