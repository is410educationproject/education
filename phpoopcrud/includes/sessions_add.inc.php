<?php
// view sessions button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='view_sessions.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> View sessions";
	echo "</a>";
echo "</div>";
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['sessionDesc'])){ 
			echo "<div class='alert alert-danger'>Please enter a session description.</div>";
		}
		
		else{
			// set session property values
			$session->sessionDesc = $_POST['sessionDesc'];
			
			// add the session
			if($session->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The session was added.";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the session, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add the session.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
	 
<!-- HTML form for adding a session -->
<form action='add_session.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Session Description</td>
			<td><input type='text' name='sessionDesc' class='form-control' required value="<?php echo isset($_POST['sessionDesc']) ? htmlspecialchars($_POST['sessionDesc'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add session
				</button>
			</td>
		</tr>
	</table>
</form>