<?php
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/contactmethod.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare contactmethod.php object
	$contactmethod = new ContactMethod($db);
	
	// set contactmethodID to be deleted
	$contactmethod->methodID = $_POST['object_id'];
	
	// delete the contact method
	if($contactmethod->delete()){
		echo "The contact method was deleted.";
	}
	
	// if unable to delete the contact method
	else{
		echo "Unable to delete the contact method.";	
	}
}
?>