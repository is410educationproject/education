<?php
// view classes button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='view_classes.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> View Classes";
	echo "</a>";
echo "</div>";

// get ID of the class to be edited
$classID = isset($_GET['classID']) ? $_GET['classID'] : die('ERROR: missing ID.');

// set ID property of class to be edited
$class->classID = $classID;

// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['classname'])){ 
			echo "<div class='alert alert-danger'>Class name cannot be empty.</div>";
		}
		
		else if(empty($_POST['gradelevel'])){ 
			echo "<div class='alert alert-danger'>Grade level cannot be empty.</div>";
		}
		
		else if(empty($_POST['room'])){
			echo "<div class='alert alert-danger'>Room number cannot be empty.</div>";
		}
		
		else if($_POST['timeID']==0){
			echo "<div class='alert alert-danger'>Please select class time</div>";
		}
		
		else if($_POST['sessionID']==0){
			echo "<div class='alert alert-danger'>Please select session</div>";
		}
		
		else{
			
			// set class property values
			$class->classname = $_POST['classname'];
			$class->gradelevel = $_POST['gradelevel'];
			$class->room = $_POST['room'];
			$class->timeID = $_POST['timeID'];
			$class->sessionID = $_POST['sessionID'];
			
			// update the class
			if($class->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Class was updated.";
				echo "</div>";
			}
			
			// if unable to update the class, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update class.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$classname = $_POST['classname'];
		$gradelevel = $_POST['gradelevel'];
		$room = $_POST['room'];
		$timeID = $_POST['timeID'];
		$sessionID = $_POST['sessionID'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of class to be edited
	$class->readOne();
}
?>
	
<!-- HTML form for updating a class -->
<form action='update_class.php?classID=<?php echo $classID; ?>' method='post'>
 
	<table class='table table-hover table-responsive table-bordered'>
	 
		<tr>
			<td>Class name</td>
			<td><input type='text' name='classname' value="<?php echo htmlspecialchars($class->classname, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
		 
		<tr>
			<td>Grade level</td>
            <td><input type='text' name='gradelevel' value="<?php echo htmlspecialchars($class->gradelevel, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></input></td>
		</tr>
		 
		<tr>
			<td>Room Number</td>
			<td><input type='text' name='room' value="<?php echo htmlspecialchars($class->room, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></input></td>
		</tr>
		 
		<tr>
			<td>Class Time</td>
			<td>
				<?php
				$stmt = $time->read();
				
				// put them in a select drop-down
				echo "<select class='form-control' name='timeID'>";
				
					echo "<option>Please select...</option>";
					while ($row_time = $stmt->fetch(PDO::FETCH_ASSOC)){
						extract($row_time);
						
						// current time of the class must be selected
						if($class->timeID==$timeID){
							echo "<option value='$timeID' selected>";
						}else{
							echo "<option value='$timeID'>";
						}
						
						echo "$timeDesc</option>";
					}
				echo "</select>";
				?>
			</td>
		</tr>
        
        <tr>
			<td>Session</td>
			<td>
				<?php
				$stmt = $session->read();
				
				// put them in a select drop-down
				echo "<select class='form-control' name='sessionID'>";
				
					echo "<option>Please select...</option>";
					while ($row_session = $stmt->fetch(PDO::FETCH_ASSOC)){
						extract($row_session);
						
						// current time of the class must be selected
						if($class->sessionID==$sessionID){
							echo "<option value='$sessionID' selected>";
						}else{
							echo "<option value='$sessionID'>";
						}
						
						echo "$sessionDesc</option>";
					}
				echo "</select>";
				?>
			</td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update
				</button>
			</td>
		</tr>
		 
	</table>
</form>