<?php
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/member.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare member object
	$member = new Member($db);
	
	// set memberID to be deleted
	$member->memberID = $_POST['object_id'];
	
	// delete the member
	if($member->delete()){
		echo "Member was deleted.";
	}
	
	// if unable to delete the member
	else{
		echo "Unable to delete member.";	
	}
}
?>