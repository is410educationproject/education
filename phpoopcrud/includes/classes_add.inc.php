<?php
// view classes button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='view_classes.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> View Classes";
	echo "</a>";
echo "</div>";
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['classname'])){ 
			echo "<div class='alert alert-danger'>Class name cannot be empty.</div>";
		}
		
		else if(empty($_POST['gradelevel'])){ 
			echo "<div class='alert alert-danger'>Grade level cannot be empty.</div>";
		}
		
		else if(empty($_POST['room'])){
			echo "<div class='alert alert-danger'>Room number cannot be empty.</div>";
		}
		
		/*else if(!is_numeric($_POST['price'])){
			echo "<div class='alert alert-danger'>Price must be a number.</div>";
		}*/
		
		else if($_POST['timeID']==0){
			echo "<div class='alert alert-danger'>Please select a class time.</div>";
		}
		
		else if($_POST['sessionID']==0){
			echo "<div class='alert alert-danger'>Please select a class session.</div>";
		}
		
		else{
			// set class property values
			$class->classname = $_POST['classname'];
			$class->gradelevel = $_POST['gradelevel'];
			$class->room = $_POST['room'];
			$class->timeID = $_POST['timeID'];
			$class->sessionID = $_POST['sessionID'];
			
			// add the class
			if($class->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Class was added.";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the class, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add class.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
	 
<!-- HTML form for adding a class -->
<form action='classes_add.inc.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Class name</td>
			<td><input type='text' name='classname' class='form-control' required value="<?php echo isset($_POST['classname']) ? htmlspecialchars($_POST['classname'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td>Grade level</td>
			<td><input type='text' name='gradelevel' class='form-control' required value="<?php echo isset($_POST['gradelevel']) ? htmlspecialchars($_POST['gradelevel'], ENT_QUOTES) : "";  ?>" /></td>
		</tr>
		 
		<tr>
			<td>Room Number</td>
			<td><input type='text' name='room' class='form-control' required value="<?php echo isset($_POST['room']) ? htmlspecialchars($_POST['room'], ENT_QUOTES) : "";  ?>" /></td>
		</tr>
		 
		<tr>
			<td>Class time</td>
			<td>
			<?php
			// read the class times from the database
			include_once 'objects/time.php';

			$time = new Time($db);
			$stmt = $time->read();
			
			// put them in a select drop-down
			echo "<select class='form-control' name='timeID'>";
				echo "<option>Select class time...</option>";
				
				while ($row_time = $stmt->fetch(PDO::FETCH_ASSOC)){
					extract($row_time);
					echo "<option value='{$timeID}'>{$timeDesc}</option>";
				}
				
			echo "</select>";
			?>
			</td>
		</tr>
        
        <tr>
			<td>Class session</td>
			<td>
			<?php
			// read the class sessions from the database
			include_once 'objects/session.php';

			$session = new Session($db);
			$stmt = $session->read();
			
			// put them in a select drop-down
			echo "<select class='form-control' name='sessionID'>";
				echo "<option>Select class session...</option>";
				
				while ($row_session = $stmt->fetch(PDO::FETCH_ASSOC)){
					extract($row_session);
					echo "<option value='{$sessionID}'>{$sessionDesc}</option>";
				}
				
			echo "</select>";
			?>
			</td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add Class
				</button>
			</td>
		</tr>
	</table>
</form>