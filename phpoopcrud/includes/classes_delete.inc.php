<?php
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/class.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare class object
	$class = new Classes($db);
	
	// set classID to be deleted
	$class->classID = $_POST['object_id'];
	
	// delete the class
	if($class->delete()){
		echo "Class was deleted.";
	}
	
	// if unable to delete the class
	else{
		echo "Unable to delete class.";	
	}
}
?>