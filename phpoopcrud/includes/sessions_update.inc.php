<?php
// view session button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='view_sessions.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> View times";
	echo "</a>";
echo "</div>";

// get ID of the session to be edited
$sessionID = isset($_GET['sessionID']) ? $_GET['sessionID'] : die('ERROR: missing ID.');

// set ID property of time to be edited
$session->sessionID = $sessionID;

// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['sessionDesc'])){ 
			echo "<div class='alert alert-danger'>The session description cannot be empty.</div>";
		}
		
		else{
			
			// set time property values
			$session->sessionDesc = $_POST['sessionDesc'];

			// update the famil yposition
			if($session->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The session was updated.";
				echo "</div>";
			}
			
			// if unable to update the session, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the session.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$sessionDesc = $_POST['sessionDesc'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of session to be edited
	$session->readOne();
}
?>
	
<!-- HTML form for updating a time -->
<form action='update_session.php?sessionID=<?php echo $sessionID; ?>' method='post'>
 
	<table class='table table-hover table-responsive table-bordered'>
	 
		<tr>
			<td>Session Description</td>
			<td><input type='text' name='sessionDesc' value="<?php echo htmlspecialchars($session->sessionDesc, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update session
				</button>
			</td>
		</tr>
		 
	</table>
</form>