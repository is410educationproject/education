<div class='container row margin-bottom-1em'>

	
	<!-- add status button -->
	<a href='add_status.php' class="btn btn-primary pull-right margin-bottom-1em">
		<span class="glyphicon glyphicon-plus"></span> Add status
	</a>
	
	
</div>
<?php 
// display the statuses if there are any
if($num>0){

	// order opposite of the current order
	$reverse_order=isset($order) && $order=="asc" ? "desc" : "asc";
	
	// field name
	$field=isset($field) ? $field : "";
	
	// field sorting arrow
	$field_sort_html="";
	
	if(isset($field_sort) && $field_sort==true){
		$field_sort_html.="<span class='badge'>";
			$field_sort_html.=$order=="asc" 
					? "<span class='glyphicon glyphicon-arrow-up'></span>"
					: "<span class='glyphicon glyphicon-arrow-down'></span>";
		$field_sort_html.="</span>";
	}
	
	echo "<table class='table table-hover table-responsive table-bordered'>";
		echo "<tr>";
			//echo "<th class='text-align-center'><input type='checkbox' id='checker' /></th>";
			echo "<th style='width:20%;'>";
					echo "Class Status";
			echo "</th>";

			/*echo "<th>";
				echo "<a href='read_products_sorted_by_fields.php?field=created&order={$reverse_order}'>";
					echo "Created ";
					echo $field=="created" ? $field_sort_html : "";
				echo "</a>";
			echo "</th>";*/
			echo "<th>Actions</th>";
		echo "</tr>";
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		
			extract($row);
			
			echo "<tr>";
				//echo "<td class='text-align-center'><input type='checkbox' name='item[]' class='checkboxes' value='{$statusID}' /></td>";
				echo "<td>{$statusDesc}</td>";
				echo "<td>";
					
					// edit status button
					echo "<a href='update_status.php?statusID={$statusID}' class='btn btn-sm btn-info right-margin'>";
						echo "<span class='glyphicon glyphicon-edit'></span>";
					echo "</a>";
					
					// delete status button
					echo "<a delete-id='{$statusID}' delete-file='includes/statuses_delete.inc.php' class='btn btn-sm btn-danger delete-object'>";
						echo "<span class='glyphicon glyphicon-remove'></span>";
					echo "</a>";
					
				echo "</td>";
				
			echo "</tr>";
			
		}
		
	echo "</table>";

	
}

// tell the user there are no statuses
else{
	echo "<div class=\"alert alert-danger alert-dismissable\">";
		echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&statuses;</button>";
		echo "No statuses found.";
	echo "</div>";
}
?>