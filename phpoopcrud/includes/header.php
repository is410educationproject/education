<?php
	//$documentation->readDocumentation($page_url);
?>

<!DOCTYPE html>
<html lang="en">
<head>
 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     
    <title><?php echo $page_title; ?></title>
	
    <!-- Bootstrap CSS -->
	<link href="libs/js/bootstrap/dist/css/bootstrap.css" rel="stylesheet" media="screen" />
    <link rel="stylesheet" href="../../css/bootstrap-switch.css" />
	<!-- jquery ui css -->
	<link rel="stylesheet" href="libs/js/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
    <link rel="stylesheet" href="../../css/custom.css" />
	
	<!-- some custom CSS -->
	<style>
	.padding-bottom-2em{
		padding-bottom:2em;
	}
	
	.width-30-pct{
		width:30%;
	}
	
	.width-40-pct{
		width:40%;
	}
	
	.overflow-hidden{
		overflow:hidden;
	}
	
	.margin-right-1em{
		margin-right:1em;
	}
	
	.margin-left-1em{
		margin-left:1em;
	}
	
	.right-margin{
		margin:0 .5em 0 0;
	}
	
	.margin-bottom-1em {
		margin-bottom:1em;
	}
	
	.margin-zero{
		margin:0;
	}
	
	.text-align-center{
		text-align:center;
	}
	</style>
	
</head>
<body>

	<?php //include_once "navigation.php"; ?>
	
    <!-- container -->
    <div class="container outercontainer">
	
		<?php
		// show page header
		echo "<div class='page-header'>";
			echo "<h1>{$page_title}<a data-toggle='modal' data-target='#helpmodal' class='btn btn-primary pull-right margin-bottom-1em margin-left-1em'>
				<span class='glyphicon glyphicon-question-sign'></span></a>
				<a href='../index.php' class='btn btn-primary pull-right margin-bottom-1em margin-left-1em'>
				<span class='glyphicon glyphicon-home'></span></a>
				</h1>";
		echo "</div>";
		?>
        <div class="modal fade" 
                        id="helpmodal" 
                        tabindex="-1" 
                        role="dialog" 
                        aria-labelledby="helpmodalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">
                                        Need help with <?php echo "{$page_title}";?>?
                                    </h4>
                                </div>                  
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="contactdate">Contact Date</label>
                                            <br />
                                            <?php echo $documentation->readDocumentation($page_url); ?>
                                            <br />
                                            <br />
                                            <label for="methodID">Method of Contact</label>
                                            <br />
                                            <?php //echo "{$cm_methodDesc}"; ?>
                                            <br />
                                            <br />
                                            <label for="notes">Notes</label>
                                            <br />
                                            <?php //echo "{$notes}"; ?>
                                            <br />
                                            <br />
                                            <label for="statusID">Status</label>
                                            <br />
                                            <?php //echo "{$st_statusDesc}"; ?>

                                            <?php //echo $member['m_lname']; ?>
                                        </div>
									</form>
                                </div>             
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                