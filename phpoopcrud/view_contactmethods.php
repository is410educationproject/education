<?php


// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/contactmethod.php';

// instantiate database and contactmethod.php object
$database = new Database();
$db = $database->getConnection();

$method = new ContactMethod($db);

// header settings
$page_title = "View contact methods";
include_once "includes/header.php";

$page_url="view_contactmethods.php?";


// query contactmethods
$stmt = $method->readAll();
$num = $stmt->rowCount();

include_once "includes/contactmethods_main.inc.php";

include_once "includes/footer.php";
?>