<?php
$pagination = false;

$page_url = "add_member.php";

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/member.php';
include_once 'objects/familyposition.php';
include_once 'objects/documentation.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate member.php object
$member = new Member($db);
$familyposition = new FamilyPosition($db);
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Add Member";
include_once "includes/header.php";

include_once "includes/members_add.inc.php";

include_once "includes/footer.php";
?>