<?php
$pagination = false;

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/status.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare status object
$status = new Status($db);

// set page headers
$page_title = "Update Statuses";
include_once "includes/header.php";

include_once "includes/statuses_update.inc.php";

include_once "includes/footer.php";
?>