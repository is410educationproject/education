<?php
// get database connection
include_once 'config/core.php';
include_once 'config/database.php';
include_once 'objects/product.php';
include_once 'objects/category.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate product object
$product = new Product($db);
$category = new Category($db);

// set page headers
$page_title = "Create Product";
include_once "layout_header.php";

// read products button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='read_products.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> Read Products";
	echo "</a>";
echo "</div>";
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['name'])){ 
			echo "<div class='alert alert-danger'>Name cannot be empty.</div>";
		}
		
		else if(empty($_POST['description'])){ 
			echo "<div class='alert alert-danger'>Description cannot be empty.</div>";
		}
		
		else if(empty($_POST['price'])){
			echo "<div class='alert alert-danger'>Price cannot be empty.</div>";
		}
		
		else if(!is_numeric($_POST['price'])){
			echo "<div class='alert alert-danger'>Price must be a number.</div>";
		}
		
		else if($_POST['category_id']==0){
			echo "<div class='alert alert-danger'>Please select category</div>";
		}
		
		else{
			// set product property values
			$product->name = $_POST['name'];
			$product->price = $_POST['price'];
			$product->description = $_POST['description'];
			$product->category_id = $_POST['category_id'];
			
			// create the product
			if($product->create()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Product was created.";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to create the product, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to create product.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
	 
<!-- HTML form for creating a product -->
<form action='create_product.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Name</td>
			<td><input type='text' name='name' class='form-control' required value="<?php echo isset($_POST['name']) ? htmlspecialchars($_POST['name'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td>Price</td>
			<td>
				<!-- step="0.01" was used so that it can accept number with two decimal places -->
				<input type='number' step="0.01" name='price' class='form-control' required value="<?php echo isset($_POST['price']) ? htmlspecialchars($_POST['price'], ENT_QUOTES) : "";  ?>" />
			</td>
		</tr>
		 
		<tr>
			<td>Description</td>
			<td><textarea type='text' name='description' class='form-control' required><?php echo isset($_POST['description']) ? htmlspecialchars($_POST['description'], ENT_QUOTES) : "";  ?></textarea></td>
		</tr>
		 
		<tr>
			<td>Category</td>
			<td>
			<?php
			// read the product categories from the database
			include_once 'objects/category.php';

			$category = new Category($db);
			$stmt = $category->read();
			
			// put them in a select drop-down
			echo "<select class='form-control' name='category_id'>";
				echo "<option>Select category...</option>";
				
				while ($row_category = $stmt->fetch(PDO::FETCH_ASSOC)){
					extract($row_category);
					echo "<option value='{$id}'>{$name}</option>";
				}
				
			echo "</select>";
			?>
			</td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Create
				</button>
			</td>
		</tr>
	</table>
</form>
             
<?php
include_once "layout_footer.php";
?>