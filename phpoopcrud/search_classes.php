<?php
// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/class.php';

// instantiate database and class.php object
$database = new Database();
$db = $database->getConnection();

$class = new Classes($db);

// get search term
$search_term=isset($_GET['s']) ? $_GET['s'] : '';

$page_title = "You searched for \"{$search_term}\"";
include_once "includes/header.php";

include_once "helpers/pagination_config.php";

// query classes
$stmt = $class->search($search_term, $from_record_num, $records_per_page);
$num = $stmt->rowCount();

$page_url="search_classes.php?s={$search_term}&";
include_once "includes/classes_main.inc.php";

include_once "includes/footer.php";
?>