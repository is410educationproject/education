<?php 
// include database and object files
include_once 'helpers/config.php';
include_once 'objects/member.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

$member = new Member($db);

header("Content-type: text/x-csv");
header("Content-Disposition: attachment; filename=all_products_" . date('Y-m-d_H-i-s') . ".csv");
echo $member->export_CSV();
?>