<?php
$pagination = true;

$page_url="view_members.php?";

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/member.php';
include_once 'objects/familyposition.php';
include_once 'objects/documentation.php';

// instantiate database and member object
$database = new Database();
$db = $database->getConnection();

$member = new Member($db);
$familyposition = new FamilyPosition($db);
$documentation = new Documentation($page_url);

// header settings
$page_title = "View Members";
include_once "includes/header.php";

include_once "helpers/pagination_config.php";

// query members
$stmt = $member->readAll($from_record_num, $records_per_page);
$num = $stmt->rowCount();

include_once "includes/members_main.inc.php";

include_once "includes/footer.php";
?>