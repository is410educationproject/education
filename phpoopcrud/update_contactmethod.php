<?php
$pagination = false;

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/contactmethod.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare contactmethod object
$contactmethod = new ContactMethod($db);

// set page headers
$page_title = "Update Contact Method";
include_once "includes/header.php";

include_once "includes/contactmethods_update.inc.php";

include_once "includes/footer.php";
?>