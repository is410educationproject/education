<?php
$pagination = false;

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/session.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare session object
$session = new Session($db);

// set page headers
$page_title = "Update Session";
include_once "includes/header.php";

include_once "includes/sessions_update.inc.php";

include_once "includes/footer.php";
?>