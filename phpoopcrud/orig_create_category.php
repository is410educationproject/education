<?php
// include database and object files
include_once 'config/core.php';
include_once 'config/database.php';
include_once 'objects/product.php';
include_once 'objects/category.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

$product = new Product($db);
$category = new Category($db);

// set page title
$page_title = "Create Category";

// import page header HTML
include_once "layout_header.php";

// read products button
echo "<div class='overflow-hidden margin-bottom-1em'>";
	echo "<a href='read_categories.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> Read Categories";
	echo "</a>";
echo "</div>";

// if the form was submitted
if($_POST){

	// set category property values
	$name=$_POST['name'];
	$description=$_POST['description'];
	$created=date('Y-m-d H:i:s');
	
	if(empty($name)){ 
		echo "<div class='alert alert-danger'>Name cannot be empty.</div>";
	}
	
	else if(empty($description)){
		echo "<div class='alert alert-danger'>Description cannot be empty.</div>";
	}
	
	else{
		
		$category->name=$name;
		$category->description=$description;
		$category->created=$created;
		
		// execute query
		if($category->create()){

			// tell the user new category was created
			echo "<div class=\"alert alert-success alert-dismissable\">";
				echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
				echo "Category was created.";
			echo "</div>";
		}
		
		// if unable to create the category, tell the user
		else{
			echo "<div class=\"alert alert-danger alert-dismissable\">";
				echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
				echo "Unable to create category.";
			echo "</div>";
		}
	}
}
?>
	 
<!-- HTML form for creating a category -->
<form action='create_category.php' method='post'>
 
	<table class='table table-hover table-responsive table-bordered'>
	 
		<tr>
			<td>Name</td>
			<td><input type='text' name='name' class='form-control' required></td>
		</tr>
		 
		<tr>
			<td>Description</td>
			<td><textarea name='description' class='form-control' required></textarea></td>
		</tr>
		
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-plus'></span> Create
				</button>
			</td>
		</tr>
		 
	</table>
</form>
		
<?php
// include page footer HTML
include_once "layout_footer.php";
?>