<?php
// check if value was posted
if($_POST){

	// include database and object files
	include_once 'config/database.php';
	include_once 'objects/category.php';

	// instantiate database and product object
	$database = new Database();
	$db = $database->getConnection();

	$category = new Category($db);
	
	// set category id to be deleted
	$category->id=$_POST['object_id'];

	// execute the query
	if($category->delete()){
		echo "Object was deleted.";
	}
	
	// if unable to delete the product, tell the user
	else{
		echo "Unable to delete object.";
	}
}
?>