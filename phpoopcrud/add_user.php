<?php
$pagination = false;

$page_url = "add_user.php";

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/user.php';
include_once 'objects/documentation.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate user.php object
$user = new User($db);
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Add User";
include_once "includes/header.php";

include_once "includes/users_add.inc.php";

include_once "includes/footer.php";
?>