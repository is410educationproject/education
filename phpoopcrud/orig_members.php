<?php
// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/member.php';
include_once 'objects/familyposition.php';

// instantiate database and member object
$database = new Database();
$db = $database->getConnection();

$member = new Member($db);
$familyposition = new FamilyPosition($db);

// header settings
$page_title = "View Members";
include_once "includes/header.php";

// query members
$stmt = $member->readAll($from_record_num, $records_per_page);
$num = $stmt->rowCount();

$page_url="members.php?";
include_once "includes/members_main.inc.php";

include_once "includes/footer.php";
?>