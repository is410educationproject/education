<?php
$pagination = false;

$page_url = 'update_class.php?';

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/class.php';
include_once 'objects/time.php';
include_once 'objects/session.php';
include_once 'objects/documentation.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare class.php object
$class = new Classes($db);
$time = new Time($db);
$session = new Session($db);
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Update Class";
include_once "includes/header.php";

include_once "includes/classes_update.inc.php";

include_once "includes/footer.php";
?>