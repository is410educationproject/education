<?php
// include database and object files
include_once 'config/core.php';
include_once 'config/database.php';
include_once 'objects/product.php';
include_once 'objects/category.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

$product = new Product($db);
$category = new Category($db);

// get search term
$search_term=isset($_GET['s']) ? $_GET['s'] : '';

// set page title
$page_title = "Category Search Results";

// include page header HTML
include_once "layout_header.php";

$stmt=$category->searchAll($search_term, $from_record_num, $records_per_page);

// count number of categories returned
$num = $stmt->rowCount();

// to identify page for paging
$page_url="search_categories.php?s={$search_term}&";

// include categories table HTML template
include_once "read_categories_template.php";

// include page footer HTML
include_once "layout_footer.php";
?>