<?php
$pagination = false;

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/familyposition.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare familyposition.php object
$familyposition = new FamilyPosition($db);

// set page headers
$page_title = "Update Family Position";
include_once "includes/header.php";

include_once "includes/familypositions_update.inc.php";

include_once "includes/footer.php";
?>