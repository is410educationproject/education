<?php
$pagination = false;

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/time.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate time.php object
$time = new Time($db);

// set page headers
$page_title = "Add Class Time";
include_once "includes/header.php";

include_once "includes/times_add.inc.php";

include_once "includes/footer.php";
?>