<?php
// include database and object files
include_once 'config/core.php';
include_once 'config/database.php';
include_once 'objects/product.php';
include_once 'objects/category.php';

// instantiate database and product object
$database = new Database();
$db = $database->getConnection();

$product = new Product($db);
$category = new Category($db);

// get category id
$category_id=isset($_GET['id']) ? $_GET['id'] : die('Category ID not found.');

// get category name
$category->id=$category_id;
$category->readNameById();
$category_name=$category->name;

// header settings
$page_title = $category_name;
include_once "layout_header.php";

$product->category_id=$category_id;

// query products
$stmt = $product->readAll_ByCategory($from_record_num, $records_per_page);
$num = $stmt->rowCount();

$page_url="category.php?id={$category_id}&";
include_once "read_products_template.php";

include_once "layout_footer.php";
?>