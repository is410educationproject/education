<?php


// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/time.php';

// instantiate database and time.php object
$database = new Database();
$db = $database->getConnection();

$time = new Time($db);

// header settings
$page_title = "View times";
include_once "includes/header.php";

$page_url="view_times.php?";


// query times
$stmt = $time->readAll();
$num = $stmt->rowCount();

include_once "includes/times_main.inc.php";

include_once "includes/footer.php";
?>