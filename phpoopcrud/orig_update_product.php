<?php
// include database and object files
include_once 'config/core.php';
include_once 'config/database.php';
include_once 'objects/product.php';
include_once 'objects/category.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare product object
$product = new Product($db);
$category = new Category($db);

// set page headers
$page_title = "Update Product";
include_once "layout_header.php";

// read products button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='read_products.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> Read Products";
	echo "</a>";
echo "</div>";

// get ID of the product to be edited
$id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: missing ID.');

// set ID property of product to be edited
$product->id = $id;

// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['name'])){ 
			echo "<div class='alert alert-danger'>Name cannot be empty.</div>";
		}
		
		else if(empty($_POST['description'])){ 
			echo "<div class='alert alert-danger'>Description cannot be empty.</div>";
		}
		
		else if(empty($_POST['price'])){
			echo "<div class='alert alert-danger'>Price cannot be empty.</div>";
		}
		
		else if(!is_numeric($_POST['price'])){
			echo "<div class='alert alert-danger'>Price must be a number.</div>";
		}
		
		else if($_POST['category_id']==0){
			echo "<div class='alert alert-danger'>Please select category</div>";
		}
		
		else{
			
			// set product property values
			$product->name = $_POST['name'];
			$product->price = $_POST['price'];
			$product->description = $_POST['description'];
			$product->category_id = $_POST['category_id'];
			
			// update the product
			if($product->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Product was updated.";
				echo "</div>";
			}
			
			// if unable to update the product, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update product.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$name = $_POST['name'];
		$description = $_POST['description'];
		$price = $_POST['price'];
		$category_id = $_POST['category_id'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of product to be edited
	$product->readOne();
}
?>
	
<!-- HTML form for updating a product -->
<form action='update_product.php?id=<?php echo $id; ?>' method='post'>
 
	<table class='table table-hover table-responsive table-bordered'>
	 
		<tr>
			<td>Name</td>
			<td><input type='text' name='name' value="<?php echo htmlspecialchars($product->name, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
		 
		<tr>
			<td>Price</td>
            <td>
				<!-- step="0.01" was used so that it can accept number with two decimal places -->
				<input type='number' step="0.01" name='price' value="<?php echo htmlspecialchars($product->price, ENT_QUOTES, 'UTF-8');  ?>" class='form-control' required />
			</td>
		</tr>
		 
		<tr>
			<td>Description</td>
			<td><textarea name='description' class='form-control'><?php echo htmlspecialchars($product->description, ENT_QUOTES, 'UTF-8'); ?></textarea></td>
		</tr>
		 
		<tr>
			<td>Category</td>
			<td>
				<?php
				
				$stmt = $category->read();
				
				// put them in a select drop-down
				echo "<select class='form-control' name='category_id'>";
				
					echo "<option>Please select...</option>";
					while ($row_category = $stmt->fetch(PDO::FETCH_ASSOC)){
						extract($row_category);
						
						// current category of the product must be selected
						if($product->category_id==$id){
							echo "<option value='$id' selected>";
						}else{
							echo "<option value='$id'>";
						}
						
						echo "$name</option>";
					}
				echo "</select>";
				?>
			</td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update
				</button>
			</td>
		</tr>
		 
	</table>
</form>
             
<?php
include_once "layout_footer.php";
?>