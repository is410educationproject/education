<?php
$pagination = false;

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/status.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate status.php object
$status = new Status($db);

// set page headers
$page_title = "Add Status";
include_once "includes/header.php";

include_once "includes/statuses_add.inc.php";

include_once "includes/footer.php";
?>