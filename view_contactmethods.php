<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$globalpage = true;
$page_url = "view_contactmethods.php?";

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/contactmethod.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';
include_once 'objects/term.php';

// instantiate database and contactmethod.php object
$database = new Database();
$db = $database->getConnection();

$contactmethod = new ContactMethod($db);
$userobject = new UserObject($db);
$term = new Term($db);

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';

// get documentation
$documentation = new Documentation($page_url);

// header settings
$page_title = "View contact methods";
include_once "includes/header_param.php";

// add the term selector to the page
include_once 'includes/termselect.inc.php';

// query contactmethods
$stmt = $contactmethod->readAll();
$num = $stmt->rowCount();

include_once "includes/contactmethods_main.inc.php";

include_once "includes/footer.php";
?>