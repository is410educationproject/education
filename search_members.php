<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();

if (!$user->logged_in()){
	header("location:login.php");
}

$pagination = true;

// set page url
$page_url = 'search_members.php?';

// include database and object files
include_once 'helpers/config.php';
include_once "helpers/pagination_config.php";
include_once 'includes/db_connect.php';
include_once 'objects/member.php';
include_once 'objects/familyposition.php';
include_once 'objects/documentation.php';

// instantiate database and member object
$database = new Database();
$db = $database->getConnection();

$member = new Member($db);
$familyposition = new FamilyPosition($db);
$documentation = new Documentation($page_url);

// get search term
$search_term=isset($_GET['s']) ? $_GET['s'] : '';

$page_title = "You searched for \"{$search_term}\"";
include_once "includes/header.php";

// query members
$stmtMembers = $member->search($search_term, $from_record_num, $records_per_page);
$num = $stmtMembers->rowCount();

$page_url="search_members.php?s={$search_term}&";
include_once "includes/members_main.inc.php";

include_once "includes/footer.php";
?>