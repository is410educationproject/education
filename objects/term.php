<?php
class Term{
	
	// database connection and table name
	private $conn;
	private $table_name = "term";
	
	// object properties
	public $termID;
	public $termDesc;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	public function readOne(){
		// read the details of term to be edited
		// select single record query
		$query = "SELECT termID, termDesc 
				FROM " . $this->table_name . "  
				WHERE termID = ? 
				LIMIT 0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->termID);

		// execute the query
		$stmt->execute();

		// get record details
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// assign values to object properties
		$this->termDesc = $row['termDesc'];
	}
	
	public function update(){
		// update the term
		$query = "UPDATE " . $this->table_name . "   
				SET termDesc = :termDesc
				WHERE termID = :termID";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(':termDesc', $this->termDesc);
		$stmt->bindParam(':termID', $this->termID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function delete(){
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE termID = ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);
			
		// bind record id
		$stmt->bindParam(1, $this->termID);

		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function add(){
		// add the term
		// insert query
		$query = "INSERT INTO term  
				SET termDesc = ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(1, $this->termDesc);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function searchAll($search_term, $from_record_num, $records_per_page){
		// search terms based on search term
		// search query
		$query = "SELECT termID, termDesc 
				FROM " . $this->table_name . " 
				WHERE termDesc LIKE ? 
				ORDER BY termID ASC 
				LIMIT ?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind  variables
		$query_search_term = "%{$search_term}%";

		$stmt->bindParam(1, $query_search_term);
		$stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();	

		return $stmt;
	}
	
	// count all familypositions
	public function countAll(){
		// query to count all data
		$query = "SELECT COUNT(*) as total_rows FROM familypositions";
	 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		return $total_rows;
	}
	
	// count all terms with search term
	public function countAll_WithSearch($search_term){
		// search query
		$query = "SELECT COUNT(*) as total_rows FROM term WHERE termDesc LIKE ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// bind search term
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		return $total_rows;
	}
	
	// read all with paging
	public function readAll_WithPaging($from_record_num, $records_per_page){
		// read all terms from the database
		$query = "SELECT termID, termDesc 
				FROM " . $this->table_name . " 
				ORDER BY termID DESC  
				LIMIT ?, ?";	

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind values
		$stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();
		
		return $stmt;
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					termID, termDesc 
				FROM 
					" . $this->table_name . " 
				ORDER BY 
					termID ASC";	
		
		$stmt = $this->conn->prepare( $query );		
		$stmt->execute();
		
		return $stmt;	
	}
	
	// used by select drop-down list
	public function readTermSelector(){		
		//select all data		
		$query = "SELECT
					termID, termDesc 
				FROM 
					" . $this->table_name . " 
				ORDER BY 
					termID ASC";	
		
		$stmt = $this->conn->prepare( $query );		
		$stmt->execute();
		
		return $stmt;	
	}
	
	// view terms
	public function readAll(){

		// select query
		$query = "SELECT 
					termID, termDesc 
				FROM 
					" . $this->table_name . "
				ORDER BY 
					termID ASC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// used to read term description by its ID
	function readNameById(){
		
		$query = "SELECT termDesc FROM " . $this->table_name . " WHERE termID = ? limit 0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->termID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->termDesc = $row['termDesc'];
	}
}
?>