<?php
class Preference{
	
	// database connection and table name
	private $conn;
	private $table_name = "preferences";
	
	// object properties
	public $preferenceID;
	public $positionID;
	public $departmentID;
	public $memberID;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	
 /*?>//	<?php used to export records to csv
	public function export_CSV(){
		
		//select all data
		//$query = "SELECT memberID, fname, lname, phone, created, modified FROM products";
		$query = "SELECT preferenceID, positionID, departmentID, memberID FROM preferences";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		//this is how to get number of rows returned
		$num = $stmt->rowCount();

		$out = "MemberID,First Name,Last Name,Phone\n";

		if($num>0){
			//retrieve our table contents
			//fetch() is faster than fetchAll()
			//http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				//extract row
				//this will make $row['name'] to
				//just $name only
				extract($row);
				$out.="{$memberID},\"{$fname}\",\"{$lname}\",{$phone}\n";
			}
		}
		
		return $out;
	}*/
	
//NEED TO DO THIS!!!	

	// add member preference
	public function add(){
		
		// to get time-stamp for 'created' field
		//$this->getTimestamp();
		
		//write query
		$query = "INSERT INTO 
					" . $this->table_name . " 
				SET 
					memberID = ?, positionID = ?, departmentID = ?";
		
		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(1, $this->memberID);
		$stmt->bindParam(2, $this->positionID);
		$stmt->bindParam(3, $this->departmentID);
		
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}

	
	// view preferences
	public function readAll_Preferences($memberID){

		// select query
		$query = "SELECT 
					p.positionID, p.departmentID, p.memberID, d.departmentdesc as d_departmentdesc, sp.positionDesc as sp_positionDesc, p.preferenceID as p_preferenceID
				FROM 
					" . $this->table_name . " p
					LEFT JOIN members m 
						ON p.memberID = m.memberID
					LEFT JOIN departments d 	
						ON p.departmentID = d.departmentID
					LEFT JOIN staffpositions sp 	
						ON p.positionID = sp.positionID
				WHERE
					p.memberID = ?
				ORDER BY 
					p.positionID ASC, d_departmentdesc ASC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $memberID, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	

	// used for paging preferences
	public function countAll(){
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
		
		$stmt = $this->conn->prepare( $query );
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used when filling up the update member form
	public function readOne(){
		
		$query = "SELECT 
					positionID, departmentID, memberID
				FROM 
					" . $this->table_name . " 
				WHERE 
					memberID = ? 
				LIMIT 
					0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->memberID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		$this->positionID = $row['positionID'];
		$this->departmentID = $row['departmentID'];
		$this->memberID = $row['memberID'];
		
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					departmentID, departmentDesc
				FROM 
					" . $this->table_name . " 
				ORDER BY 
					methodID";	
		
		$stmt = $this->conn->prepare( $query );		
		$stmt->execute();
		
		return $stmt;	
	}
	
	// update the contact event
	public function update(){

		$query = "UPDATE 
					" . $this->table_name . " 
				SET 
					contactdate = :contactdate, 
					methodID = :methodID, 
					notes = :notes, 
					statusID = :statusID
				WHERE
					contacteventID = :contacteventID";

		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(':contactdate', $this->contactdate);
		$stmt->bindParam(':methodID', $this->methodID);
		$stmt->bindParam(':notes', $this->notes);
		$stmt->bindParam(':statusID', $this->statusID);
		$stmt->bindParam(':contacteventID', $this->contacteventID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete the preference
	public function delete($memberID){
	
		$query = "DELETE FROM " . $this->table_name . " WHERE preferenceID = ?";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(1, $this->preferenceID);

		if($result = $stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
}
	
?>