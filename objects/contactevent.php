<?php
class ContactEvent{
	
	// database connection and table name
	private $conn;
	private $table_name = "contactevents";
	
	// object properties
	public $contacteventID;
	public $memberID;
	public $contactdate;
	public $methodID;
	public $notes;
	public $teacherstatusID;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	// count records in date ranges
	/*public function countSearchByDateRange($date_from, $date_to){
		
		// query to count records in date ranges
		$query = "SELECT COUNT(*) as total_rows 
					FROM members 
					WHERE 
						created BETWEEN :date_from AND :date_to 
						OR created LIKE :date_from_for_query 
						OR created LIKE :date_to_for_query";

		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// bind date variables
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);
		
		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		// execute query and get total rows
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}*/
	
	public function searchByDateRange($date_from, $date_to, $from_record_num, $records_per_page){

		//select all data
		$query = "SELECT m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc as f_familypositiondesc 
					FROM " . $this->table_name . " m 
						LEFT JOIN familypositions f 
							ON m.familypositionID=f.familypositionID 
					ORDER BY memberID ASC
					LIMIT :from_record_num, :records_per_page";
					/*WHERE 
						p.created BETWEEN :date_from AND :date_to 
						OR p.created LIKE :date_from_for_query 
						OR p.created LIKE :date_to_for_query*/
						
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);

		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();
		
		return $stmt;
	}
	
	// used to export records to csv
	public function export_CSV(){
		
		//select all data
		//$query = "SELECT memberID, fname, lname, phone, created, modified FROM products";
		$query = "SELECT memberID, fname, lname, phone FROM members";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		//this is how to get number of rows returned
		$num = $stmt->rowCount();

		$out = "MemberID,First Name,Last Name,Phone\n";

		if($num>0){
			//retrieve our table contents
			//fetch() is faster than fetchAll()
			//http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				//extract row
				//this will make $row['name'] to
				//just $name only
				extract($row);
				$out.="{$memberID},\"{$fname}\",\"{$lname}\",{$phone}\n";
			}
		}
		
		return $out;
	}
	
//NEED TO DO THIS!!!	
	
	// view members by search term
	public function search($search_term, $from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					f.familypositiondesc as f_familypositiondesc, m.memberID, m.fname, m.lname, m.phone, m.familypositionID 
				FROM 
					" . $this->table_name . " m
					LEFT JOIN 
						familypositions f
							ON m.familypositionID = f.familypositionID
				WHERE 
					m.fname LIKE ? OR m.lname LIKE ? 
				ORDER BY 
					m.memberID ASC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		$stmt->bindParam(2, $search_term);
		$stmt->bindParam(3, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(4, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	public function countAll_BySearch($search_term){

		// select query
		$query = "SELECT 
					COUNT(*) as total_rows 
				FROM 
					" . $this->table_name . " m
					LEFT JOIN 
						familypositions f
							ON m.familypositionID = f.familypositionID
				WHERE 
					m.fname LIKE ? OR m.lname LIKE ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		$stmt->bindParam(2, $search_term);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// add contact event
	public function add(){
		
		// write query
		$query = "INSERT INTO 
					" . $this->table_name . " 
				SET 
					memberID = ?, contactdate = ?, methodID = ?, notes = ?, statusID = ?";
		
		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(1, $this->memberID);
		$stmt->bindParam(2, $this->contactdate);
		$stmt->bindParam(3, $this->methodID);
		$stmt->bindParam(4, $this->notes);
		$stmt->bindParam(5, $this->statusID);
		
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
	
	// view contact events with field sorting
	public function readAll_WithSorting($from_record_num, $records_per_page, $field, $order){

		$query = "SELECT m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc 
					FROM members m 
						LEFT JOIN familypositions f 
							ON m.familypositionID = f.familypositionID 
					ORDER BY {$field} {$order}
					LIMIT :from_record_num, :records_per_page";

		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();

		// return values from database
		return $stmt;
	}
	
	// view contact events
	public function readAll($memberID){

		// select query
		$query = "SELECT 
					ce.contacteventID, m.memberID AS m_memberID, m.lname AS m_lname, m.fname AS m_fname, ce.contactdate, cm.methodDesc AS cm_methodDesc, ce.notes, st.statusDesc AS st_statusDesc
				FROM 
					" . $this->table_name . " ce
					LEFT JOIN members m 
						ON ce.memberID = m.memberID
					LEFT JOIN contactmethods cm
						ON ce.methodID = cm.methodID
					LEFT JOIN status st
						ON ce.statusID = st.statusID
				WHERE
					ce.memberID = ?
				ORDER BY 
					ce.contactdate DESC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $memberID, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// view members
	public function readAll_ByCategory($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc  
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
				WHERE 
					f.familypositionID=?
				ORDER BY 
					m.memberID ASC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $this->familypositionID);
		$stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// view members
	public function countAll_ByCategory(){

		// select query
		$query = "SELECT 
					COUNT(*) as total_rows 
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
				WHERE 
					m.memberID=?";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->familypositionID);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used for paging member list with field sorting
	public function countAll_WithSorting($field, $order){
		// for now countAll() is used
	}
	
	// used for paging contact events
	public function countAll(){
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
		
		$stmt = $this->conn->prepare( $query );
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used when filling up the update member form
	public function readOne(){
		
		$query = "SELECT 
					contactdate, methodID, notes, statusID 
				FROM 
					" . $this->table_name . " 
				WHERE 
					contacteventID = ? 
				LIMIT 
					0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->contacteventID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->contactdate = $row['contactdate'];
		$this->methodID = $row['methodID'];
		$this->notes = $row['notes'];
		$this->statusID = $row['statusID'];
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					methodID, methodDesc 
				FROM 
					" . $this->table_name . " 
				ORDER BY 
					methodID";	
		
		$stmt = $this->conn->prepare( $query );		
		$stmt->execute();
		
		return $stmt;	
	}
	
	// update the contact event
	public function update(){

		$query = "UPDATE 
					" . $this->table_name . " 
				SET 
					contactdate = :contactdate, 
					methodID = :methodID, 
					notes = :notes, 
					statusID = :statusID
				WHERE
					contacteventID = :contacteventID";

		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(':contactdate', $this->contactdate);
		$stmt->bindParam(':methodID', $this->methodID);
		$stmt->bindParam(':notes', $this->notes);
		$stmt->bindParam(':statusID', $this->statusID);
		$stmt->bindParam(':contacteventID', $this->contacteventID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete the contact event
	public function delete(){
	
		$query = "DELETE FROM " . $this->table_name . " WHERE contacteventID = ?";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(1, $this->contacteventID);

		if($result = $stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete selected members
	public function deleteSelected($memberIDs){
		
		$in_memberIDs = str_repeat('?,', count($memberIDs) - 1) . '?';
		
		// query to delete multiple records
		$query = "DELETE FROM " . $this->table_name . " WHERE memberID IN ({$in_memberIDs})";
		 
		$stmt = $this->conn->prepare($query);
	 
		if($stmt->execute($memberIDs)){
			return true;
		}else{
			return false;
		}
	}
	
	// used for the 'created' field when creating a member
	/*public function getTimestamp(){
		date_default_timezone_set('Asia/Manila');
		$this->timestamp = date('Y-m-d H:i:s');
	}*/
}
?>