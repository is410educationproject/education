<?php
class StaffAssignment{
	
	// database connection and table name
	private $conn;
	private $table_name = "staffassign";
	
	// object properties
	public $assignmentID;
	public $classID;
	public $memberID;
	public $positionID;
	public $activeuserID;
	public $termID;
	public $departmentID;
	public $m_fname;
	public $m_lname;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	// count records in date ranges
	/*public function countSearchByDateRange($date_from, $date_to){
		
		// query to count records in date ranges
		$query = "SELECT COUNT(*) as total_rows 
					FROM members 
					WHERE 
						created BETWEEN :date_from AND :date_to 
						OR created LIKE :date_from_for_query 
						OR created LIKE :date_to_for_query";

		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// bind date variables
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);
		
		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		// execute query and get total rows
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}*/
	
	public function searchByDateRange($date_from, $date_to, $from_record_num, $records_per_page){

		//select all data
		$query = "SELECT m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc as f_familypositiondesc 
					FROM " . $this->table_name . " m 
						LEFT JOIN familypositions f 
							ON m.familypositionID=f.familypositionID 
					ORDER BY memberID ASC
					LIMIT :from_record_num, :records_per_page";
					/*WHERE 
						p.created BETWEEN :date_from AND :date_to 
						OR p.created LIKE :date_from_for_query 
						OR p.created LIKE :date_to_for_query*/
						
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);

		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();
		
		return $stmt;
	}
	
	// used to export records to csv
	public function export_CSV(){
		
		//select all data
		//$query = "SELECT memberID, fname, lname, phone, created, modified FROM products";
		$query = "SELECT memberID, fname, lname, phone FROM members";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		//this is how to get number of rows returned
		$num = $stmt->rowCount();

		$out = "MemberID,First Name,Last Name,Phone\n";

		if($num>0){
			//retrieve our table contents
			//fetch() is faster than fetchAll()
			//http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				//extract row
				//this will make $row['name'] to
				//just $name only
				extract($row);
				$out.="{$memberID},\"{$fname}\",\"{$lname}\",{$phone}\n";
			}
		}
		
		return $out;
	}
	
//NEED TO DO THIS!!!	
	
	// view members by search term
	public function search($search_term, $from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					sa.assignmentID, sa.memberID, sa.positionID, sa.classID, c.classname AS c_classname, t.timeDesc AS t_timeDesc, m.fname AS m_fname, m.lname AS m_lname, CONCAT(m.fname, ' ', m.lname) 
				FROM 
					" . $this->table_name . " sa
					LEFT JOIN
						members m
							ON sa.memberID = m.memberID
					LEFT JOIN
						classes c
							ON sa.classID = c.classID
					LEFT JOIN
						time t
							ON c.timeID = t.timeID
				WHERE 
					m.fname LIKE ? OR m.lname LIKE ? OR CONCAT(m.fname, ' ', m.lname) LIKE ? OR CONCAT(m.lname, ' ', m.fname) LIKE ? OR c.classname LIKE ? AND
					sa.positionID = ? AND
					sa.classID = ANY (SELECT classes.classID
										FROM classes
										WHERE classes.departmentID = ?) AND
					sa.termID = ANY (SELECT users.termID
										FROM users
										WHERE users.userID = ?)
				ORDER BY 
					m.lname ASC, m.fname ASC
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		$stmt->bindParam(2, $search_term);
		$stmt->bindParam(3, $search_term);
		$stmt->bindParam(4, $search_term);
		$stmt->bindParam(5, $search_term);
		$stmt->bindParam(6, $this->positionID, PDO::PARAM_INT);
		$stmt->bindParam(7, $this->departmentID, PDO::PARAM_INT);
		$stmt->bindParam(8, $this->activeuserID, PDO::PARAM_INT);
		$stmt->bindParam(9, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(10, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	public function countAll_BySearch($search_term){

		// select query
		$query = "SELECT 
					COUNT(*) as total_rows 
				FROM 
					" . $this->table_name . " m
					LEFT JOIN 
						familypositions f
							ON m.familypositionID = f.familypositionID
				WHERE 
					m.fname LIKE ? OR m.lname LIKE ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		$stmt->bindParam(2, $search_term);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// add contact event
	public function add(){
		
		// to get time-stamp for 'created' field
		//$this->getTimestamp();
		
		//write query
		$query = "INSERT INTO 
					" . $this->table_name . " 
				SET 
					classID = ?, memberID = ?, positionID = ?, termID = ?";
		
		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(1, $this->classID);
		$stmt->bindParam(2, $this->memberID);
		$stmt->bindParam(3, $this->positionID);
		$stmt->bindParam(4, $this->termID);
		
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
	
	// view contact events with field sorting
	public function readAll_WithSorting($from_record_num, $records_per_page, $field, $order){

		$query = "SELECT m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc 
					FROM members m 
						LEFT JOIN familypositions f 
							ON m.familypositionID = f.familypositionID 
					ORDER BY {$field} {$order}
					LIMIT :from_record_num, :records_per_page";

		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();

		// return values from database
		return $stmt;
	}
	
	// view contact events
	public function readAll($classID){

		// select query
		$query = "SELECT 
					sa.assignmentID, c.classID AS c_classID, c.classname AS c_classname, c.timeID AS c_timeID, c.termID AS c_termID, m.memberID AS m_memberID, m.lname AS m_lname, m.fname AS m_fname, m.phone AS m_phone, m.phcell AS m_phcell, m.phwork AS m_phwork, m.email AS m_email, sp.positionDesc AS sp_positionDesc
				FROM 
					" . $this->table_name . " sa
					LEFT JOIN classes c 
						ON sa.classID = c.classID
					LEFT JOIN members m
						ON sa.memberID = m.memberID
					LEFT JOIN staffpositions sp
						ON sa.positionID = sp.positionID
				WHERE
					sa.classID = ?
				ORDER BY 
					sa.positionID DESC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $classID, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// view contact events
	public function readAll_Staffassign(){

		// select query
		$query = "SELECT 
					c.classname AS c_classname, c.gradelevel AS c_gradelevel, t.timeDesc AS t_timeDesc, sp.positionDesc AS sp_positionDesc
				FROM 
					" . $this->table_name . " sa
					LEFT JOIN classes c 
						ON sa.classID = c.classID
					LEFT JOIN members m
						ON sa.memberID = m.memberID
					LEFT JOIN staffpositions sp
						ON sa.positionID = sp.positionID
					LEFT JOIN time t
						ON c.timeID = t.timeID
				WHERE
					sa.memberID = ? AND
					sa.classID = ANY (SELECT classes.classID
										FROM classes
										WHERE termID = (SELECT termID
														FROM users
														WHERE userID = ?))
				ORDER BY 
					sa.assignmentID DESC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $this->memberID, PDO::PARAM_INT);
		$stmt->bindParam(2, $this->activeuserID, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
		// view staff assignments by position by department by term
	public function readAll_ByPosition(){

		// select query
		$query = "SELECT 
					sa.assignmentID, c.classname AS c_classname, t.timeDesc AS t_timeDesc, m.fname AS m_fname, m.lname AS m_lname, m.phone AS m_phone, m.phcell AS m_phcell, m.phwork AS m_phwork, m.email AS m_email 
				FROM
					staffassign sa
					LEFT JOIN classes c 
						ON sa.classID = c.classID
					LEFT JOIN members m
						ON sa.memberID = m.memberID
					LEFT JOIN time t
						ON c.timeID = t.timeID
				WHERE
					sa.positionID = ? AND
					sa.classID = ANY (SELECT classes.classID
										FROM classes
										WHERE classes.departmentID = ?) AND
					sa.termID = ANY (SELECT users.termID
										FROM users
										WHERE users.userID = ?)
				ORDER BY 
					sa.assignmentID DESC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $this->positionID, PDO::PARAM_INT);
		$stmt->bindParam(2, $this->departmentID, PDO::PARAM_INT);
		$stmt->bindParam(3, $this->activeuserID, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}

		// view staff assignments by position by department by term
	public function readAll_ByPositionwithgradelevel(){

		// select query
		$query = "SELECT 
					c.classname AS c_classname, t.timeDesc AS t_timeDesc, gl.gradelevelDesc AS gl_gradelevelDesc, (SELECT gl.gradelevelDesc AS gl_gradelevelDesc 
																													FROM classes c
                                                                                                                    LEFT JOIN gradelevels gl
																														ON c.gradelevelID = gl.gradelevelID
                                                                                                                        WHERE gl.gradelevelID = c.gradelevelID)
				FROM
					staffassign sa
					LEFT JOIN classes c 
						ON sa.classID = c.classID
					LEFT JOIN members m
						ON sa.memberID = m.memberID
					LEFT JOIN time t
						ON c.timeID = t.timeID
					LEFT JOIN gradelevels gl
						ON c.gradelevelID = gl.gradelevelID
				WHERE
					sa.positionID = 1 AND
					sa.classID = ANY (SELECT classes.classID, classes.gradelevelID
										FROM classes
										WHERE classes.departmentID = 1) AND
					sa.termID = ANY (SELECT users.termID
										FROM users
										WHERE users.userID = 1005)
				ORDER BY 
					sa.assignmentID DESC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $this->positionID, PDO::PARAM_INT);
		$stmt->bindParam(2, $this->departmentID, PDO::PARAM_INT);
		$stmt->bindParam(3, $this->activeuserID, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}

	
	// view teacher involvement 
	public function readAll_MemberInfo($memberID){

		// select query
		$query = "SELECT 
					sa.assignmentID, c.classID AS c_classID, c.classname AS c_classname, c.timeID AS c_timeID, c.termID AS c_termID, m.memberID AS m_memberID, m.lname AS m_lname, m.fname AS m_fname, sp.positionDesc AS sp_positionDesc
				FROM 
					" . $this->table_name . " sa
					LEFT JOIN classes c 
						ON sa.classID = c.classID
					LEFT JOIN members m
						ON sa.memberID = m.memberID
					LEFT JOIN staffpositions sp
						ON sa.positionID = sp.positionID
				WHERE
					sa.memberID = ?
				ORDER BY 
					sa.positionID DESC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $memberID, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// view members
	public function readAll_ByCategory($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc  
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
				WHERE 
					f.familypositionID=?
				ORDER BY 
					m.memberID ASC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $this->familypositionID);
		$stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// view members
	public function countAll_ByCategory(){

		// select query
		$query = "SELECT 
					COUNT(*) as total_rows 
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
				WHERE 
					m.memberID=?";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->familypositionID);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used for paging member list with field sorting
	public function countAll_WithSorting($field, $order){
		// for now countAll() is used
	}
	
	// used for paging contact events
	public function countAll(){
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . "";
		
		$stmt = $this->conn->prepare( $query );
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used when filling up the update staff assignment form
	public function readOne(){
		
		$query = "SELECT 
					classID, memberID, positionID 
				FROM 
					" . $this->table_name . " 
				WHERE 
					assignmentID = ? 
				LIMIT 
					0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->assignmentID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->classID = $row['classID'];
		$this->memberID = $row['memberID'];
		$this->positionID = $row['positionID'];
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					positionID, positionDesc 
				FROM 
					" . $this->table_name . " 
				ORDER BY 
					positionID";	
		
		$stmt = $this->conn->prepare( $query );		
		$stmt->execute();
		
		return $stmt;	
	}
	
	// update the staff assignment
	public function update(){

		$query = "UPDATE 
					" . $this->table_name . " 
				SET 
					classID = :classID, 
					memberID = :memberID, 
					positionID = :positionID
				WHERE
					assignmentID = :assignmentID";

		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(':classID', $this->classID);
		$stmt->bindParam(':memberID', $this->memberID);
		$stmt->bindParam(':positionID', $this->positionID);
		$stmt->bindParam(':assignmentID', $this->assignmentID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete the contact event
	public function delete(){
	
		$query = "DELETE FROM " . $this->table_name . " WHERE assignmentID = ?";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(1, $this->assignmentID);

		if($result = $stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete selected members
	public function deleteSelected($memberIDs){
		
		$in_memberIDs = str_repeat('?,', count($memberIDs) - 1) . '?';
		
		// query to delete multiple records
		$query = "DELETE FROM " . $this->table_name . " WHERE memberID IN ({$in_memberIDs})";
		 
		$stmt = $this->conn->prepare($query);
	 
		if($stmt->execute($memberIDs)){
			return true;
		}else{
			return false;
		}
	}
	
	// used for the 'created' field when creating a member
	/*public function getTimestamp(){
		date_default_timezone_set('Asia/Manila');
		$this->timestamp = date('Y-m-d H:i:s');
	}*/
}
?>