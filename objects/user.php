<?php
class UserObject{
	
	// database connection and table name
	private $conn;
	private $table_name = "users";
	
	// object properties
	public $userID;
	public $fname;
	public $lname;
	public $email;
	public $emailusername;
	public $password;
	public $termID;
	public $activeuserID;
	public $term_termDesc;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	public function check_login(){
		// for login process
		//$password = md5($_POST['password']);
		$query = "SELECT COUNT(*) AS total_rows, userID, termID 
				FROM " . $this->table_name . " 
				WHERE email = ? OR username = ? AND password = ?";
		
		//checking if the username is available in the table
		/*$result = mysqli_query($this->db,$query);
		$user_data = mysqli_fetch_array($result);
		$count_row = $result->num_rows;
	
		if ($count_row == 1) {
			// this login var will use for the session thing
			$_SESSION['login'] = true; 
			$_SESSION['activeuserID'] = $user_data['userID'];
			//$_SESSION['activesessionID'] = $user_data['sessionID'];
			return true;
		}
		else{
			return false;
		}*/
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// bind selected record id
		$stmt->bindParam(1, $this->emailusername);
		$stmt->bindParam(2, $this->emailusername);
		$stmt->bindParam(3, $this->password);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		if($total_rows == 1){
			// this login var will use for the session thing
			$_SESSION['login'] = true; 
			$_SESSION['activeuserID'] = $row['userID'];
			
			return true;
		}
		else{
			return false;	
		}
		
	}
	
	public function reg_user($fname,$lname,$username,$password,$email){
		// for registration process
		$password = md5($password);
		$query = "SELECT * 
				FROM " . $this->table_name . " 
				WHERE username = ? OR email = ?";
				
		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->username);
		$stmt->bindParam(2, $this->email);

		// execute the query
		$stmt->execute();
		
		//checking if the username or email is available in db
		$check =  $this->db->query($query) ;
		$count_row = $check->num_rows;

		//if the username is not in db then insert to the table
		if ($count_row == 0){
			$sql1="INSERT INTO users SET username='$username', password='$password', fname='$fname', lname='$lname', email='$email'";
			$result = mysqli_query($this->db,$sql1) or die(mysqli_connect_errno()."Data cannot be inserted");
			return $result;
		}
		else { return false;}
	}
	
	public function emailcheck(){
		// check if the email is already registered
		// select single record query
		$query = "SELECT 1 
				FROM " . $this->table_name . "  
				WHERE email = ? 
				LIMIT 0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->email);

		// execute the query
		$stmt->execute();

		// get record details
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// assign values to object properties
		$this->email = $row['email'];
	}
	
	public function readOne(){
		// read the details of user to be edited
		// select single record query
		$query = "SELECT userID, fname, termID 
				FROM " . $this->table_name . "  
				WHERE userID = ? 
				LIMIT 0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->userID);

		// execute the query
		$stmt->execute();

		// get record details
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// assign values to object properties
		$this->fname = $row['fname'];
		$this->termID = $row['termID'];
	}
	
	public function readActiveTerm(){
		$query = "SELECT u.userID, u.termID, term.termDesc AS term_termDesc
				FROM " . $this->table_name . " u
					LEFT JOIN term 
						ON u.termID = term.termID
				WHERE u.userID = ?
				LIMIT
					0,1";
		
		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->activeuserID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->activetermID = $row['termID'];
		$this->term_termDesc = $row['term_termDesc'];
	}
	
	public function updateTermID(){    
		// update the active term
		$query = "UPDATE " . $this->table_name . " 
				SET termID = :termID
				WHERE userID = :activeuserID";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(':termID', $this->termID);
		$stmt->bindParam(':activeuserID', $this->activeuserID);
		
		// execute the query
		if($stmt->execute()){
			$termchange = true;
			return($termchange);
		}
		else{
			return false;
		}
	}
	
	public function update(){
		// update the user
		$query = "UPDATE " . $this->table_name . "   
				SET fname = :fname,
					termID = :termID
				WHERE userID = :userID";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(':fname', $this->fname);
		$stmt->bindParam(':termID', $this->termID);
		$stmt->bindParam(':userID', $this->userID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function delete(){
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE userID = ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);
			
		// bind record id
		$stmt->bindParam(1, $this->userID);

		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function add(){
		// add the user
		// insert query
		$query = "INSERT INTO users  
				SET fname = ?, lname = ?, email = ?, password = ?, salt = ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(1, $this->fname);
		$stmt->bindParam(2, $this->lname);
		$stmt->bindParam(3, $this->email);
		$stmt->bindParam(4, $this->password);
		$stmt->bindParam(5, $this->salt);
		
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	// count all users
	public function countAll(){
		// query to count all data
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name;
	 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		return $total_rows;
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					userID, termID 
				FROM 
					" . $this->table_name . " 
				ORDER BY 
					userID ASC";	
		
		$stmt = $this->conn->prepare( $query );		
		$stmt->execute();
		
		return $stmt;	
	}
	
	// view sessions
	public function readAll(){

		// select query
		$query = "SELECT 
					userID, fname, termID
				FROM 
					" . $this->table_name . "
				ORDER BY 
					userID ASC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
}
?>