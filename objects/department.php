<?php
class Department{
	
	// database connection and table name
	private $conn;
	private $table_name = "departments";
	
	// object properties
	public $departmentID;
	public $departmentdesc;
	public $termID;
	public $activeuserID;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	public function readOne(){
		// read the details of department to be edited
		// select single record query
		$query = "SELECT departmentID, departmentdesc 
				FROM " . $this->table_name . "  
				WHERE departmentID = ? 
				LIMIT 0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->departmentID);

		// execute the query
		$stmt->execute();

		// get record details
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// assign values to object properties
		$this->departmentdesc = $row['departmentdesc'];
	}
	
	public function update(){
		// update the category
		$query = "UPDATE " . $this->table_name . "   
				SET departmentdesc = :departmentdesc
				WHERE departmentID = :departmentID";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(':departmentdesc', $this->departmentdesc);
		$stmt->bindParam(':departmentID', $this->departmentID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function delete(){
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE departmentID = ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);
			
		// bind record id
		$stmt->bindParam(1, $this->departmentID);

		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function add(){
		// add the department
		// insert query
		$query = "INSERT INTO ". $this->table_name . " 
				SET departmentdesc = ?, termID = ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(1, $this->departmentdesc);
		$stmt->bindParam(2, $this->termID);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function searchAll($search_term, $from_record_num, $records_per_page){
		// search departments based on search term
		// search query
		$query = "SELECT departmentID, departmentdesc 
				FROM " . $this->table_name . " 
				WHERE departmentdesc LIKE ? 
				ORDER BY departmentID ASC 
				LIMIT ?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind  variables
		$query_search_term = "%{$search_term}%";

		$stmt->bindParam(1, $query_search_term);
		$stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();	

		return $stmt;
	}
	
	// count all departments
	public function countAll(){
		// query to count all data
		$query = "SELECT COUNT(*) as total_rows 
				FROM departments  
				WHERE termID = (SELECT termID 
								FROM users 
								WHERE userID = ?)";
	 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// bind termID
		$stmt->bindParam(1, $activeuserID);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		return $total_rows;
	}
	
	// count all departments with search term
	public function countAll_WithSearch($search_term){
		// search query
		$query = "SELECT COUNT(*) as total_rows FROM departments WHERE departmentdesc LIKE ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// bind search term
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		return $total_rows;
	}
	
	// read all with paging
	public function readAll_WithPaging($from_record_num, $records_per_page){
		// read all departments from the database
		$query = "SELECT departmentID, departmentdesc 
				FROM " . $this->table_name . " 
				ORDER BY departmentID DESC  
				LIMIT ?, ?";	

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind values
		$stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();
		
		return $stmt;
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					d.departmentID, d.departmentdesc 
				FROM 
					" . $this->table_name . " d
				WHERE
					d.termID = ANY (SELECT u.termID
							FROM users u
							WHERE u.userID = ?)
				ORDER BY 
					d.departmentID";	
		
		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind userID
		$stmt->bindParam(1, $this->activeuserID);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;	
	}
	
	// view departments
	public function readAll(){

		// select query
		$query = "SELECT 
					departmentID, departmentdesc, termID 
				FROM 
					" . $this->table_name . "
				WHERE termID = (SELECT termID
								FROM users
								WHERE userID = ?)
				ORDER BY 
					departmentID ASC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind userID
		$stmt->bindParam(1, $this->activeuserID);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// used to read department description by its ID
	function readNameById(){
		
		$query = "SELECT departmentdesc FROM " . $this->table_name . " WHERE departmentID = ? limit 0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->departmentID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->departmentdesc = $row['departmentdesc'];
	}
}
?>