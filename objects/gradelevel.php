<?php
class GradeLevel{
	
	// database connection and table name
	private $conn;
	private $table_name = "gradelevels";
	
	// object properties
	public $gradelevelID;
	public $gradelevelDesc;
	public $activeuserID;
	public $termID;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	public function readOne(){
		// read the details of family position to be edited
		// select single record query
		$query = "SELECT gradelevelID, gradelevelDesc 
				FROM " . $this->table_name . "  
				WHERE gradelevelID = ? 
				LIMIT 0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->gradelevelID);

		// execute the query
		$stmt->execute();

		// get record details
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// assign values to object properties
		$this->gradelevelDesc = $row['gradelevelDesc'];
	}
	
	public function update(){
		// update the gradelevel
		$query = "UPDATE " . $this->table_name . "   
				SET gradelevelDesc = :gradelevelDesc
				WHERE gradelevelID = :gradelevelID";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(':gradelevelDesc', $this->gradelevelDesc);
		$stmt->bindParam(':gradelevelID', $this->gradelevelID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function delete(){
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE gradelevelID = ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);
			
		// bind record id
		$stmt->bindParam(1, $this->gradelevelID);

		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function add(){
		// add the grade level
		// insert query
		$query = "INSERT INTO " . $this->table_name . "  
				SET gradelevelDesc = ?, termID = ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(1, $this->gradelevelDesc);
		$stmt->bindParam(2, $this->termID);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					gl.gradelevelID, gl.gradelevelDesc 
				FROM 
					" . $this->table_name . " gl
				WHERE 
					gl.termID = ANY (SELECT u.termID
						FROM users u 
						WHERE u.userID = ?)	
				ORDER BY 
					gl.gradelevelID";	
		
		$stmt = $this->conn->prepare( $query );	
		
		// bind values
		$stmt->bindParam(1, $this->activeuserID);
		$stmt->execute();
		
		return $stmt;	
	}
	
	// view family positions
	public function readAll(){

		// select query
		$query = "SELECT 
					gl.gradelevelID, gl.gradelevelDesc 
				FROM 
					" . $this->table_name . " gl
				WHERE
					gl.termID = ANY (SELECT u.termID
									FROM users u
									WHERE u.userID = ?)
				ORDER BY 
					gradelevelID ASC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind values
		$stmt->bindParam(1, $this->activeuserID);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// used to read family position description by its ID
	function readNameById(){
		
		$query = "SELECT gradelevelDesc FROM " . $this->table_name . " WHERE gradelevelID = ? limit 0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->gradelevelID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->gradelevelDesc = $row['gradelevelDesc'];
	}
}
?>