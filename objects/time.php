<?php
class Time{
	
	// database connection and table name
	private $conn;
	private $table_name = "time";
	
	// object properties
	public $timeID;
	public $timeDesc;
	public $termID;
	public $activeuserID;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	public function readOne(){
		// read the details of time to be edited
		// select single record query
		$query = "SELECT timeID, timeDesc 
				FROM " . $this->table_name . "  
				WHERE timeID = ? 
				LIMIT 0,1";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );

		// bind selected record id
		$stmt->bindParam(1, $this->timeID);

		// execute the query
		$stmt->execute();

		// get record details
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		// assign values to object properties
		$this->timeDesc = $row['timeDesc'];
	}
	
	public function update(){
		// update the category
		$query = "UPDATE " . $this->table_name . "   
				SET timeDesc = :timeDesc
				WHERE timeID = :timeID";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(':timeDesc', $this->timeDesc);
		$stmt->bindParam(':timeID', $this->timeID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function delete(){
		// delete query
		$query = "DELETE FROM " . $this->table_name . " WHERE timeID = ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);
			
		// bind record id
		$stmt->bindParam(1, $this->timeID);

		// execute the query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function add(){
		// add the time
		// insert query
		$query = "INSERT INTO " . $this->table_name . 
				" SET timeDesc = ?, termID = ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind values
		$stmt->bindParam(1, $this->timeDesc);
		$stmt->bindParam(2, $this->termID);
		
		// execute query
		if($stmt->execute()){
			return true;
		}
		
		return false;
	}
	
	public function searchAll($search_term, $from_record_num, $records_per_page){
		// search times based on search term
		// search query
		$query = "SELECT timeID, timeDesc 
				FROM " . $this->table_name . " 
				WHERE timeDesc LIKE ? 
				ORDER BY timeID ASC 
				LIMIT ?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare($query);

		// bind  variables
		$query_search_term = "%{$search_term}%";

		$stmt->bindParam(1, $query_search_term);
		$stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();	

		return $stmt;
	}
	
	// count all times
	public function countAll(){
		// query to count all data
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name;
	 
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		return $total_rows;
	}
	
	// count all times with search term
	public function countAll_WithSearch($search_term){
		// search query
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . " WHERE timeDesc LIKE ?";
		
		// prepare query statement
		$stmt = $this->conn->prepare($query);
		
		// bind search term
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$total_rows = $row['total_rows'];
		
		return $total_rows;
	}
	

	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					t.timeID, t.timeDesc 
				FROM 
					" . $this->table_name . " t
				WHERE 
					t.termID = ANY (SELECT u.termID
									FROM users u 
									WHERE u.userID = ?)	
				ORDER BY 
					t.timeID";	
		
		$stmt = $this->conn->prepare( $query );		
		
		// bind values
		$stmt->bindParam(1, $this->activeuserID);
		
		$stmt->execute();
		
		return $stmt;	
	}
	
	// view times
	public function readAll(){

		// select query
		$query = "SELECT 
					time.timeID, time.timeDesc 
				FROM 
					" . $this->table_name . " 
				WHERE
					time.termID = ANY (SELECT u.termID
										FROM users u
										WHERE u.userID = ?)
				ORDER BY 
					time.timeID ASC";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$stmt->bindParam(1, $this->activeuserID, PDO::PARAM_INT);
		
		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	// used to read time description by its ID
	function readNameById(){
		
		$query = "SELECT timeDesc FROM " . $this->table_name . " WHERE timeID = ? limit 0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->timeID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->timeDesc = $row['timeDesc'];
	}
}
?>