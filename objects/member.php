<?php
class Member{
	
	// database connection and table name
	private $conn;
	private $table_name = "members";
	
	// object properties
	public $memberID;
	public $fname;
	public $lname;
	public $phone;
	public $familypositionID;
	public $email;
	public $phcell;
	public $phwork;
	public $dob;
	public $isTeacher;
	public $isSub;
	public $isHelper;
	public $ceCount;
	public $saCount;
	
	public function __construct($db){
		$this->conn = $db;
	}
	
	// count records in date ranges
	/*public function countSearchByDateRange($date_from, $date_to){
		
		// query to count records in date ranges
		$query = "SELECT COUNT(*) as total_rows 
					FROM members 
					WHERE 
						created BETWEEN :date_from AND :date_to 
						OR created LIKE :date_from_for_query 
						OR created LIKE :date_to_for_query";

		// prepare query
		$stmt = $this->conn->prepare($query);
		
		// bind date variables
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);
		
		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		// execute query and get total rows
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}*/
	
	public function searchByDateRange($date_from, $date_to, $from_record_num, $records_per_page){

		//select all data
		$query = "SELECT m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc as f_familypositiondesc 
					FROM " . $this->table_name . " m 
						LEFT JOIN familypositions f 
							ON m.familypositionID=f.familypositionID 
					ORDER BY memberID ASC
					LIMIT :from_record_num, :records_per_page";
					/*WHERE 
						p.created BETWEEN :date_from AND :date_to 
						OR p.created LIKE :date_from_for_query 
						OR p.created LIKE :date_to_for_query*/
						
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":date_from", $date_from);
		$stmt->bindParam(":date_to", $date_to);

		$date_from_for_query = "%{$date_from}%";
		$date_to_for_query = "%{$date_to}%";
		$stmt->bindParam(":date_from_for_query", $date_from_for_query);
		$stmt->bindParam(":date_to_for_query", $date_to_for_query);

		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();
		
		return $stmt;
	}
	
	// used to export records to csv
	public function export_CSV(){
		
		//select all data
		//$query = "SELECT memberID, fname, lname, phone, created, modified FROM products";
		$query = "SELECT memberID, fname, lname, phone, email, phcell, phwork, dob FROM members
		ORDER BY lname ASC, fname ASC";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();

		//this is how to get number of rows returned
		$num = $stmt->rowCount();

		$out = "MemberID,First Name,Last Name,Phone,Email,Cell Phone,Work Phone\n";

		if($num>0){
			//retrieve our table contents
			//fetch() is faster than fetchAll()
			//http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
				//extract row
				//this will make $row['name'] to
				//just $name only
				extract($row);
				$out.="{$memberID},\"{$fname}\",\"{$lname}\",\"{$phone}\",\"{$email}\",\"{$phcell}\",\"{$phwork}\"\n";
			}
		}
		
		return $out;
	}
	
//NEED TO DO THIS!!!	
	
	// view members by search term
	public function search($search_term, $from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					f.familypositiondesc AS f_familypositiondesc, m.memberID, m.fname, m.lname, m.phone, m.email, m.familypositionID, m.phcell, m.phwork, CONCAT(m.fname, ' ', m.lname)
				FROM 
					" . $this->table_name . " m
					LEFT JOIN 
						familypositions f
							ON m.familypositionID = f.familypositionID
				WHERE 
					m.fname LIKE ? OR m.lname LIKE ? OR m.phone LIKE ? OR m.email LIKE ? OR m.phcell LIKE ? OR m.phwork LIKE ? OR CONCAT(m.fname, ' ', m.lname) LIKE ?
				ORDER BY 
					m.lname ASC, m.fname ASC 
				LIMIT 
					?, ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		$stmt->bindParam(2, $search_term);
		$stmt->bindParam(3, $search_term);
		$stmt->bindParam(4, $search_term);
		$stmt->bindParam(5, $search_term);
		$stmt->bindParam(6, $search_term);
		$stmt->bindParam(7, $search_term);
		$stmt->bindParam(8, $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(9, $records_per_page, PDO::PARAM_INT);

		// execute query
		$stmt->execute();
		
		// return values from database
		return $stmt;
	}
	
	public function countAll_BySearch($search_term){

		// select query
		$query = "SELECT 
					COUNT(*) as total_rows 
				FROM 
					" . $this->table_name . " m
					LEFT JOIN 
						familypositions f
							ON m.familypositionID = f.familypositionID
				WHERE 
					m.fname LIKE ? OR m.lname LIKE ? OR m.phone LIKE ? OR m.email LIKE ? OR m.phcell LIKE ? OR m.phwork LIKE ?";

		// prepare query statement
		$stmt = $this->conn->prepare( $query );
		
		// bind variable values
		$search_term = "%{$search_term}%";
		$stmt->bindParam(1, $search_term);
		$stmt->bindParam(2, $search_term);
		$stmt->bindParam(3, $search_term);
		$stmt->bindParam(4, $search_term);
		$stmt->bindParam(5, $search_term);
		$stmt->bindParam(6, $search_term);
		
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// add member
	public function add(){
		
		// to get time-stamp for 'created' field
		//$this->getTimestamp();
		
		//write query
		$query = "INSERT INTO 
					" . $this->table_name . " 
				SET 
					fname = ?, lname = ?, phone = ?, familypositionID = ?, email = ?, phcell = ?, phwork = ?, isTeacher = ?, isSub = ?, isHelper = ?";
		
		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(1, $this->fname);
		$stmt->bindParam(2, $this->lname);
		$stmt->bindParam(3, $this->phone);
		$stmt->bindParam(4, $this->familypositionID);
		$stmt->bindParam(5, $this->email);
		$stmt->bindParam(6, $this->phcell);
		$stmt->bindParam(7, $this->phwork);
		$stmt->bindParam(8, $this->isTeacher);
		$stmt->bindParam(9, $this->isSub);
		$stmt->bindParam(10, $this->isHelper);
		
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
		
	}
	
	// view members with field sorting
	public function readAll_WithSorting($from_record_num, $records_per_page, $field, $order){

		$query = "SELECT m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc, m.phcell, m.phwork 
					FROM members m 
						LEFT JOIN familypositions f 
							ON m.familypositionID = f.familypositionID 
					ORDER BY {$field} {$order}
					LIMIT :from_record_num, :records_per_page";

		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(":from_record_num", $from_record_num, PDO::PARAM_INT);
		$stmt->bindParam(":records_per_page", $records_per_page, PDO::PARAM_INT);
		$stmt->execute();

		// return values from database
		return $stmt;
	}
	
	public function countContactEvents($memberID){
		$query = "SELECT
					COUNT(*) as ceCount from contactevents where memberID = $memberID";
					// prepare query statement
					
	$stmt = $this->conn->prepare( $query );
	
	// execute query
	$stmt->execute();
	
	// return values from database
	return $stmt;
}

	// view members
	public function readAll($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc, m.email, m.phcell, m.phwork, m.isTeacher, m.isSub, m.isHelper, COUNT(ce.contacteventID) AS ceCount, COUNT(sa.assignmentID) AS saCount
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
					LEFT JOIN contactevents ce
						ON m.memberID = ce.memberID
					LEFT JOIN staffassign sa
						ON m.memberID = sa.memberID
				GROUP BY
					m.memberID
				ORDER BY 
					m.lname ASC, m.fname ASC
				LIMIT 
					?, ?";
					
		// prepare query statement
		$stmtMembers = $this->conn->prepare( $query );
		
		// bind variable values
		$stmtMembers->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmtMembers->bindParam(2, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmtMembers->execute();
		
		// return values from database
		return $stmtMembers;
	}
	
	// used by select drop-down list
	public function read(){		
		//select all data		
		$query = "SELECT
					memberID, fname, lname, isTeacher, isSub, isHelper, dob
				FROM 
					" . $this->table_name . "
				WHERE
					isTeacher = 1 OR isSub = 1 OR isHelper = 1 
				ORDER BY 
					lname ASC, fname ASC";	
		
		$stmt = $this->conn->prepare( $query );
		$stmt->execute();
		return $stmt;
	}
	
	// view members
	public function readAll_ByTeachers($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc, m.email, m.phcell, m.phwork, m.isTeacher, m.isSub, m.isHelper, COUNT(ce.contacteventID) AS ceCount, COUNT(sa.assignmentID) AS saCount
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
					LEFT JOIN contactevents ce
						ON m.memberID = ce.memberID
						LEFT JOIN staffassign sa
						ON m.memberID = sa.memberID
				WHERE 
					m.isTeacher = 1 
				GROUP BY
					m.memberID 
				ORDER BY 
					m.lname ASC, m.fname ASC
				LIMIT 
					?, ?";

		// prepare query statement
		$stmtTeachers = $this->conn->prepare( $query );
		
		// bind variable values
		//$stmt->bindParam(1, $this->familypositionID);
		$stmtTeachers->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmtTeachers->bindParam(2, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmtTeachers->execute();
		
		// return values from database
		return $stmtTeachers;
	}
	
	// view subs
	public function readAll_BySubs($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc, m.email, m.phcell, m.phwork, m.isTeacher, m.isSub, m.isHelper, COUNT(ce.contacteventID) AS ceCount, COUNT(sa.assignmentID) AS saCount
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
					LEFT JOIN contactevents ce
						ON m.memberID = ce.memberID
						LEFT JOIN staffassign sa
						ON m.memberID = sa.memberID
				WHERE 
					m.isSub = 1 
				GROUP BY
					m.memberID
				ORDER BY 
					m.lname ASC, m.fname ASC
				LIMIT 
					?, ?";

		// prepare query statement
		$stmtSubs = $this->conn->prepare( $query );
		
		// bind variable values
		//$stmt->bindParam(1, $this->familypositionID);
		$stmtSubs->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmtSubs->bindParam(2, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmtSubs->execute();
		
		// return values from database
		return $stmtSubs;
	}
	
	// view subs
	public function readAll_ByHelpers($from_record_num, $records_per_page){

		// select query
		$query = "SELECT 
					m.memberID, m.fname, m.lname, m.phone, f.familypositiondesc AS f_familypositiondesc, m.email, m.phcell, m.phwork, m.isTeacher, m.isSub, m.isHelper, COUNT(ce.contacteventID) AS ceCount, COUNT(sa.assignmentID) AS saCount
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
					LEFT JOIN contactevents ce
						ON m.memberID = ce.memberID
						LEFT JOIN staffassign sa
						ON m.memberID = sa.memberID
				WHERE 
					m.isHelper = 1 
				GROUP BY
					m.memberID
				ORDER BY 
					m.lname ASC, m.fname ASC
				LIMIT 
					?, ?";

		// prepare query statement
		$stmtHelpers = $this->conn->prepare( $query );
		
		// bind variable values
		//$stmt->bindParam(1, $this->familypositionID);
		$stmtHelpers->bindParam(1, $from_record_num, PDO::PARAM_INT);
		$stmtHelpers->bindParam(2, $records_per_page, PDO::PARAM_INT);
		
		// execute query
		$stmtHelpers->execute();
		
		// return values from database
		return $stmtHelpers;
	}
	
	// used for paging member list with field sorting
	public function countAll_WithSorting($field, $order){
		// for now countAll() is used
	}
	
	// used for paging members
	public function countAll(){
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . " m LEFT JOIN familypositions f ON m.familypositionID = f.familypositionID";
		
		$stmtMembers = $this->conn->prepare( $query );
		$stmtMembers->execute();
		$row = $stmtMembers->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	public function countStaffAssignments($memberID){
		$query = "SELECT COUNT(*) as total_rows
					from staffassign where memberID = $memberID";
					// prepare query statement
					
		$stmt = $this->conn->prepare( $query );
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$saCount = $row['total_rows'];
		return $saCount;
	}

	
	
	// used for paging teachers
	public function countAll_ByTeachers(){
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . " m LEFT JOIN familypositions f ON m.familypositionID = f.familypositionID WHERE m.isTeacher = 1";
		
		$stmtTeachers = $this->conn->prepare( $query );
		$stmtTeachers->execute();
		$row = $stmtTeachers->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used for paging subs
	public function countAll_BySubs(){
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . " m LEFT JOIN familypositions f ON m.familypositionID = f.familypositionID WHERE m.isSub = 1";
		
		$stmtSubs = $this->conn->prepare( $query );
		$stmtSubs->execute();
		$row = $stmtSubs->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used for paging helpers
	public function countAll_ByHelpers(){
		$query = "SELECT COUNT(*) as total_rows FROM " . $this->table_name . " m LEFT JOIN familypositions f ON m.familypositionID = f.familypositionID WHERE m.isHelper = 1";
		
		$stmtHelpers = $this->conn->prepare( $query );
		$stmtHelpers->execute();
		$row = $stmtHelpers->fetch(PDO::FETCH_ASSOC);
		
		return $row['total_rows'];
	}
	
	// used when filling up the update member form
	public function readOne(){
		
		$query = "SELECT 
					m.fname, m.lname, m.phone, m.familypositionID, m.dob, f.familypositiondesc AS f_familypositiondesc, m.email, m.phcell, m.phwork, m.isTeacher, m.isSub, m.isHelper
				FROM 
					" . $this->table_name . " m
					LEFT JOIN familypositions f 
						ON m.familypositionID = f.familypositionID
				WHERE 
					memberID = ? 
				LIMIT 
					0,1";

		$stmt = $this->conn->prepare( $query );
		$stmt->bindParam(1, $this->memberID);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
		$this->fname = $row['fname'];
		$this->lname = $row['lname'];
		$this->phone = $row['phone'];
		$this->familypositionID = $row['familypositionID'];
		$this->f_familypositiondesc = $row['f_familypositiondesc'];
		$this->email = $row['email'];
		$this->phcell = $row['phcell'];
		$this->phwork = $row['phwork'];
		$this->isTeacher = $row['isTeacher'];
		$this->isSub = $row['isSub'];
		$this->isHelper = $row['isHelper'];
		$this->dob = $row['dob'];
	}
	
	// update the member
	public function update(){

		$query = "UPDATE 
					" . $this->table_name . " 
				SET 
					fname = :fname, 
					lname = :lname, 
					phone = :phone, 
					familypositionID = :familypositionID,
					email = :email,
					phcell = :phcell,
					phwork = :phwork,
					isTeacher = :isTeacher,
					isSub = :isSub,
					isHelper = :isHelper
				WHERE
					memberID = :memberID";

		$stmt = $this->conn->prepare($query);

		$stmt->bindParam(':fname', $this->fname);
		$stmt->bindParam(':lname', $this->lname);
		$stmt->bindParam(':phone', $this->phone);
		$stmt->bindParam(':familypositionID', $this->familypositionID);
		$stmt->bindParam(':email', $this->email);
		$stmt->bindParam(':phcell', $this->phcell);
		$stmt->bindParam(':phwork', $this->phwork);
		$stmt->bindParam(':isTeacher', $this->isTeacher);
		$stmt->bindParam(':isSub', $this->isSub);
		$stmt->bindParam(':isHelper', $this->isHelper);
		$stmt->bindParam(':memberID', $this->memberID);
		
		// execute the query
		if($stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	
	// delete the member
	public function delete(){
	
		$query = "DELETE FROM " . $this->table_name . " WHERE memberID = ?";
		
		$stmt = $this->conn->prepare($query);
		$stmt->bindParam(1, $this->memberID);

		if($result = $stmt->execute()){
			return true;
		}else{
			return false;
		}
	}
	
	// delete selected members
	public function deleteSelected($memberIDs){
		
		$in_memberIDs = str_repeat('?,', count($memberIDs) - 1) . '?';
		
		// query to delete multiple records
		$query = "DELETE FROM " . $this->table_name . " WHERE memberID IN ({$in_memberIDs})";
		 
		$stmt = $this->conn->prepare($query);
	 
		if($stmt->execute($memberIDs)){
			return true;
		}else{
			return false;
		}
	}
	
	// used for the 'created' field when creating a member
	/*public function getTimestamp(){
		date_default_timezone_set('Asia/Manila');
		$this->timestamp = date('Y-m-d H:i:s');
	}*/
}
?>