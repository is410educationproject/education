<?php
class Documentation{
	
	//database connection and table name
	//private $conn;
	//private $table_name = "users";
	
	// object properties
	public $page_url;
	public $text;
	
	// output tips to the help modals based on the URL
	public function readDocumentation($page_url){
		switch ($page_url) {
			case "view_members.php?":
				$text = "<p>
						 View Members allows you to view and edit information pertaining to the members on the list.<br><br>
						 Use the search box to search for information from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd a member</button>
						 &nbsp; to add a member to the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-download-alt'></span>&nbspExport CSV</button>
						 &nbsp; to download a copy of the current list in an *Excel file.<br><br>
						 In the list, a member who is highlighted in green means that the specific member has a contact event associated with them.<br><br>
						 **Tap or click on the member's phone number or email address in order to contact them via phone call, SMS, or ***email.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span></button>&nbsp; to edit a 
						 member's information.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open'></span></button>&nbsp; to view a 
						 member's information.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 member from the list.<br><br>
						 Use the page numbers below to navigate between the list's pages. Alternatively, type the desired page number in the box.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.<br><br><br><br>
						 <em>*requires compitable software (i.e. Microsoft Excel, OS X Numbers, Google Sheets)</em><br><br>
						 <em>**requires compatible software to initiate contact (i.e. Skype for PC or FaceTime for Mac for calling functions and an email client for email 
						 functions)</em><br><br>
						 <em>**For email clients, by default a PC will use a default email client and a Mac will use Mail. If you wish to use Gmail as your default email client
						 on your browser, you can refer to <a href='http://blog.hubspot.com/marketing/set-gmail-as-browser-default-email-client-ht' target='_blank'>this article
						 </a> forassistance.</em>
						 </p>";
				break;
			case "update_member.php?":
				$text = "<p>
						 Edit Member allows you to update the informaton of the member selected from the list.<br><br>
						 Use the toggles to designate a member's assignment. <input data-on='On' data-off='Off' type='checkbox' data-toggle='toggle' /><br><br>
						 When you are finished editing the member's information, tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon 
						 glyphicon-edit'></span>&nbsp;Update</button>&nbsp; to complete the update(s)<br><br>
						 To cancel this update or return to the View Members list, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-list'></span>&nbsp;View Members</button>&nbsp;or &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "view_member_info.php?":
				$text = "<p>
						 View Member Profile allows you to view the associated information of the selected member. 
						 The top portion of this page shows the members Full Name, Primary Phone number, their 
						 postition within the associated family, date of birth, any preferences they may have and any staff assignments they have. The lower portion shows a 
						 list of contact events 
						 associated with the selected member, the dates in which they were contacted, the method they were
						 contacted by, and the status reported by the member.<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd Contact Event
						 </button>&nbsp; to add a contact event to the profile.<br><br>
						  *Tap or click on the member's phone number or email address in order to contact them via phone call, SMS, or **email.<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span></button>&nbsp; to edit a 
						 member's contact event.<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open'></span></button>&nbsp; to view a 
						 member's contact event information.<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 member's contact event from the profile.<br><br>
						 To return to the View Members list, tap or click  &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon 
						 glyphicon-chevron-left'></span>&nbsp;View all members</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.<br><br><br><br>
						 <em>*compatible software is required to initiate contact (i.e. Skype for PC or FaceTime for Mac for calling functions and an email client for email 
						 functions)</em><br><br>
						 <em>**For email clients, by default a PC will use Outlook and a Mac will use Mail. If you wish to use Gmail as your default email client on your 
						 browser, you can refer to <a href='http://blog.hubspot.com/marketing/set-gmail-as-browser-default-email-client-ht' target='_blank'>this article</a> for
						 assistance.</em>
						 </p>";
				break;
			case "add_member.php?":
				$text = "<p>
						 Add member allows you to add a member to the list. Simply input the member's first name, last name, associated phone numbers, email, and their position
						 within the associated family.<br><br>
						 Tap or click the drop down list to select the family position for the member.<br><br>
						 Use the toggles to designate a member's assignment. <input data-on='On' data-off='Off' type='checkbox' data-toggle='toggle' /><br><br>
						 Once you have input the information, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'>
						 </span>&nbsp;Add member</button>&nbsp;<br><br>
						 To cancel this addition or return to the View Members list, tap or click  &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-list'></span>&nbsp;View members</button>&nbsp; or&nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "search_members.php?":
				$text = "<p>
						 With the search box you can search for a specific member or members by their first or last name, phone number, and email address.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span></button>&nbsp; to edit a 
						 member's information.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open'></span></button>&nbsp; to view a 
						 member's information.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 member from the list.<br><br>
						 To cancel this search or return to the View Members list, tap or click  &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-chevron-left'></span>&nbsp;View all members</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "update_contactevent.php?":
				$text = "<p>
						 Update Contact Event allows you to edit or information about a specfic contact event.<br><br>
						 When you are finished editing the contact event information, tap or click on the &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-edit'></span>Update Contact Event</button>&nbsp;<br><br>
						 To cancel this update or return to the Member's Profile, tap or click the &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-list'></span>Back to Member Profile</button>&nbsp;or &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "add_contactevent.php?":
				$text = "<p>
						 Add Contact Event allows you to add a specific contact event to a member's profile. Simply input 
						 the date the member was contacted, what method was used to contact that member, any notes you may 
						 have, and what the status of that member reported.<br><br>
						 Tapping or clicking on the data input field in the data selection box will bring up a calendar to select a date for the contact event. Otherwise, you 
						 may use the selection arrows or input the date manually.<br><br>
						 Tap or click the drop down list to select a contact method and/or a status reported.<br><br>
						 Once you have input the information, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'>
						 </span>&nbsp;Add Contact Event</button>&nbsp;<br><br>
						 To cancel this addition or to return to the Member's profile, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-list'></span>&nbsp;Back to Member Profile</button>&nbsp;or &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "view_classes.php?":
				$text = "<p>
						 View Classes allows you to view and edit information pertaining to the classes on the list.<br><br>
						 Use the search box to search for information from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd class</button>&nbsp; 
						 to add a class to the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-download-alt'></span>&nbspExport CSV</button>
						 &nbsp; to download a copy of the current list in an Excel file.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span></button>&nbsp; to edit a 
						 class's information.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open'></span></button>&nbsp; to view a 
						 class's information.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-remove'></span></button>&nbsp; to remove a 
						 class from the list.<br><br>
						 Use the page numbers below to navigate between the list's pages. Alternatively, type the desired page number in the box.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "update_class.php?":
				$text = "<p>
						 Update Class allows you to update the informaton of the class selected from the list.<br><br>
						 When you are finished updating the class's information, tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon 
						 glyphicon-edit'></span>&nbsp;Update</button>&nbsp; to complete the update(s).<br><br>
						 To cancel this update or return to the View Classes list, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-list'></span>&nbsp;View Classes</button>&nbsp;or &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "view_class_info.php?":
				$text = "<p>
						 View Class Profile allows you to view the associated information of the selected class. 
						 The top portion of this page shows the class's Name, Grade level, room number, class time, and term. The lower portion shows a list of staff members
						 assigned to that class, including their name and staff position..<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAssign a Staff Member
						 </button>&nbsp; to assign a staff member to the class.<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span></button>&nbsp; to update a 
						 staff member's assignment.<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open'></span></button>&nbsp; to view a 
						 staff member's assignment.<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 staff member from the class.<br><br>
						 To return to the View Classes list, tap or click  &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon 
						 glyphicon-chevron-left'></span>&nbsp;View all classes</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "add_class.php?":
				$text = "<p>
						 Add Class allows you to add a class to the list. Simply input the class name, grade level, room number, class time, and term.<br><br>
						 Tap or click the drop down list to select the class time and term for the class.<br><br>
						 Once you have input the information, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'>
						 </span>&nbsp;Add Class</button>&nbsp;<br><br>
						 To cancel this addition or return to the View Classes list, tap or click  &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-list'></span>&nbsp;View classes</button>&nbsp;or &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "search_classes.php?":
				$text = "<p>
						 With the search box you can search for a specific class by its name, grade level, room number, class time, or term.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span></button>&nbsp; to edit a 
						 class's information.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open'></span></button>&nbsp; to view a 
						 class's information.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 class from the list.<br><br>
						 To cancel this search or return to the View Classs list, tap or click  &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-chevron-left'></span>&nbsp;View all classes</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "add_staffassignment.php?":
				$text = "<p>
						 Assign a Staff Member allows you to assign a staff member to a class. Simply tap or click the drop down list to select a member to assign and select a 
						 postion to assign them to.<br><br>
						 Once you have input the information, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'>
						 </span>&nbsp;Assign Staff Member</button>&nbsp;<br><br>
						 To cancel this addition or return to the Class Profile, tap or click  &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-list'></span>&nbsp;Back to Class Profile</button>&nbsp;or &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "update_staffassignment.php?":
				$text = "<p>
						 Update Staff Assignment allows you to update the assignment of a staff member selected from the list.<br><br>
						 When you are finished updating the staff member's assignment, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-edit'></span>&nbsp;Update staff assignment</button>&nbsp; to complete the reassignment.<br><br>
						 To cancel this update or return to the Class Profile, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon 
						 glyphicon-list'></span>&nbsp;Back to Class Proflie</button>&nbsp;or &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.
						 </p>";
				break;
			case "view_staffassignments.php?":
				$text = "<p>
						 View Staff Assignments allows you to view and edit a staff member's assignment selected from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-download-alt'></span>&nbspExport CSV</button>
						 &nbsp; to download a copy of the current list in an *Excel file.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span></button>&nbsp; to edit a 
						 staff member's assignment.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open'></span></button>&nbsp; to view a 
						 staff member's assignment.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 staff assignment from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-chevron-left'></span>View Department Profile
						 </button>&nbsp; to return to the Parameter's Dashboard.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameter's Dashboard.<br><br><br><br>
						 <em>*compatible software is required to initiate contact (i.e. Skype for PC or FaceTime for Mac for calling functions and an email client for email 
						 functions)</em>
						 </p>";
				break;
			case "view_terms.php?":
				$text = "<p>
						 View terms allows you to view and edit information pertaining to the terms on the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd term</button>&nbsp;
						 to add a term to the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-edit'></span></button>&nbsp; to edit a 
						 term's description.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 term from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameter's Dashboard.
						 </p>";
				break;
			case "update_term.php?":
				$text = "<p>
						 Update term allows you to update the description of a term selected from the list.<br><br>
						 When you are finished updating the term's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-edit'></span>&nbsp;Update term</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View terms list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
			case "add_term.php?":
				$text = "<p>
						 Add term allows you to add a term to the lsit. Simply input a description for the term.<br><br>
						 Once you have input the description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'>
						 </span>&nbsp;Add term</button>&nbsp;<br><br>
						 To cancel this addition or return to the View terms list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Paramters Dashboard.
						 </p>";
				break;
			case "view_times.php?":
				$text = "<p>
						 View Times allows you to view and edit class times on the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd time</button>&nbsp; to
						 add a time to the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-edit'></span></button>&nbsp; to edit a time's 
						 description.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 time from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameter's Dashboard.
						 </p>";
				break;
			case "update_time.php?":
				$text = "<p>
						 Update Time allows you to update the description of a class time selected from the list.<br><br>
						 When you are finished updating the time's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon
						 glyphicon-edit'></span>&nbsp;Update class time</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View times list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
			case "add_time.php?":
				$text = "<p>
					 	 Add Time allows you to add a class time to the list. Simply input a description for the class time.<br><br>
						 Once you have input the description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'>
						 </span>&nbsp;Add time</button>&nbsp;<br><br>
						 To cancel this addition or return to the View times list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Paramters Dashboard.
						 </p>";
				break;
			case "view_contactmethods.php?":
				$text = "<p>
						 View Contact Methods allows you to view and edit contact methods on the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd contact method
						 </button>&nbsp; to add a contact method to the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-edit'></span></button>&nbsp; to edit a contact 
						 method's description.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 contact method from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameter's Dashboard.
						 </p>";
				break;
			case "update_contactmethod.php?":
				$text = "<p>
						 Update Contact Method allows you to update the description of a contact method selected from the list.<br><br>
						 When you are finished updating the contact method's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-edit'></span>&nbsp;Update contact method</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View Contact Methods list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
			case "add_contactmethod.php?":
				$text = "<p>
						 Add Contact Method allows you to add a contact method to the list. Simply input a description for the contact method.<br><br>
						 Once you have input the description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'>
						 </span>&nbsp;Add contact method</button>&nbsp;<br><br>
						 To cancel this addition or return to the View Contact Methods list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Paramters Dashboard.
						 </p>";
				break;
			case "view_statuses.php?":
				$text = "<p>
						 View Statuses allows you to view and edit the statuses on the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd status</button>&nbsp; 
						 to add a status to the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-edit'></span></button>&nbsp; to edit a status's
						 description.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 status from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameter's Dashboard.
						 </p>";
				break;
			case "update_status.php?":
				$text = "<p>
						 Update Status allows you to update the description of a status selected from the list.<br><br>
						 When you are finished updating the status's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-edit'></span>&nbsp;Update status</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View Statuses list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
			case "add_status.php?":
				$text = "<p>
						 Add Status allows you to add a status to the list. Simply input a description for the status.<br><br>
						 When you are finished updating the status's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-edit'></span>&nbsp;Update status</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View Statuses list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
			case "view_familypositions.php?":
				$text = "<p>
						 View Family Positions allows you to view and edit a family position selected from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd family position
						 </button>&nbsp; to add a family position to the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-edit'></span></button>&nbsp; to edit a family 
						 positions's description.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 family position from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameter's Dashboard.
						 </p>";
				break;
			case "update_familyposition.php?":
				$text = "<p>
						 Update Family Position allows you to update the description of a family position selected from the list.<br><br>
						 When you are finished updating the family position's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-edit'></span>&nbsp;Update family position</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View Family Positions list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
			case "add_familyposition.php?":
				$text = "<p>
						 Add Family Position allows you to add a family position to the list. Simply input a description for the family position.<br><br>
						 When you are finished updating the family position's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-plus'></span>&nbsp;Add family position</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View Family Positions list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
			case "view_departments.php?":
				$text = "<p>
						 View Departments allows you to view and edit a department selected from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd department
						 </button>&nbsp; to add a department to the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-edit'></span></button>&nbsp; to edit a 
						 department's description.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open'></span></button>&nbsp; to view a 
						 department's information.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 department from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameter's Dashboard.
						 </p>";
				break;
			case "update_department.php?":
				$text = "<p>
						 Update Department allows you to update the description of a department selected from the list.<br><br>
						 When you are finished updating the department's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-edit'></span>&nbsp;Update department</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View Family Positions list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
			case "view_department_info.php?":
				$text = "<p>
						 View Department Profile allows you to view the associated information of the selected department. 
						 The left portion of this page shows the departments staff position and the number of associate staff. their 
						 The right portion shows classes within the selected department, the grade level of that class, and the respective time of that class.<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd class
						 </button>&nbsp; to add a class to the department profile.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-download-alt'></span>&nbspExport CSV</button>
						 &nbsp; to download a copy of the current list in an *Excel file.<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-pencil'></span></button>&nbsp; to edit a 
						 class in the department.<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-success btn-xs'><span class='glyphicon glyphicon-eye-open'></span></button>&nbsp; on the 
						 left section to view a department's staff assignments and members. On the right you can view a specific class's profile.<br><br>
						 Tap or click  &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 departments's class from the profile.<br><br>
						 To return to the View Departments list, tap or click  &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon 
						 glyphicon-chevron-left'></span>&nbsp;View all departments</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Dashboard.<br><br><br><br>
						 <em>*requires comptable software (i.e. Microsoft Excel, OS X Numbers, Google Sheets)</em>
						 </p>";
				break;
			case "view_gradelevels.php?":
				$text = "<p>
						 View Grade Levels allows you to view and edit a grade level selected from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd grade level
						 </button>&nbsp; to add a grade level to the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-edit'></span></button>&nbsp; to edit a 
						 grade level's description.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 grade level from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameter's Dashboard.
						 </p>";
				break;
			case "add_gradelevel.php?":
				$text = "<p>
						 Add Grade Level allows you to add a grade level to the list. Simply input a description for the grade level.<br><br>
						 When you are finished updating the grade level's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-plus'></span>&nbsp;Add grade level</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View Grade Level list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
			case "update_gradelevel.php?":
				$text = "<p>
						 Update Grade Level allows you to update the description of a grade level selected from the list.<br><br>
						 When you are finished updating the grade levels's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-edit'></span>&nbsp;Update grade level</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View Grade Level list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
			case "view_staffpositions.php?":
				$text = "<p>
						 View Staff Positions allows you to view and edit a staff position selected from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-plus'></span>&nbspAdd staff position
						 </button>&nbsp; to add a staff position to the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-info btn-xs'><span class='glyphicon glyphicon-edit'></span></button>&nbsp; to edit a 
						 staff positions's description.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span class='glyphicon glyphicon-trash'></span></button>&nbsp; to remove a 
						 staff position from the list.<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameter's Dashboard.
						 </p>";
				break;
			case "add_staffposition.php?":
				$text = "<p>
						 Add Staff Positions allows you to add a staff position to the list. Simply input a description for the staff position.<br><br>
						 When you are finished updating thestaff position's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-plus'></span>&nbsp;Add staff position</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View Staff Positions list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
			case "update_staffposition.php?":
				$text = "<p>
						 Update Staff Positions allows you to update the description of a staff position selected from the list.<br><br>
						 When you are finished updating the staff position's description, tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span 
						 class='glyphicon glyphicon-edit'></span>&nbsp;Update staff position</button>&nbsp; to complete the udpate.<br><br>
						 To cancel this update or return to the View Staff Positions list, tap or click &nbsp;<button type = 'button' class='btn btn-danger btn-xs'><span 
						 class='glyphicon glyphicon-remove'></span>&nbsp;Cancel</button>&nbsp;<br><br>
						 Tap or click &nbsp;<button type = 'button' class='btn btn-primary btn-xs'><span class='glyphicon glyphicon-home'></span></button>&nbsp; to return to 
						 the Parameters Dashboard.
						 </p>";
				break;
				default:
				$text = "No documentation available.";
		}
		return $text;
	}
}
?>