<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = true;
$page_url = "view_member_info.php?";

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

$memberID = isset($_GET['memberID']) ? $_GET['memberID'] : 1;

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/member.php';
include_once 'objects/contactevent.php';
include_once 'objects/familyposition.php';
include_once 'objects/documentation.php';
include_once 'objects/staffassign.php';
include_once 'objects/staffposition.php';
include_once 'objects/preference.php';
include_once 'objects/user.php';
include_once 'objects/term.php';

// instantiate database and member object
$database = new Database();
$db = $database->getConnection();

$member = new Member($db);
$contact = new ContactEvent($db);
$familyposition = new FamilyPosition($db);
$staffassign = new StaffAssignment($db);
$staffposition = new StaffPosition($db);
$preference = new Preference($db);
$userobject = new UserObject($db);
$term = new Term($db);

$staffassign->activeuserID = $activeuserID;
$staffassign->memberID = $memberID;

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';
$activetermDesc = $userobject->readActiveTerm();

// get documentation
$documentation = new Documentation($page_url);

// header settings
$page_title = "View Member Profile";
include_once "includes/header.php";

// add the term selector to the page
include_once 'includes/termselect.inc.php';

$page_url="view_member_info.php?";
include_once "helpers/pagination_config.php";

// query contact events
$stmtCE = $contact->readAll($memberID);
$numCE = $stmtCE->rowCount();

// query staff assignments
$stmtSA = $staffassign->readAll_Staffassign();
$numSA = $stmtSA->rowCount();

// query member preference
$stmtPR = $preference->readAll_Preferences($memberID);
$numPR = $stmtPR->rowCount();


include_once "includes/members_view_info.inc.php";

include_once "includes/footer.php";
?>