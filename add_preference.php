<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$page_url = "add_contactevent.php?";

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

$memberID = isset($_GET['memberID']) ? $_GET['memberID'] : die('ERROR: missing ID.');

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/preference.php';
include_once 'objects/documentation.php';
include_once 'objects/department.php';
include_once 'objects/staffposition.php';
include_once 'objects/user.php';
include_once 'objects/term.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate objects
$preference = new Preference($db);
$department = new Department($db);
$position = new StaffPosition($db);
$userobject = new UserObject($db);
$term = new Term($db);

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';

// get documentation
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Add Preference";
include_once "includes/header.php";

include_once "includes/preferences_add.inc.php";

include_once "includes/footer.php";
?>