<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}
$pagination = false;

$page_url="update_user.php?";

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/user.php';
include_once 'objects/session.php';
include_once 'objects/documentation.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare session object
$userobject = new UserObject($db);
$session = new Session($db);
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Update User";
include_once "includes/header.php";

include_once "includes/users_update.inc.php";

include_once "includes/footer.php";
?>