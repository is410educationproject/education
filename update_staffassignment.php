<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$page_url = 'update_staffassignment.php?';

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/staffassign.php';
include_once 'objects/class.php';
include_once 'objects/member.php';
include_once 'objects/staffposition.php';
include_once 'objects/time.php';
include_once 'objects/term.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate objects
$assignment = new StaffAssignment($db);
$class = new Classes($db);
$member = new Member($db);
$time = new Time($db);
$position = new StaffPosition($db);
$userobject = new UserObject($db);
$term = new Term($db);

$stmt = $assignment->readOne();

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';
//$activetermDesc = $userobject->readActiveTerm();

//get documentation
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Update Staff Assignment";
include_once "includes/header.php";

include_once "includes/staffassignment_update.inc.php";

include_once "includes/footer.php";
?>