//JavaScript for remove function
function remove_teachers()
	{	
		var selectedIDs = $("input:checked").
		map(function() { return this.id; }).get();
		console.log(selectedIDs);
		if(selectedIDs.length == 0) {
			alert("Please choose a staff member to remove.");
			return false;
		}
		if (confirm("Are you sure you want to remove this staff member?") == true)
		{
			$.ajax({
				type: 'POST',
				url: 'view_teachers_remove_button.php',
				data: {selected: selectedIDs},
				success: function(data) {
					alert("Teacher has been removed.");		
					/* $("#demo").html("Teacher is removed!"); */
					window.location.reload(true);
				}
			});
		}
		else
		{
			return false;
		}
	}