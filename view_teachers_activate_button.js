//JavaScript for activate function
function activate_teachers()
	{	
		var selectedIDs = $("input:checked").
		map(function() { return this.id; }).get();
		console.log(selectedIDs);
		if(selectedIDs.length == 0) {
			alert("Please choose a staff member to activate.");
			return false;
		}
		if (confirm("Are you sure you want to activate this staff member?") == true)
		{
			$.ajax({
				type: 'POST',
				url: 'view_teachers_activate_button.php',
				data: {selected: selectedIDs},
				success: function(data) {
					alert("Staff member has been activated.");		
					/* $("#demo").html("Staff member is activated!"); */
					window.location.reload(true)
				}
			});
		}
		else
		{
			return false;
		}
	}