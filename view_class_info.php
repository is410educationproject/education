<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$page_url = "view_class_info.php?";

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/class.php';
include_once 'objects/staffassign.php';
include_once 'objects/member.php';
include_once 'objects/staffposition.php';
include_once 'objects/time.php';
include_once 'objects/term.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';

// instantiate database and objects
$database = new Database();
$db = $database->getConnection();

$assignment = new StaffAssignment($db);
$class = new Classes($db);
$member = new Member($db);
$time = new Time($db);
$term = new Term($db);
$staffposition = new StaffPosition($db);
$userobject = new UserObject($db);

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';
$activetermDesc = $userobject->readActiveTerm();

// get documentation
$documentation = new Documentation($page_url);

// header settings
$page_title = "View Class Profile";
include_once "includes/header.php";

// add the term selector to the page
include_once 'includes/termselect.inc.php';

include_once "helpers/pagination_config.php";

$classID = isset($_GET['classID']) ? $_GET['classID'] : 1;
$departmentID = isset($_GET['departmentID']) ? $_GET['departmentID'] : 1;

// query classes
$stmt = $assignment->readAll($classID);
$num = $stmt->rowCount();

include_once "includes/classes_view_info.inc.php";

include_once "includes/footer.php";
?>