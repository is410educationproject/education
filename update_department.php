<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$page_url = 'update_department.php?';

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/department.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';
include_once 'objects/term.php';
	
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare objects
$department = new Department($db);
$userobject = new UserObject($db);
$term = new Term($db);

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';
$activetermDesc = $userobject->readActiveTerm();

// get documentation
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Update Department";
include_once "includes/header.php";

include_once "includes/departments_update.inc.php";

include_once "includes/footer.php";
?>