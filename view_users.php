<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}
$pagination = false;

$page_url="view_users.php?";

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/user.php';
include_once 'objects/session.php';
include_once 'objects/documentation.php';

// instantiate database and objects
$database = new Database();
$db = $database->getConnection();

$userobject = new UserObject($db);
$session = new Session($db);
$documentation = new Documentation($page_url);

// header settings
$page_title = "View Users";
include_once "includes/header.php";

// query sessions
$stmt = $userobject->readAll();
$num = $stmt->rowCount();

include_once "includes/users_main.inc.php";

include_once "includes/footer.php";
?>