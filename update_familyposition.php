<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$globalpage = true;
$page_url = 'update_familyposition.php?';

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/familyposition.php';
include_once 'objects/documentation.php';
include_once 'objects/term.php';
include_once 'objects/user.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare familyposition.php object
$familyposition = new FamilyPosition($db);
$userobject = new UserObject($db);
$term = new Term($db);

// get documentation
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Update Family Position";
include_once "includes/header_param.php";

include_once "includes/familypositions_update.inc.php";

include_once "includes/footer.php";
?>