<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/contactmethod.php';

// instantiate database and contactmethod.php object
$database = new Database();
$db = $database->getConnection();

$contactmethod = new ContactMethod($db);

// header settings
$page_title = "View contact methods";
include_once "includes/header.php";

$page_url="view_contactmethods.php?";


// query contactmethods
$stmt = $contactmethod->readAll();
$num = $stmt->rowCount();

include_once "phpoopcrud/includes/contactmethods_main.inc.php";

include_once "includes/footer.php";
?>