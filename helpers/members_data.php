<?php
// Include the connect.php file
include ('../includes/db_connect.php');

// Connect to the database
$mysqli = new mysqli($hostname, $username, $password, $database);
/* check connection */
if (mysqli_connect_errno())
	{
	printf("Connect failed: %s\n", mysqli_connect_error());
	exit();
	}
// get data and store in a json array
$pagenum = $_GET['pagenum'];
$pagesize = $_GET['pagesize'];
$start = $pagenum * $pagesize;
$query = "SELECT SQL_CALC_FOUND_ROWS memberID, fname, lname, familyposition FROM members LIMIT ?,?";
if (isset($_GET['sortdatafield']))
	{
	$sortfields = array(
		"memberID",
		"fname",
		"lname",
		"familyposition"
	);
	$sortfield = $_GET['sortdatafield'];
	$sortorder = $_GET['sortorder'];
	if (($sortfield != NULL) && (in_array($sortfield, $sortfields)))
		{
		if ($sortorder == "desc")
			{
			$query = "SELECT SQL_CALC_FOUND_ROWS memberID, fname, lname, familyposition FROM members ORDER BY" . " " . $sortfield . " DESC LIMIT ?,?";
			}
		  else if ($sortorder == "asc")
			{
			$query = "SELECT SQL_CALC_FOUND_ROWS memberID, fname, lname, familyposition FROM members ORDER BY" . " " . $sortfield . " ASC LIMIT ?,?";
			}
		}
	}
$result = $mysqli->prepare($query);
$result->bind_param('ii', $start, $pagesize);
$result->execute();
/* bind result variables */
$result->bind_result($memberID, $fname, $lname, $familyposition);
/* fetch values */
while ($result->fetch())
	{
	$customers[] = array(
		'memberID' => $memberID,
		'fname' => $fname,
		'lname' => $lname,
		'familyposition' => $familyposition
	);
	}
$result = $mysqli->prepare("SELECT FOUND_ROWS()");
$result->execute();
$result->bind_result($total_rows);
$result->fetch();
$data[] = array(
	'TotalRows' => $total_rows,
	'Rows' => $customers
);
echo json_encode($data);
/* close statement */
$result->close();
/* close connection */
$mysqli->close();
?>