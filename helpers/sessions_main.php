<table class="table table-bordered table-hover">
	<thead>
	  <tr>
	    <th>Session Description</th>
	    <th>Session Year</th>
	    <th>Action</th>
	  </tr>
	</thead>
	<tbody>
<?php
include "../includes/db_connect.php";
$conn = new mysqli($hostname, $username, $password, $database);
if ($conn->connect_errno) {
    echo "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
}
$res = $conn->query("SELECT sessionID, sessionDesc, sessionYear FROM session");
while ($row = $res->fetch_assoc()) {
?>
    
	  <tr>
	    <td><?php echo $row['sessionDesc']; ?></td>
	    <td><?php echo $row['sessionYear']; ?></td>
        <td>
	    <a class="btn btn-warning btn-sm" data-toggle="modal" data-target="#myModal<?php echo $row['sessionID']; ?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
	    <a class="btn btn-danger btn-sm"  onclick="deletedata('<?php echo $row['sessionID']; ?>')" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>

<!-- Modal -->
<div class="modal fade" id="myModal<?php echo $row['sessionID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel<?php echo $row['sessionID']; ?>" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel<?php echo $row['sessionID']; ?>">Edit Data</h4>
      </div>
      <div class="modal-body">

<form>
  <div class="form-group">
    <label for="sessionDesc">Session Description</label>
    <input type="text" class="form-control" id="sessionDesc<?php echo $row['sessionID']; ?>" value="<?php echo $row['sessionDesc']; ?>">
  </div>
  <div class="form-group">
    <label for="sessionYear">Session Year</label>
    <input type="text" class="form-control" id="sessionYear<?php echo $row['sessionID']; ?>" value="<?php echo $row['sessionYear']; ?>">
  </div>

</form>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" onclick="updatedata('<?php echo $row['sessionID']; ?>')" class="btn btn-primary" data-dismiss="modal">Save changes</button>
      </div>
    </div>
  </div>
</div>
	    
	    </td>
	  </tr>
<?php
}
?>
	</tbody>
      </table>