<table class="table table-bordered table-hover">
	<thead>
	  <tr>
	    <th>Time Description</th>
	    <th>Action</th>
	  </tr>
	</thead>
	<tbody>
<?php
include "../includes/db_connect.php";
// pass in db connection parameters to variable
$mysqli = new mysqli($hostname, $username, $password, $database);
// if error, display error msg
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $connection->connect_errno . ") " . $connection->connect_error;
}

// query for Members
$queryTime = "
	SELECT timeID, timeDesc FROM time ";
// execute Members query, store it in a variable
$resultTime = $mysqli->query($queryTime);

while ($time = $resultTime->fetch_assoc()) {
?>
    
	  <tr>
	    <td><?php echo $time['timeDesc']; ?></td>	    
        <td>
       <a class="btn btn-info btn-sm" data-toggle="modal" data-target="#viewTimeModal<?php echo $time['timeID']; ?>">
            	<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
            </a>
			<a class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editTimeModal<?php echo $time['timeID']; ?>">
				<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
			</a>
			<a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteTimeModal<?php echo $time['timeID']; ?>">
				<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
			</a>

			<!-- Edit Time Modal -->
			 <div class="modal fade" 
            	id="editTimeModal<?php echo $time['timeID']; ?>" 
                tabindex="-1" 
                role="dialog" 
                aria-labelledby="editTimeModalLabel<?php echo $time['timeID']; ?>" 
                aria-hidden="true">
            	<div class="modal-dialog">
            		<div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            
                            <h4 class="modal-title" id="editTimeModalLabel<?php echo $time['timeID']; ?>">
                                Edit a Time
                            </h4>
                        </div>
                                                
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="timeDesc">Time Description</label>
                                    <input type="text" class="form-control" id="timeDesc<?php echo $time['timeID']; ?>" value="<?php echo $time['timeDesc']; ?>">
                                </div>
                                
                   
                            </form>
                        </div>
                                                    
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close
                            </button>
                            <!-- call the updatedata() function in time.php when button is clicked, pass it the timeID string as argument -->
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updatedata('<?php echo $time['timeID']; ?>')">
                                Save changes
                            </button>
                        </div>
                    </div>
				</div>
			</div>
            

            <!-- Delete Member Modal -->
            <div class="modal fade" 
            	id="deleteTimeModal<?php echo $time['timeID']; ?>" 
                tabindex="-1" 
                role="dialog" 
                aria-labelledby="deleteTimeModalLabel<?php echo $time['timeID']; ?>" 
                aria-hidden="true">
            	<div class="modal-dialog">
            		<div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            
                            <h4 class="modal-title" id="deleteTimeModalLabel<?php echo $time['timeID']; ?>">
                                Delete a Time
                            </h4>
                        </div>
                        
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    Are you sure you want to delete time information for <em><strong><?php echo $time['timeDesc']; ?></strong></em>?
                                </div>
                            </form>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                No
                            </button>
                            <!-- call the deletedata() function in members.php when button is clicked, pass it the memberID string as argument -->
                            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deletedata('<?php echo $time['timeID']; ?>')">
                                Yes
                            </button>
                        </div>
					</div>
                </div>
            </div>
            
  <!-- View Time Modal   -->      
 <div class="modal fade" 
            	id="viewTimeModal<?php echo $time['timeID']; ?>" 
                tabindex="-1" 
                role="dialog" 
                aria-labelledby="viewTimeModalLabel<?php echo $time['timeID']; ?>" 
                aria-hidden="true">
            	<div class="modal-dialog">
            		<div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            
                            <h4 class="modal-title" id="viewTimeModalLabel<?php echo $time['timeID']; ?>">
                                View Time 
                            </h4>
                        </div>
                                                
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="timeDesc">Time Description</label>
                                    <br />
                                    <?php echo $time['timeDesc']; ?>
                                </div>
        
                            </form>
                        </div>
                                             
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close
                            </button>
                           
                        </div>
                    </div>
				</div>
			</div>
	    
	    </td>
	  </tr>
<?php
}
?>
	</tbody>
      </table>