<?php
include "../includes/db_connect.php";
// pass in db connection parameters to variable
$mysqli = new mysqli($hostname, $username, $password, $database);
// if error, display error msg
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

if(isset($_GET['m_memberID'])) { 
	// prepare the update query and store it in a variable
	$queryUpdateMember = $mysqli->prepare("UPDATE members SET fname=?, lname=?, phone=?, email=?, familypositionID=? WHERE memberID=?");
	// bind the POST and GET variables to the prepared update query
	$queryUpdateMember->bind_param('ssssis', $m_fname, $m_lname, $m_phone, $m_email, $m_familypositionID, $m_memberID);
	
	// pull the values in the URL sent from the javascript in members.php
	// store in variables
	$m_fname = $_POST['m_fname'];
	$m_lname = $_POST['m_lname'];
	$m_phone = $_POST['m_phone'];
	$m_email = $_POST['m_email'];
	$m_familypositionID = $_POST['m_familypositionID'];
	$m_memberID = $_GET['m_memberID'];

	// execute update query
	// display status message
	if($queryUpdateMember->execute()) { ?>
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>Success!</strong> Member information was updated for <em><?php echo $m_fname . " " . $m_lname ?>.</em>
		</div>
	<?php } 
	else{ ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>Error!</strong>
		</div>
	<?php }
}

else{ ?> 
    <div class="alert alert-warning alert-dismissible" role="alert">
    	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
    		<span aria-hidden="true">&times;</span>
    	</button>
    	<strong>Warning!</strong>
    </div>
<?php } ?>