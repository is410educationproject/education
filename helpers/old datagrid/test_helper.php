<?php
include "../includes/db_connect.php";
// pass in db connection parameters to variable
$connection = new mysqli($hostname, $username, $password, $database);
// if error, display error msg
if ($connection->connect_errno) {
    echo "Failed to connect to MySQL: (" . $connection->connect_errno . ") " . $connection->connect_error;
}

 //fetch table rows from mysql db
    $queryMembers = "
	SELECT m.memberID 			AS m_memberID, 
		m.fname 				AS m_fname, 
		m.lname 				AS m_lname, 
		m.phone 				AS m_phone, 
		m.email 				AS m_email
	FROM members m";
    $resultMembers = mysqli_query($connection, $queryMembers) or die("Error in Members Query " . mysqli_error($connection));
	
	//create an array
    $membersArray = array();
    while($row = mysqli_fetch_assoc($resultMembers))
    {
        $membersArray[] = $row;
    }
	
	//write to json file
    //$fp = fopen('../json/membersData.json', 'w');
   // fwrite($fp, json_encode($membersArray));
   echo json_encode($membersArray);
    //fclose($fp);
	
	

?>