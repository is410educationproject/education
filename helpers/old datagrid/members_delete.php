<?php
include "../includes/db_connect.php";
// pass in db connection parameters to variable
$mysqli = new mysqli($hostname, $username, $password, $database);
// if error, display error msg
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

if(isset($_GET['m_memberID'])) {
	// prepare the delete query and store it in a variable
	$queryDeleteMember = $mysqli->prepare("DELETE FROM members WHERE memberID=?");
	// bind the GET variables to the prepared delete query
	$queryDeleteMember->bind_param('s', $m_memberID);
	
	// pull the value in the URL sent from the javascript in members.php
	// store in variable
	$m_memberID = $_GET['m_memberID'];
	
	// execute delete query
	// display status message
	if($queryDeleteMember->execute()) { ?>
        <div class="alert alert-success alert-dismissible" role="alert">
        	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        	<strong>Success!</strong> Member # <?php echo $m_memberID ?>'s information was deleted.
        </div>
	<?php } 
	else{ ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Error!</strong>
        </div>
	<?php }
}

else{ ?> 
	<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<strong>Warning!</strong>
	</div>
<?php } ?>