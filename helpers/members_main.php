<!--<div class="example" id="memberrecords">
                    <fieldset class="controls">
                        <input type="text" class="form-control" id="ex2f" placeholder="Type here to filter">
                    </fieldset>-->



<!-- table that wraps the page -->
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <!-- display the column headers -->
            <th>Full Name</th>
            <th>Family Position</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    
<?php
include "../includes/db_connect.php";
// pass in db connection parameters to variable
$mysqli = new mysqli($hostname, $username, $password, $database);
// if error, display error msg
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $connection->connect_errno . ") " . $connection->connect_error;
}


// query for Members
$queryMembers = "
	SELECT m.memberID 			AS m_memberID, 
		m.fname 				AS m_fname, 
		m.lname 				AS m_lname, 
		m.phone 				AS m_phone, 
		m.email 				AS m_email, 
		f.familypositiondesc 	AS f_familypositionDesc, 
		m.familypositionID 		AS m_familypositionID,
		ce.date                 AS ce_date
	FROM members m
	LEFT JOIN familypositions f 
	ON m.familypositionID = f.familypositionID
	LEFT JOIN contactevents ce
	ON m.memberIDa = ce.memberID
	ORDER BY m.lname asc";
// execute Members query, store it in a variable
$resultMembers = $mysqli->query($queryMembers);

// query for FamilyPositions
$queryFamilyPositions = "
	SELECT fp.familypositionID 	AS fp_ID, 
		fp.familypositiondesc 	AS fp_Desc 
	FROM familypositions fp";
// execute FamilyPositions query, store it in a variable
$resultFamilyPositions = $mysqli->query($queryFamilyPositions);
// define a new array
$familypositionarray = array();
// for all records in the FamilyPositions table, store them in an array
// mysqli_fetch_assoc() is fetching the values from the resultFamilyPositions variable
while($positiondata = mysqli_fetch_assoc($resultFamilyPositions)) { 
    $familypositionarray[] = $positiondata; 
} 

$queryContactEvents = "
	SELECT c.contacteventID AS c_contacteventID,
		c.date AS c_date,
        c.memberID AS c_memberID,
		m.memberID AS m_memberID
	FROM contactevents c
	left JOIN members m
	ON c.memberID = m.memberID";

$resultContactEvents = $mysqli->query($queryContactEvents);

$contacteventsarray = array();

while($contacteventsdata = mysqli_fetch_assoc($resultContactEvents)) {
	$contacteventsarray[] = $contacteventsdata;
}

// loop to output each record from the Members table
// with the buttons and Modals for each record
while($member = $resultMembers->fetch_assoc()) { ?>
    
	<tr class="instafilta-target">
        <!-- data to output to grid -->
        <td><?php echo $member['m_lname'] . ", " . $member['m_fname']; ?></td>
        <td><?php echo $member['f_familypositionDesc']; ?></td>
        <td>
        	<a class="btn btn-info btn-sm" data-toggle="modal" data-target="#viewMemberModal<?php echo $member['m_memberID']; ?>">
            	<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
            </a>
            <a class="btn btn-success btn-sm" data-toggle="modal" data-target="#addContactEventModal<?php echo $member['m_memberID']; ?>">
            	<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
            </a>
			<a class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editMemberModal<?php echo $member['m_memberID']; ?>">
				<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
			</a>
			<a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteMemberModal<?php echo $member['m_memberID']; ?>">
				<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
			</a>

            <!-- Edit Member Modal -->
            <div class="modal fade" 
            	id="editMemberModal<?php echo $member['m_memberID']; ?>" 
                tabindex="-1" 
                role="dialog" 
                aria-labelledby="editMemberModalLabel<?php echo $member['m_memberID']; ?>" 
                aria-hidden="true">
            	<div class="modal-dialog">
            		<div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            
                            <h4 class="modal-title" id="editMemberModalLabel<?php echo $member['m_memberID']; ?>">
                                Edit a member
                            </h4>
                        </div>
                                                
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="m_fname">First Name</label>
                                    <input type="text" class="form-control" id="m_fname<?php echo $member['m_memberID']; ?>" value="<?php echo $member['m_fname']; ?>">
                                </div>
                                
                                <div class="form-group">
                                    <label for="m_lname">Last Name</label>
                                    <input type="text" class="form-control" id="m_lname<?php echo $member['m_memberID']; ?>" value="<?php echo $member['m_lname']; ?>">
                                </div>
                                
                                <div class="form-group">
                                    <label for="m_phone">Phone</label>
                                    <input type="text" class="form-control" id="m_phone<?php echo $member['m_memberID']; ?>" value="<?php echo $member['m_phone']; ?>">
                                </div>
                                
                                <div class="form-group">
                                    <label for="m_email">Email</label>
                                    <input type="text" class="form-control" id="m_email<?php echo $member['m_memberID']; ?>" value="<?php echo $member['m_email']; ?>">
                                </div>
                                
                                <div class="form-group">
                                    <label class="text" for="m_familypositionID">Family Position</label>
                                    <select name="familyposition_dropdown" class="form-control">
                                            
                                        <!-- foreach loop to display options for dropdown -->
                                        <?php foreach ($familypositionarray as $fp) { ?>
                                            
                                            <option 
                                                value="<?php echo $fp['fp_ID']; ?>"
                                                id="m_familypositionID<?php echo $member['m_memberID']; ?>"
                                                <?php if($member['m_familypositionID'] == $fp['fp_ID']) { echo "selected"; } ?> >
                                                    <?php echo $fp['fp_Desc']; ?>
                                            </option>
                                        <?php } ?>
                                    </select> 
                                </div>
                            </form>
                        </div>
                                                    
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close
                            </button>
                            <!-- call the updatedata() function in members.php when button is clicked, pass it the memberID string as argument -->
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updatedata('<?php echo $member['m_memberID']; ?>')">
                                Save changes
                            </button>
                        </div>
                    </div>
				</div>
			</div>
            
            <!-- Delete Member Modal -->
            <div class="modal fade" 
            	id="deleteMemberModal<?php echo $member['m_memberID']; ?>" 
                tabindex="-1" 
                role="dialog" 
                aria-labelledby="deleteMemberModalLabel<?php echo $member['m_memberID']; ?>" 
                aria-hidden="true">
            	<div class="modal-dialog">
            		<div class="modal-content">

                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            
                            <h4 class="modal-title" id="deleteMemberModalLabel<?php echo $member['m_memberID']; ?>">
                                Delete a member
                            </h4>
                        </div>
                        
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    Are you sure you want to delete member information for <em><strong><?php echo $member['m_fname'] . " " . $member['m_lname']; ?></strong></em>?
                                </div>
                            </form>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                No
                            </button>
                            <!-- call the deletedata() function in members.php when button is clicked, pass it the memberID string as argument -->
                            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deletedata('<?php echo $member['m_memberID']; ?>')">
                                Yes
                            </button>
                        </div>
					</div>
                </div>
            </div>
            
             <!-- View Member Modal   -->      
 <div class="modal fade" 
            	id="viewMemberModal<?php echo $member['m_memberID']; ?>" 
                tabindex="-1" 
                role="dialog" 
                aria-labelledby="viewMemberModalLabel<?php echo $member['m_memberID']; ?>" 
                aria-hidden="true">
            	<div class="modal-dialog">
            		<div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title" id="viewMemberModalLabel<?php echo $member['m_memberID']; ?>">
                                View Member
                            </h4>
                        </div>                  
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="m_fname">Member Name</label>
                                    <br />
                                    <?php echo $member['m_fname']; ?>
                                    <?php echo $member['m_lname']; ?>
                                </div>
                                <div class="form-group">
                                    <label for="m_fname">Family Position</label>
                  					<br />
                                    <?php echo $member['f_familypositionDesc']; ?>
                                </div>
                                <div class="form-group">
                                    <label for="m_fname">Contact Information</label>
                  					<br />
                                    <?php echo $member['m_phone']; ?>
                                    <br />
                                    <?php echo $member['m_email']; ?>
                        		</div>
                                  <div class="form-group">
                                    <label for="m_fname">Contact Date</label>
                  					<br />
                                   
                                    
                                    
									<?php 
									/*foreach ($contacteventsarray as $ce) {
										if($member['m_memberID'] == $ce['c_memberID']) {
											echo $ce['c_date'];
										}
										else {
											echo "Nothing";
										}
										unset($ce);
									}*/
										
									
									
                                    if($member['c_'] != $member['m_memberID']) {
                                    	echo "No Contact Event";
									}
									elseif($member['m_memberID'] == $contacteventsarray['m_memberID']) { 
										foreach ($contacteventsarray as $ce ) {
											$date = strtotime($ce['c_date']);
											echo $myFormatForView = date("m/d/y ", $date);
										}
									
									}
									
                                    ?>
                                   
                        		</div>
                               </form>              
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close
                            </button>
                           
                        </div>
                    </div>
				</div>
			</div>
	    
		</td>
	</tr>
<?php } ?>
</tbody>
</table>
</div>

