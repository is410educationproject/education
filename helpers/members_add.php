<?php
include "../includes/db_connect.php";
// pass in db connection parameters to variable
$mysqli = new mysqli($hostname, $username, $password, $database);
// if error, display error msg
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}
	
// pull the values in the URL sent from the javascript in members.php
// store in variables
$m_fname = $_POST['m_fname'];
$m_lname = $_POST['m_lname'];
$m_phone = $_POST['m_phone'];
$m_email = $_POST['m_email'];
$m_familypositionID = $_POST['m_familypositionID'];

if($m_fname != null && $m_lname != null && $m_phone != null && $m_email != null && $m_familypositionID != null) {
	// prepare the insert query and store it in a variable
	$queryAddMember = $mysqli->prepare("INSERT INTO members (fname, lname, phone, email, familypositionID) VALUES (?,?,?,?,?)");
	// bind the POST variables to the prepared insert query
	$queryAddMember->bind_param('sssss', $m_fname, $m_lname, $m_phone, $m_email, $m_familypositionID);
	
	// execute insert query
	// display status message
	if($queryAddMember->execute()) { ?>
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            	<span aria-hidden="true">&times;</span>
            </button>
			<strong>Success!</strong> <?php echo $m_fname . " " . $m_lname ?>'s member information was added.
		</div>
	<?php } 
	else{ ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            	<span aria-hidden="true">&times;</span>
            </button>
			<strong>Error!</strong>
		</div>
	<?php }
} 

else{ ?> 
	<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        	<span aria-hidden="true">&times;</span>
        </button>
		<strong>Warning!</strong> 
	</div>
<?php } ?>