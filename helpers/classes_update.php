<?php
include "../includes/db_connect.php";
// pass in db connection parameters to variable
$mysqli = new mysqli($hostname, $username, $password, $database);
// if error, display error msg
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

if(isset($_GET['c_classID'])) { 
	// prepare the update query and store it in a variable
	$queryUpdateClass = $mysqli->prepare("UPDATE classes SET classname=?, gradelevel=?, room=?, timeID=?, sessionID=? WHERE classID=?");
	// bind the POST and GET variables to the prepared update query
	$queryUpdateClass->bind_param('ssssss', $c_classname, $c_gradelevel, $c_room, $c_timeID, $c_sessionID, $c_classID);
	
	// pull the values in the URL sent from the javascript in classes.php
	// store in variables
	$c_classname = $_POST['c_classname'];
	$c_gradelevel = $_POST['c_gradelevel'];
	$c_room = $_POST['c_room'];
	$c_timeID = $_POST['c_timeID'];
	$c_sessionID = $_POST['c_sessionID'];
	$c_classID = $_GET['c_classID'];
	
	// execute update query
	// display status message
	if($queryUpdateClass->execute()) { ?>
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>Success!</strong> Class information was updated for <em><?php echo $c_classname ?>.</em>
		</div>
	<?php } 
	else{ ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>Error!</strong>
		</div>
	<?php }
} 

else{ ?> 
	<div class="alert alert-warning alert-dismissible" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
        	<span aria-hidden="true">&times;</span>
        </button>
		<strong>Warning!</strong>
	</div>
<?php } ?>