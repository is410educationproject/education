<!--<div class="example" id="classes">
                    <fieldset class="controls">
                        <input type="text" class="form-control" id="classesform" placeholder="Type here to filter">
                    </fieldset>-->
                    
                    

<!-- table that wraps the page -->
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <!-- display the column headers -->
            <th>Class Name</th>
            <th>Grade</th>
            <th>Room Number</th>
            <th>Time</th>
            <th>Session</th>
            <th>Action</th>
		</tr>
	</thead>
	<tbody>
    
<?php
include "../includes/db_connect.php";
// pass in db connection parameters to variable
$mysqli = new mysqli($hostname, $username, $password, $database);
// if error, display error msg
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $connection->connect_errno . ") " . $connection->connect_error;
}

$findinthiscolumn = "c.classname";
$whateverisentered = $_POST['whateverisentered'];

// query for Classes
$queryClasses = "
	SELECT c.classID 	AS c_classID, 
		c.classname 	AS c_classname, 
		c.gradelevel 	AS c_gradelevel, 
		c.room 			AS c_room, 
		t.timeDesc 		AS t_timeDesc, 
		c.timeID 		AS c_timeID, 
		s.sessionDesc 	AS s_sessionDesc,  
		c.sessionID 	AS c_sessionID
	FROM classes c
	LEFT JOIN time t
	ON c.timeID = t.timeID
	LEFT JOIN session s
	ON c.sessionID = s.sessionID
	WHERE $findinthiscolumn LIKE '%$whateverisentered%'";
// execute Classes query, store it in a variable
$resultClasses = $mysqli->query($queryClasses);

// query for Times
$queryTimes = "
	SELECT time.timeID 	AS time_ID, 
		time.timeDesc 	AS time_Desc
	FROM time";
// execute Times query, store it in a variable
$resultTimes = $mysqli->query($queryTimes);
// define a new array
$timearray = array();
// for all records in the Times table, store them in an array
// mysqli_fetch_assoc() is fetching the values from the resultTimes variable
while($timedata = mysqli_fetch_assoc($resultTimes)) { 
    $timearray[] = $timedata; 
}

// query for Sessions
$querySessions = "
	SELECT session.sessionID AS session_ID, 
		session.sessionDesc AS session_Desc
	FROM session";
// execute Sessions query, store it in a variable
$resultSessions = $mysqli->query($querySessions);
// define a new array
$sessionarray = array();
// for all records in the Sessions table, store them in an array
// mysqli_fetch_assoc() is fetching the values from the resultSessions variable
while($sessiondata = mysqli_fetch_assoc($resultSessions)) { 
    $sessionarray[] = $sessiondata; 
}

// loop to output each record from the Classes table
// with the buttons and modals for each record
while ($class = $resultClasses->fetch_assoc()) { ?>
    
    <tr class="instafilta-target">
    	<!-- data to output to grid -->
        <td><?php echo $class['c_classname']; ?></td>
        <td><?php echo $class['c_gradelevel']; ?></td>
        <td><?php echo $class['c_room']; ?></td>
        <td><?php echo $class['t_timeDesc']; ?></td>
        <td><?php echo $class['s_sessionDesc']; ?></td>
        <td>
            <a class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editClassModal<?php echo $class['c_classID']; ?>">
            	<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            </a>
            <a class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteClassModal<?php echo $class['c_classID']; ?>">
            	<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
            </a>
            
            <!-- Edit Class Modal -->
            <div class="modal fade" 
            	id="editClassModal<?php echo $class['c_classID']; ?>" 
                tabindex="-1" 
                role="dialog" 
                aria-labelledby="editClassModalLabel<?php echo $class['c_classID']; ?>" 
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            
                            <h4 class="modal-title" id="editClassModalLabel<?php echo $class['c_classID']; ?>">
                                Edit a class
                            </h4>
                        </div>
                              
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="c_classname">Class name</label>
                                    <input type="text" class="form-control" id="c_classname<?php echo $class['c_classID']; ?>" value="<?php echo $class['c_classname']; ?>">
                                </div>
                              
                                <div class="form-group">
                                    <label for="c_gradelevel">Grade</label>
                                    <input type="text" class="form-control" id="c_gradelevel<?php echo $class['c_classID']; ?>" value="<?php echo $class['c_gradelevel']; ?>">
                                </div>
                              
                                <div class="form-group">
                                    <label for="c_room">Room Number</label>
                                    <input type="text" class="form-control" id="c_room<?php echo $class['c_classID']; ?>" value="<?php echo $class['c_room']; ?>">
                                </div>
                              
                                <div class="form-group">
                                    <label class="text" for="c_timeID">Time</label>
                                    <select name="time_dropdown" class="form-control">
                                        
                                        <!-- foreach loop to display options for dropdown -->
                                        <?php foreach ($timearray as $time) { ?>
                                            
                                            <option 
                                                value="<?php echo $time['time_ID']; ?>"
                                                id="c_timeID<?php echo $class['c_classID']; ?>"
                                                <?php if($class['c_timeID'] == $time['time_ID']) { echo "selected"; } ?> >
                                                    <?php echo $time['time_Desc']; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                              
                                <div class="form-group">
                                    <label class="text" for="c_sessionID">Session</label>
                                    <select name="session_dropdown" class="form-control">
                                        
                                        <!-- foreach loop to display options for dropdown -->
                                        <?php foreach ($sessionarray as $session) { ?>
                                            
                                            <option
                                                value="<?php echo $session['session_ID']; ?>"
                                                id="c_sessionID<?php echo $class['c_classID']; ?>"
                                                <?php if($class['c_sessionID'] == $session['session_ID']) { echo "selected"; } ?> >
                                                    <?php echo $session['session_Desc']; ?>
                                            </option>
                                            
                                        <?php } ?>
                                    </select>
                                </div>
                            </form>
                        </div>
                              
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close
                            </button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updatedata('<?php echo $class['c_classID']; ?>')">
                                Save Changes
                            </button>
                        </div>
                    </div>
                </div>
            </div>

			<!-- Delete Class Modal -->
            <div class="modal fade" 
            	id="deleteClassModal<?php echo $class['c_classID']; ?>" 
                tabindex="-1" 
                role="dialog" 
                aria-labelledby="deleteClassModalLabel<?php echo $class['c_classID']; ?>" 
                aria-hidden="true">
            	<div class="modal-dialog">
            		<div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            
                            <h4 class="modal-title" id="deleteClassModalLabel<?php echo $class['c_classID']; ?>">
                                Delete a class
                            </h4>
                        </div>
                        
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    Are you sure you want to delete class information for <em><strong><?php echo $class['c_classname']; ?></strong></em>?
                                </div>
                            </form>
                        </div>
                        
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                No
                            </button>
                            <!-- call the deletedata() function in classes.php when button is clicked, pass it the classID string as argument -->
                            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="deletedata('<?php echo $class['c_classID']; ?>')">
                                Yes
                            </button>
                        </div>
					</div>
                </div>
            </div>
		</td>
	</tr>
<?php } ?>
    </tbody>
</table>
<script src="js/instafilta.min.js"></script>
    <script src="js/demo.js"></script>