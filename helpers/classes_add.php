<?php
include "../includes/db_connect.php";
$conn = new mysqli($hostname, $username, $password, $database);
if ($conn->connect_errno) {
    echo "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
}
$classname = $_POST['classname'];
$gradelevel = $_POST['gradelevel'];
$room = $_POST['room'];
$timeID = $_POST['timeID'];
$sessionID = $_POST['sessionID'];
if($classname != null && $gradelevel != null && $room != null && $timeID != null && $sessionID != null){
	$stmt = $conn->prepare("INSERT INTO classes (classname, gradelevel, room, timeID, sessionID) VALUES (?,?,?,?,?)");
	$stmt->bind_param('sssii', $classname, $gradelevel, $room, $timeID, $sessionID);

if($stmt->execute()){
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> <?php echo $classname ?>'s information was added.
</div>
<?php
} else{
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error!</strong>
</div>
<?php
}
} else{
?> 
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong>
</div>
<?php
}
?>