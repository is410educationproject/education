<?php
include "../includes/db_connect.php";
$conn = new mysqli($hostname, $username, $password, $database);
if ($conn->connect_errno) {
    echo "Failed to connect to MySQL: (" . $conn->connect_errno . ") " . $conn->connect_error;
}
$sessionDesc = $_POST['sessionDesc'];
$sessionYear = $_POST['sessionYear'];
if($sessionDesc != null && $sessionYear != null){
	$stmt = $conn->prepare("INSERT INTO session (sessionDesc, sessionYear) VALUES (?,?)");
	$stmt->bind_param('ss', $sessionDesc, $sessionYear);

if($stmt->execute()){
?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Success!</strong> <?php echo $sessionDesc . " " . $sessionYear ?> was added.
</div>
<?php
} else{
?>
<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Error!</strong>
</div>
<?php
}
} else{
?> 
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong>
</div>
<?php
}
?>