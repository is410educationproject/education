<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = true;
$termselector = true;
$page_url="view_members.php?";

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/member.php';
include_once 'objects/familyposition.php';
include_once 'objects/user.php';
include_once 'objects/term.php';
include_once 'objects/documentation.php';

// instantiate database and member object
$database = new Database();
$db = $database->getConnection();

$member = new Member($db);
$familyposition = new FamilyPosition($db);
$userobject = new UserObject($db);
$term = new Term($db);

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';

// get documentation
$documentation = new Documentation($page_url);
$activetermDesc = $userobject->readActiveTerm();

// header settings
$page_title = "View Members";
include_once "includes/header.php";

// add the term selector to the page
include_once 'includes/termselect.inc.php';

include_once "helpers/pagination_config.php";

// get filter option that was selected from url
$filter = isset($_GET['filter']) ? $_GET['filter'] : '';

// query members
$stmtMembers = $member->readAll($from_record_num, $records_per_page);
$num = $stmtMembers->rowCount();

// query teachers
$stmtTeachers = $member->readAll_ByTeachers($from_record_num, $records_per_page);
$numTeachers = $stmtTeachers->rowCount();

// query subs
$stmtSubs = $member->readAll_BySubs($from_record_num, $records_per_page);
$numSubs = $stmtSubs->rowCount();

// query helpers
$stmtHelpers = $member->readAll_ByHelpers($from_record_num, $records_per_page);
$numHelpers = $stmtHelpers->rowCount();

include_once "includes/members_main.inc.php";

include_once "includes/footer.php";
?>