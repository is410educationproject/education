<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$page_url = 'update_time.php?';

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/time.php';
include_once 'objects/documentation.php';
include_once 'objects/term.php';
include_once 'objects/user.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare time object
$time = new Time($db);
$userobject = new UserObject($db);
$term = new Term($db);

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';

// get documentation
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Update Class Times";
include_once "includes/header_param.php";

include_once "includes/times_update.inc.php";

include_once "includes/footer.php";
?>