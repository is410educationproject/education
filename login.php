<?php session_start();
/*include_once 'includes/login/class.user.php';
$user = new User();
*/
if(isset($_SESSION['login'])){header("location:dashboard.php");exit();
}

// get page URL
$page_url = 'login.php?';

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/user.php';
include_once 'objects/documentation.php';

// instantiate database and department.php object
$database = new Database();
$db = $database->getConnection();

$userobject = new UserObject($db);
$documentation = new Documentation($page_url);

// header settings
//$page_title = "View Departments";
//include_once "includes/header.php";

// if the form was submitted
if($_POST){
	
	// set user property values
	$userobject->emailusername = $_POST['emailusername'];
	$userobject->password = md5($_POST['password']);
	
	$userobject->check_login();
	
	// try to login
	if($userobject->check_login()){
		// Login Success
		header("location:dashboard.php");
	}
	// if unable to login, tell the user
	else{
		echo "<div class=\"alert alert-danger alert-dismissable\">";
			echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
			echo "Incorrect email or password.";
		echo "</div>";
	}
}

	/*if (isset($_REQUEST['submit'])) { 
		extract($_REQUEST);   
	    $login = $user->check_login($emailusername, $password);
	    if ($login) {
			// Registration Success
			//header("location:dashboard.php");
			echo "success";
	    } else {
			// Registration Failed
			echo 'Wrong username or password';
	    }
	}*/
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable: no">

    <!-- bootstrap css --> 
    <link href="css/bootstrap.css" rel="stylesheet" media="screen" />
    <!-- custom site css -->
    <link rel="stylesheet" href="css/custom.css" />
    <link rel="stylesheet" href="css/login.css" />
    </head>
    <body>
      <form class="form-signin" method="post" action="" name="login">
        <h2 class="form-signin-heading">Login Here</h2>
        <label for="emailusername" class="sr-only">Email address</label>
        <input type="text" id="emailusername" name="emailusername" class="form-control" placeholder="Email address" required autofocus="">
        <label for="password" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" value="login" type="submit" onClick="return(submitlogin());" >Sign in</button>
      </form>
    </body>
    </html>
        
<?php include_once "includes/footer.php"; ?>