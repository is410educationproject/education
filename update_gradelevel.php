<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$page_url = 'update_gradelevel.php?';

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/gradelevel.php';
include_once 'objects/documentation.php';
include_once 'objects/term.php';
include_once 'objects/user.php';
				
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare gradelevel object
$gradelevel = new gradelevel($db);
$userobject = new UserObject($db);
$term = new Term($db);

// add the term selector update code 
include_once 'includes/termselect_update.inc.php';

// get documentation
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Update Grade Level";
include_once "includes/header_param.php";

include_once "includes/gradelevels_update.inc.php";

include_once "includes/footer.php";
?>