//JavaScript for deactivate function
function deactivate_teachers()
	{	
		var selectedIDs = $("input:checked").
		map(function() { return this.id; }).get();
		console.log(selectedIDs);
		if(selectedIDs.length == 0) {
			alert("Please choose a staff member to deactivate.");
			return false;
		}
		if (confirm("Are you sure you want to deactivate this staff member?") == true)
		{
			$.ajax({
				type: 'POST',
				url: 'view_teachers_deactivate_button.php',
				data: {selected: selectedIDs},
				success: function(data) {
					alert("Staff member has been deactivated.");		
					//$("#demo").html("Staff member is deactivated!");
					window.location.reload(true)
				}
			});
		}
		else
		{
			return false;
		}
	}