<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$globalpage = true;
$page_url = "view_staffpositions.php?";

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// include database and object files
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/staffposition.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';
include_once 'objects/term.php';

// instantiate database and status.php object
$database = new Database();
$db = $database->getConnection();

$position = new StaffPosition($db);
$userobject = new UserObject($db);
$term = new Term($db);

// get documentation
$documentation = new Documentation($page_url);

// header settings
$page_title = "View Staff Positions";
include_once "includes/header_param.php";

// query statuses
$stmt = $position->readAll();
$num = $stmt->rowCount();

include_once "includes/staffpositions_main.inc.php";

include_once "includes/footer.php";
?>