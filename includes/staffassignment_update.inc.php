<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}

$departmentID = isset($_GET['departmentID']) ? $_GET['departmentID'] : 1;
$assignmentID = isset($_GET['assignmentID']) ? $_GET['assignmentID'] : 1;
$positionID = isset($_GET['positionID']) ? $_GET['positionID'] : 1;
$route = isset($_GET['route']) ? $_GET['route'] : 1;
$classID = isset($_GET['classID']) ? $_GET['classID'] : 1;
$search = isset($_GET['search']) ? $_GET['search'] : 1;
$search_term = isset($_GET['s']) ? $_GET['s'] : 1;

echo "<div class='row'>";
// view class profile button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='view_class_info.php?classID={$classID}&departmentID={$departmentID}' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> Back to Class Profile";
	echo "</a>";
echo "</div>";
echo "</div>";


echo "<div class='row no-margin-bottom'>";
// set ID property of staff assignment to be edited
$assignment->assignmentID = $assignmentID;
$assignment->classID = $classID;
$class->classID = $classID;
$position->activeuserID = $activeuserID;

// if the form was submitted
if($_POST){

	try{
		// data validation
		if($_POST['positionID']==0){ 
			echo "<div class='alert alert-danger'>Please select a date of contact.</div>";
		}
		
		else if($_POST['memberID']==0){ 
			echo "<div class='alert alert-danger'>Please select a method of contact from the dropdown.</div>";
		}
		
		else{
			
			// set contact event property values
			$assignment->memberID = $_POST['memberID'];
			$assignment->positionID = $_POST['positionID'];
			$assignment->classID = $_POST['classID'];
			
			
			// update the staff assignment
			if($assignment->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The staff assignment was updated.";
				echo "</div>";
			}
			
			// if unable to update the staff assignment, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the staff assignment.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$memberID = $_POST['memberID'];
		$positionID = $_POST['positionID'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of family position to be edited
	$assignment->readOne();
}
?>
	
<!-- HTML form for updating a family position -->
<form action='update_staffassignment.php?<?php if(isset($route) && $route==1 && isset($search) && $search=='true'){
					echo "departmentID={$departmentID}&positionID={$positionID}&assignmentID={$assignmentID}&search=true&s={$search_term}&route=1";					
				} 
				elseif(isset($route) && $route==1){
					echo "departmentID={$departmentID}&positionID={$positionID}&assignmentID={$assignmentID}&route=1";
				}
				elseif(isset($route) && $route==2){
                    echo "departmentID={$departmentID}&classID={$classID}&assignmentID={$assignmentID}&route=2";
            	} 
				?>' method='post'>
 
	<table class='table table-hover table-responsive table-bordered'>
	 
     <input type='hidden' name='classID' class='form-control' value="<?php echo "$assignment->classID"; ?>" />
		 
		<tr>
			<td>Member</td>
			<td>
				<?php
				$stmt = $member->read();
				//<input type="text" name="typeahead">?>
                <!--<div id="prefetch">
                    <input class="typeahead" type="text" placeholder="Type a member's name">
                </div>-->
                
                <!--<br>
                <div class="well col-md-5">
                    <input id="demo1" type="text" class="col-md-12 form-control" autocomplete="off" placeholder="Search cities..." />
                </div>-->
                 
				<?php
				// put them in a select drop-down
				echo "<select class='form-control' name='memberID'>";
				
					echo "<option>Please select a member...</option>";
					while ($row_member = $stmt->fetch(PDO::FETCH_ASSOC)){
						extract($row_member);
						
						if($isTeacher == 1 || $isSub == 1 || $isHelper == 1) {
							// name of member must be selected
							if($assignment->memberID==$memberID){
								echo "<option value='{$memberID}' selected>";
							}else{
								echo "<option value='{$memberID}'>";
							}
							
							echo "{$lname}, {$fname}</option>";
						}
					}
				echo "</select>";
				?>
			</td>
		</tr>
		 
		<tr>
			<td>Staff Position</td>
			<td>
				<?php
				$stmt = $position->read();
				
				// put them in a select drop-down
				echo "<select class='form-control' name='positionID'>";
				
					echo "<option>Please select a staff position...</option>";
					while ($row_position = $stmt->fetch(PDO::FETCH_ASSOC)){
						extract($row_position);
						
						// staff position must be selected
						if($assignment->positionID==$positionID){
							echo "<option value='{$positionID}' selected>";
						}else{
							echo "<option value='{$positionID}'>";
						}
						
						echo "{$positionDesc}</option>";
					}
				echo "</select>";
				?>
			</td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-edit"></span> Update Staff Assignment
				</button>
                <?php if(isset($route) && $route==1 && isset($search) && $search=='true'){ ?>
					<a href='search_staffassignments.php?<?php 
						 echo "departmentID={$departmentID}&positionID={$positionID}&search=true&s={$search_term}&route=1";
					?>' class='btn btn-danger'>
						<span class='glyphicon glyphicon-remove'></span> Cancel
					</a>
				<?php } 
				elseif(isset($route) && $route==2){ ?>
                    <a href='view_class_info.php?<?php
                    	echo "classID={$classID}&departmentID={$departmentID}&route=2";
					?>' class='btn btn-danger'>
						<span class='glyphicon glyphicon-remove'></span> Cancel
                    </a>
            	<?php } 
				else{ ?>
					<a href='view_staffassignments.php?<?php
					$positionID = isset($_GET['positionID']) ? $_GET['positionID'] : 1;
                    echo "departmentID={$departmentID}&positionID={$positionID}&route=1";
					?>' class='btn btn-danger'>
						<span class='glyphicon glyphicon-remove'></span> Cancel
                    </a>
				<?php }?>
			</td>
		</tr>
	</table>
</form>
</div>