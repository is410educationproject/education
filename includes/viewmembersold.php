<?php
error_reporting(E_ALL);

// speed things up with gzip plus ob_start() is required for csv export
if(!ob_start('ob_gzhandler'))
	ob_start();

include('lazy_mofo.php');

//edit here adam

// enter your database host, name, username, and password
$db_host = '127.0.0.1';
$db_name = 'edu';
$db_user = 'root';
$db_pass = 'edumain';


// connect with pdo 
try {
	$dbh = new PDO("mysql:host=$db_host;dbname=$db_name;", $db_user, $db_pass);
}
catch(PDOException $e) {
	die('pdo connection error: ' . $e->getMessage());
}


// create LM object, pass in PDO connection
$lm = new lazy_mofo($dbh); 


// table name for updates, inserts and deletes
$lm->table = 'members';


// identity / primary key for table
$lm->identity_name = 'memberID';


// optional, make friendly names for fields
$lm->rename['fname'] = 'First Name';
$lm->rename['lname'] = 'Last Name';
$lm->rename['familypositiondesc'] = 'Family Position';
$lm->rename['memberstatus'] = 'Member Status';
$lm->rename['dob'] = 'Date of Birth';
$lm->rename['phone'] = 'Phone';
$lm->rename['phcell'] = 'Cell Phone';
$lm->rename['email'] = 'Email Address';



// optional, define input controls on the form
//$lm->form_input_control['photo'] = '--image';
//$lm->form_input_control['is_active'] = "select 1, 'Yes' union select 0, 'No' union select 2, 'Maybe'; --radio";
$lm->form_input_control['familyposition'] = 'SELECT familypositionID, familypositiondesc FROM familypositions; --select';


// optional, define editable input controls on the grid
//$lm->grid_input_control['is_active'] = '--checkbox';


// optional, define output control on the grid 
//$lm->grid_output_control['contact_email'] = '--email'; // make email clickable
//$lm->grid_output_control['photo'] = '--image'; // image clickable  


// new in version >= 2015-02-27 all searches have to be done manually
$lm->grid_show_search_box = true;


// optional, query for grid(). LAST COLUMN MUST BE THE IDENTITY for [edit] and [delete] links to appear
$lm->grid_sql = "
SELECT 
  m.memberID
, m.fname
, m.lname
, f.familypositiondesc
, m.memberstatus
, m.dob
, m.phone
, m.phcell
, m.email 
, m.memberID
FROM  members m
left  
join  familypositions f 
on    m.familyposition = f.familypositionID  
WHERE coalesce(m.fname, '') like :_search 
or    coalesce(m.lname, '') like :_search 
or    coalesce(m.phone, '') like :_search
or    coalesce(m.phcell, '') like :_search 
or    coalesce(m.email, '') like :_search 
ORDER BY m.memberID
";
$lm->grid_sql_param[':_search'] = '%' . trim(@$_REQUEST['_search']) . '%';


// optional, define what is displayed on edit form. identity id must be passed in also.  
$lm->form_sql = "
SELECT 
  m.memberID
, m.fname
, m.lname
, f.familypositiondesc
, m.memberstatus
, m.dob
, m.phone
, m.phcell
, m.email
FROM  members m
left  
join  familypositions f 
on    m.familyposition = f.familypositionID  
WHERE memberID = :memberID
";
$lm->form_sql_param[":$lm->identity_name"] = @$_REQUEST[$lm->identity_name]; 


// optional, validation. input:  regular expression (with slashes), error message, tip/placeholder
// first element can also be a user function or 'email'
$lm->on_insert_validate['market_name'] = array('/.+/', 'Missing Market Name', 'this is required'); 
$lm->on_insert_validate['contact_email'] = array('email', 'Invalid Email', 'this is optional', true); 


// copy validation rules to update - same rules
$lm->on_update_validate = $lm->on_insert_validate;  


// use the lm controller
$lm->run();


?>