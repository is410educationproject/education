<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/preference.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare preference.php object
	$preference = new Preference($db);
	
	// set preferenceID to be deleted
	$preference->preferenceID = $_POST['object_id'];
	
	// delete the preference
	if($preference->delete()){
		echo "The preference was deleted.";
	}
	
	// if unable to delete the preference
	else{
		echo "Unable to delete the preference.";	
	}
}
?>