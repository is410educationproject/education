<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}

// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/term.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare term.php object
	$term = new term($db);
	
	// set termID to be deleted
	$term->termID = $_POST['object_id'];
	
	// delete the term
	if($term->delete()){
		echo "The term was deleted.";
	}
	
	// if unable to delete the term
	else{
		echo "Unable to delete the term.";	
	}
}
?>