<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view family positions button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_familypositions.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View family positions";
//	echo "</a>";
//echo "</div>";

// get ID of the family position to be edited
$familypositionID = isset($_GET['familypositionID']) ? $_GET['familypositionID'] : die('ERROR: missing ID.');

// set ID property of family position to be edited
$familyposition->familypositionID = $familypositionID;

// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['familypositiondesc'])){ 
			echo "<div class='alert alert-danger'>The family position description cannot be empty.</div>";
		}
		
		else{
			
			// set family position property values
			$familyposition->familypositiondesc = $_POST['familypositiondesc'];

			// update the famil yposition
			if($familyposition->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The family position was updated. <a href='view_familypositions.php'>Return to View Family Positions</a>";
				echo "</div>";
			}
			
			// if unable to update the familyposition, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the family position.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$familypositiondesc = $_POST['familypositiondesc'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of family position to be edited
	$familyposition->readOne();
}
?>
<div class="row no-margin-bottom">
<!-- HTML form for updating a family position -->
<form action='update_familyposition.php?familypositionID=<?php echo $familypositionID; ?>' method='post'>
 
	<table class='table table-hover table-responsive table-bordered'>
	 
		<tr>
			<td>Family Position Description</td>
			<td><input type='text' name='familypositiondesc' value="<?php echo htmlspecialchars($familyposition->familypositiondesc, ENT_QUOTES, 'UTF-8'); ?>" class=
            'form-control' required></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update family position
				</button>
                 <a href='view_familypositions.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>