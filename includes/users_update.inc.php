<?php
// view user button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='view_users.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> View users";
	echo "</a>";
echo "</div>";

// get ID of the user to be edited
$userID = isset($_GET['userID']) ? $_GET['userID'] : die('ERROR: missing ID.');

// set ID property of user to be edited
$userobject->userID = $userID;

// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['fname'])){ 
			echo "<div class='alert alert-danger'>The first name field cannot be empty.</div>";
		}
		
		else{
			
			// set user property values
			$userobject->fname = $_POST['fname'];
			$userobject->sessionID = $_POST['sessionID'];

			// update the user
			if($userobject->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The user was updated.";
				echo "</div>";
			}
			
			// if unable to update the user, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the user.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$fname = $_POST['fname'];
		$sessionID = $_POST['sessionID'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of user to be edited
	$userobject->readOne();
}
?>
	
<!-- HTML form for updating a user -->
<form action='update_user.php?userID=<?php echo $userID; ?>' method='post'>
 
	<table class='table table-hover table-responsive table-bordered'>
	 
		<tr>
			<td>First Name</td>
			<td><input type='text' name='fname' value="<?php echo htmlspecialchars($userobject->fname, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
		
        <tr>
        	<td>Active Session</td>
            <td>
        	<?php
            $stmt = $session->readSessionSelector();
            
            // put them in a select drop-down
            echo "<select class='form-control' name='sessionID'>";
            
                echo "<option>Please select...</option>";
                while ($row_session = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row_session);
                    
                    // current family position of the member must be selected
                    if($userobject->sessionID==$sessionID){
                        echo "<option value='$sessionID' selected>";
                    }else{
                        echo "<option value='$sessionID'>";
                    }
                    
                    echo "$sessionDesc</option>";
                }
            echo "</select>";
            ?>
        </div>
        
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update user
				</button>
                 <a href='view_users.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
		 
	</table>
</form>