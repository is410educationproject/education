<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view members button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_class_info.php?classID={$classID}' class='btn btn-primary pull-left'>";
//		echo "<span class='glyphicon glyphicon-list'></span> Back to Class Profile";
//	echo "</a>";
//echo "</div>";

// set ID property of member to be edited
$assignment->classID = $classID;
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['memberID'])){ 
			echo "<div class='alert alert-danger'>Please select a date of contact.</div>";
		}
		
		else if($_POST['positionID']==0){ 
			echo "<div class='alert alert-danger'>Please select a method of contact from the dropdown.</div>";
		}		
		
		else{
			// set contact event property values
			$assignment->classID = $_POST['classID'];
			$assignment->memberID = $_POST['memberID'];
			$assignment->positionID = $_POST['positionID'];
			$assignment->termID = $userobject->activetermID;
			
			// add the contact event
			if($assignment->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "<span class='glyphicon glyphicon-thumbs-up'></span> Member was assigned to class. <a href='view_class_info.php?classID={$classID}'>Return to Class Profile</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the contact event, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to assign member to class.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
	 
<!-- HTML form for adding a contact event -->
<form action='add_staffassignment.php?<?php echo "classID={$classID}&departmentID={$departmentID}"; ?>' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<input type='hidden' name='classID' class='form-control' value="<?php echo "$assignment->classID"; ?>" />

		 
		<tr>
			<td>Member</td>
			<td>
				<?php
                // read the family positions from the database
                $stmt = $member->read();
                
                // put them in a select drop-down
                echo "<select class='form-control' name='memberID'>";
                    echo "<option>Select a member...</option>";
                    
                    while ($row_member = $stmt->fetch(PDO::FETCH_ASSOC)){
                        extract($row_member);
                        if($isTeacher == 1 || $isSub == 1 || $isHelper == 1) {
							echo "<option value='{$memberID}'>{$lname}, {$fname}</option>";
						}
                    }
                    
                echo "</select>";
                ?>
			</td>
		</tr>
		 
		<tr>
			<td>Staff Position</td>
			<td>
				<?php
                // read the staff positions from the database
                $stmt = $position->read();
                
                // put them in a select drop-down
                echo "<select class='form-control' name='positionID'>";
                    echo "<option>Select staff position...</option>";
                    
                    while ($row_position = $stmt->fetch(PDO::FETCH_ASSOC)){
                        extract($row_position);
                        echo "<option value='{$positionID}'>{$positionDesc}</option>";
                    }
                    
                echo "</select>";
                ?>
			</td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Assign Staff Member
				</button>
                 <a href='view_class_info.php?<?php echo "classID={$classID}&departmentID={$departmentID}"; ?>' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>