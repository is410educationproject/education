<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/gradelevel.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare gradelevel.php object
	$gradelevel = new gradelevel($db);
	
	// set gradelevelID to be deleted
	$gradelevel->gradelevelID = $_POST['object_id'];
	
	// delete the gradelevel
	if($gradelevel->delete()){
		echo "The grade level was deleted.";
	}
	
	// if unable to delete the gradelevel
	else{
		echo "Unable to delete the grade level.";	
	}
}
?>