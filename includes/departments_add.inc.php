<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view department button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_departments.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View Departments";
//	echo "</a>";
//echo "</div>";
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['departmentdesc'])){ 
			echo "<div class='alert alert-danger'>Department</div>";
		}
		
		else if(!isset($userobject->activetermID)){
			echo "<div class='alert alert-danger'>A term was not set.</div>";
		}
		
		else{
			// set department property values
			$department->departmentdesc = $_POST['departmentdesc'];
			$department->termID = $userobject->activetermID;
			
			// add the department
			if($department->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The department was added. <a href='view_departments.php'>Return to View Departments</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the department, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add department.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
	 
<!-- HTML form for adding a department-->
<form action='add_department.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Department Description</td>
			<td><input type='text' name='departmentdesc' class='form-control' required value="<?php echo isset($_POST['departmentdesc']) ? htmlspecialchars($_POST['departmentdesc'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add Department
				</button>
                 <a href='view_departments.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>