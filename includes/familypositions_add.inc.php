<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view family positions button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_familypositions.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View Family Positions";
//	echo "</a>";
//echo "</div>";
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['familypositiondesc'])){ 
			echo "<div class='alert alert-danger'>Family Position</div>";
		}
		
		else{
			// set family position property values
			$familyposition->familypositiondesc = $_POST['familypositiondesc'];
			
			// add the family position
			if($familyposition->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The family position was added. <a href='view_familypositions.php'>Return to View Family Positions</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the family position, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add the family position.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
<div class="row no-margin-bottom"> 
<!-- HTML form for adding a family position -->
<form action='add_familyposition.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Family Position description</td>
			<td><input type='text' name='familypositiondesc' class='form-control' required value="<?php echo isset($_POST['familypositiondesc']) ? htmlspecialchars($_POST['familypositiondesc'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add family position
				</button>
                 <a href='view_familypositions.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>