<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view times button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_times.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View class times";
//	echo "</a>";
//echo "</div>";
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['timeDesc'])){ 
			echo "<div class='alert alert-danger'>Please enter a class time.</div>";
		}
		
		else if(!isset($userobject->activetermID)){
			echo "<div class='alert alert-danger'>A term was not set.</div>";
		}
		
		else{
			// set time property values
			$time->timeDesc = $_POST['timeDesc'];
			$time->termID = $userobject->activetermID;
			
			// add the time
			if($time->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The class time was added. <a href='view_times.php'>Return to View Times</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the time, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add the class time.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
<div class="row no-margin-bottom"> 
<!-- HTML form for adding a time -->
<form action='add_time.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Class time description</td>
			<td><input type='text' name='timeDesc' class='form-control' required value="<?php echo isset($_POST['timeDesc']) ? htmlspecialchars($_POST['timeDesc'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add class time
				</button>
                 <a href='view_times.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>