<?php
// view statuses button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_statuses.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View statuses";
//	echo "</a>";
//echo "</div>";

// get ID of the status to be edited
$statusID = isset($_GET['statusID']) ? $_GET['statusID'] : die('ERROR: missing ID.');

// set ID property of status to be edited
$status->statusID = $statusID;

// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['statusDesc'])){ 
			echo "<div class='alert alert-danger'>The status description cannot be empty.</div>";
		}
		
		else{
			
			// set status property values
			$status->statusDesc = $_POST['statusDesc'];

			// update the status
			if($status->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The status was updated. <a href='view_statuses.php'>Return to View Statuses</a>";
				echo "</div>";
			}
			
			// if unable to update the status, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the status.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$statusDesc = $_POST['statusDesc'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of status to be edited
	$status->readOne();
}
?>
<div class="row no-margin-bottom">
<!-- HTML form for updating a time -->
<form action='update_status.php?statusID=<?php echo $statusID; ?>' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
	 
		<tr>
			<td>Status description</td>
			<td><input type='text' name='statusDesc' value="<?php echo htmlspecialchars($status->statusDesc, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update status
				</button>
                 <a href='view_statuses.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
		 
	</table>
</form>
</div>