<?php
// view gradelevels button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_gradelevels.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View gradelevels";
//	echo "</a>";
//echo "</div>";

// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['gradelevelDesc'])){ 
			echo "<div class='alert alert-danger'>Please enter a gradelevel.</div>";
		}
		
		else{
			// set gradelevel property values
			$gradelevel->gradelevelDesc = $_POST['gradelevelDesc'];
			$gradelevel->termID = $userobject->activetermID;
			
			// add the gradelevel
			if($gradelevel->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The class gradelevel was added. <a href='view_gradelevels.php'>Return to View gradelevels</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the gradelevel, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add the class gradelevel.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
<div class="row no-margin-bottom">
<!-- HTML form for adding a time -->
<form action='add_gradelevel.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Grade Level Description</td>
			<td><input type='text' name='gradelevelDesc' class='form-control' required value="<?php echo isset($_POST['gradelevelDesc']) ? htmlspecialchars($_POST[
			'gradelevelDesc'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add gradelevel
				</button>
                 <a href='view_gradelevels.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>