<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}

?>
<div class='row'>

	<!--<form role="search" action='search_departmentpositions.php'>
		<div class="input-group col-md-3 pull-left margin-right-1em">
			<input type="text" class="form-control" placeholder="Type department description..." name="s" id="srch-term" required <?php echo isset($search_term) ? "value='$search_term'" : ""; ?> />
			<div class="input-group-btn">
				<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			</div>
		</div>
	</form>-->
	
	<!--<form role="search" action='../search_products_by_date_range.php'>
		<div class="input-group col-md-3 pull-left">
			<input type="text" class="form-control" placeholder="Date from..." name="date_from" id="date-from" required 
				<?php echo isset($date_from) ? "value='$date_from'" : ""; ?> />
			<span class="input-group-btn" style="width:0px;"></span>
			
			<input type="text" class="form-control" placeholder="Date to..." name="date_to" id="date-to" 
				required <?php echo isset($date_to) ? "value='$date_to'" : ""; ?> />
			<div class="input-group-btn">
				<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			</div>
		</div>
	</form>-->
	
	<!-- add department button -->
	<a href='add_department.php' class="btn btn-primary pull-right">
		<span class="glyphicon glyphicon-plus"></span> Add department
	</a>
	
	<!-- delete selected records -->
	<!--<button class="btn btn-danger pull-right margin-bottom-1em margin-right-1em" id="delete-selected">
		<span class="glyphicon glyphicon-remove-circle"></span> Delete Selected
	</button>-->
	
</div>
<div class="row no-margin-bottom">
<?php 
// display the department positions if there are any
if($num>0){
	
	echo "<table class='table table-hover table-responsive table-bordered no-margin-bottom'>";
		echo "<tr>";
			echo "<th>";
				echo "Department";
			echo "</th>";
			echo "<th>";
				echo "Actions";
			echo "</th>";
		echo "</tr>";
		
		while ($row = $stmtDepartment->fetch(PDO::FETCH_ASSOC)){
		
			extract($row);
			
			echo "<tr>";
				//echo "<td class='text-align-center'><input type='checkbox' name='item[]' class='checkboxes' value='{$departmentID}' /></td>";
				echo "<td>{$departmentdesc}</td>";
				echo "<td>";
					
					// edit department button + FIND OUT WHY THIS BUTTON IS SLIGHTLY SMALLER
					echo "<a href='update_department.php?departmentID={$departmentID}' class='btn btn-sm btn-info right-margin'>";
						echo "<span class='glyphicon glyphicon-pencil'></span>";
					echo "</a>";
					
					// contact events button
					echo "<a href='view_department_info.php?departmentID={$departmentID}' class='btn btn-sm btn-success right-margin'>";
						echo "<span class='glyphicon glyphicon-eye-open'></span>";
					echo "</a>";
					
					// delete department button + FIND OUT WHY THIS BUTTON IS SLIGHTLY SMALLER
					echo "<a delete-id='{$departmentID}' delete-file='includes/departments_delete.inc.php' class='btn btn-sm btn-danger delete-object'>";
						echo "<span class='glyphicon glyphicon-trash'></span>";
					echo "</a>";
					
				echo "</td>";
				
			echo "</tr>";
			
		}
		
	echo "</table>";
}

// tell the user there are no departments
else{
	echo "<div class=\"alert alert-danger alert-dismissable\">";
		echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		echo "No departments found.";
	echo "</div>";
}
?>
</div>