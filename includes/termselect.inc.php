<?php
// check for session
//session_start();

include_once 'login/class.user.php';
$user = new User($db);
if (!$user->logged_in()){
	header("location:../login.php");
}

// if the active term was updated, tell the user
if(isset($termchanged) && $termchanged==true){
	echo "<div class=\"alert alert-success alert-dismissable margin-bottom-1em\">";
		echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		echo "The active term was changed.";
	echo "</div>";
}

// if the active term was not updated, tell the user
elseif(isset($termchanged) && $termchanged==false){
	echo "<div class=\"alert alert-danger alert-dismissable margin-bottom-1em\">";
		echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		echo "Unable to change the active term.";
	echo "</div>";
}
?>

