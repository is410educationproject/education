<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}

?>

	<div class="row add-margin-top">
        <div class='col-xs-7 col-sm-4 col-md-3 pull-left no-padding'>
        <form role="search" action='search_members.php'>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Type member name..." name="s" id="srch-term" required <?php echo isset($search_term) ? 
				"value='$search_term'" : ""; ?> />
                <div class="input-group-btn">
                    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
            </div>
        </form>
        </div>
    <!--<div class='col xs-3 col-sm-2 col-md-3 pull-left'>
    <form role="filter" action='view_members.php'>
        <div class="input-group">
            <select id="filter-select" multiple>
                <option value="teachers" <?php if($filter == "teachers"){ echo "selected"; } ?>>View all teachers</option>
                <option value="subs" <?php if($filter == "subs"){ echo "selected"; } ?>>View all subs</option>
                <option value="helpers" <?php if($filter == "helpers"){ echo "selected"; } ?>>View all helpers</option>
            </select>
        </div>
	</form>
    </div>-->
    
    
	<!--<form role="search" action='../search_products_by_date_range.php'>
		<div class="input-group col-md-3 pull-left">
			<input type="text" class="form-control" placeholder="Date from..." name="date_from" id="date-from" required 
				<?php echo isset($date_from) ? "value='$date_from'" : ""; ?> />
			<span class="input-group-btn" style="width:0px;"></span>
			
			<input type="text" class="form-control" placeholder="Date to..." name="date_to" id="date-to" 
				required <?php echo isset($date_to) ? "value='$date_to'" : ""; ?> />
			<div class="input-group-btn">
				<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			</div>
		</div>
	</form>-->
	
	<!-- create member button -->
	<a href='add_member.php' class="btn btn-primary pull-right">
		<span class="glyphicon glyphicon-plus"></span> Add Member
	</a>
	
	<!-- export members to CSV -->
	<a href='helpers/export_csv.php' class="btn btn-info pull-right margin-right-1em hidden-xs">
		<span class="glyphicon glyphicon-download-alt"></span> Export CSV
	</a>
	</div> 

<?php 
//$memberID = 1000;
//	$member->countContactEvents($memberID);
// display the members if there are any
//if($filter == "teachers"){
//	$numOption = $numTeachers;
//} else if($filter == "subs"){
//	$numOption = $numSubs;
//} else if($filter == "helpers"){
//	$numOption = $numHelpers;
//} else if($filter == "none"){
//	$numOption = $numMembers;
//} else {
//	$numOption = $numMembers;
//}
echo "<div class='row no-margin-bottom'>";
if($num>0){
	echo "<table class='table table-hover table-condensed table-responsive table-bordered'>";
		echo "<tr>";
			echo "<th style='width:20%;'>";
					echo "Full Name";
			echo "</th>";
			echo "<th style='width:20%;' class='hidden-sm hidden-xs'>";
					echo "Phone Number ";
			echo "</th>";
			echo "<th style='width:20%;' class='hidden-sm hidden-xs'>";
					echo "Family Position ";
			echo "</th>";
			echo "<th style='width:20%;' class='hidden-sm hidden-xs'>";
					echo "Email address ";
			echo "</th>";
			echo "<th style='width:20%;'>Actions</th>";
		echo "</tr>";

		
		//if($filter == "teachers"){
//			$filteroption = $stmtTeachers;
//		} else if($filter == "subs"){
//			$filteroption = $stmtSubs;
//		} else if($filter == "helpers"){
//			$filteroption = $stmtHelpers;
//		} else if($filter == "none"){
//			$filteroption = $stmtMembers;
//		} else {
//			$filteroption = $stmtMembers;
//		}
			
			while ($row = $stmtMembers->fetch(PDO::FETCH_ASSOC)){
		
		
			extract($row);
			
			echo "<tr style='";
			if($ceCount>= 1){
				echo "background-color: #dff0d8'>";
			}
			else{
				echo "background-color: white'>";
			}
				//echo "<td class='text-align-center'><input type='checkbox' name='item[]' class='checkboxes' value='{$memberID}' /></td>";
				echo "<td>{$lname}, {$fname}</td>";
				//echo "<td>&#36;" . number_format($price, 2) . "</td>";
				echo "<td class='hidden-sm hidden-xs'><a href='tel:{$phone}'>{$phone}</a></td>";
				echo "<td class='hidden-sm hidden-xs'>{$f_familypositiondesc}</td>";
				echo "<td class='hidden-sm hidden-xs'><a href='mailto:{$email}'>{$email}</a></td>";
				//echo "<td>{$created}</td>";
				echo "<td>";
					
					// edit member button
					echo "<a href='update_member.php?memberID={$memberID}' class='btn btn-sm btn-info right-margin'>";
						echo "<span class='glyphicon glyphicon-pencil'></span>";
					echo "</a>";
					
					// contact events button
					echo "<a href='view_member_info.php?memberID={$memberID}' class='btn btn-sm btn-success right-margin'>";
						echo "<span class='glyphicon glyphicon-eye-open'></span>";
					echo "</a>";
					
					// delete member button
					echo "<a delete-id='{$memberID}' delete-file='includes/members_delete.inc.php' class='btn btn-sm btn-danger delete-object'>";
						echo "<span class='glyphicon glyphicon-trash'></span>";
					echo "</a>";
					
				echo "</td>";
				
			echo "</tr>";
			
		}
		
	echo "</table>";
	
	// needed for paging
	$total_rows=0;
	
	if($page_url == "view_members.php?"){
		$total_rows=$member->countAll();
	}
	
	//if($filter == "none"){
//		$total_rows=$member->countAll();
//	}
//	
//	else if($filter == "teachers"){
//		$total_rows=$member->countAll_ByTeachers();
//	}
//	
//	else if($filter == "subs"){
//		$total_rows=$member->countAll_BySubs();
//	}
//	
//	else if($filter == "helpers"){
//		$total_rows=$member->countAll_ByHelpers();
//	}
	
	else if(isset($category_id) && $page_url=="category.php?id={$category_id}&"){
		$total_rows=$member->countAll_ByCategory();
	}
	
	else if(isset($search_term) && $page_url=="search_members.php?s={$search_term}&"){
		$total_rows=$member->countAll_BySearch($search_term);
	}
	else if(isset($teacherfilter) && $page_url=="view_members.php?filter={$teacherfilter}&"){
		$total_rows=$member->countAll_ByTeacher($teacherfilter);
	}
	
	else if(isset($field) && isset($order) && $page_url=="read_products_sorted_by_fields.php?field={$field}&order={$order}&"){
		$total_rows=$member->countAll();
	}
	
	// search by date range
	else if(isset($date_from) && isset($date_to) 
				&& $page_url=="search_products_by_date_range.php?date_from={$date_from}&date_to={$date_to}&"){
		$total_rows=$member->countSearchByDateRange($date_from, $date_to);	
	}
else {
$total_rows=$member->countAll();
}
	
	// paging buttons
	include_once 'helpers/paging.php';
}


// tell the user there are no members
else{
	echo "<div class=\"alert alert-danger alert-dismissable\">";
		echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		echo "No members found.";
	echo "</div>";
}
echo "</div>";
?>