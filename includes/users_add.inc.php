<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view sessions button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='view_sessions.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> View users";
	echo "</a>";
echo "</div>";

// if the form was submitted
if($_POST){
	
	// Security measures
        $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647)); 
        $password = hash('sha256', $_POST['password'] . $salt); 
        for($round = 0; $round < 65536; $round++){ $password = hash('sha256', $password . $salt); } 
        $query_params = array( 
            ':email' => $_POST['email'], 
            ':password' => $password, 
            ':salt' => $salt
        ); 

	$query = " 
		 SELECT 
			1 
		FROM users 
		WHERE 
			email = :email 
		" ; 
		$query_params = array( 
			':email' => $_POST['email'] 
	);
	
	try { 
		$stmt = $db->prepare($query); 
		$result = $stmt->execute($query_params); 
	} 
	catch(PDOException $ex){ die("Failed to run query: " . $ex->getMessage());} 
	$row = $stmt->fetch(); 
	if($row){ die("This email address is already registered"); } 	


	try{
		
		$stmt = $db->prepare($query); 
        $result = $stmt->execute($query_params); 
		
		// data validation
		if(empty($_POST['fname'])){ 
			echo "<div class='alert alert-danger'>Please enter a session description.</div>";
		}
		
		else if(empty($_POST['lname'])){ 
			echo "<div class='alert alert-danger'>Please enter a session description.</div>";
		}
		
		else if(empty($_POST['email'])){ 
			echo "<div class='alert alert-danger'>Please enter a session description.</div>";
		}
		
		else if(empty($_POST['password'])){ 
			echo "<div class='alert alert-danger'>Please enter a session description.</div>";
		}
		
		else{
			// set session property values
			$user->fname = $_POST['fname'];
			$user->lname = $_POST['lname'];
			$user->email = $_POST['email'];
			$user->password = $_POST['password'];
			$user->salt = $salt;
			
			// add the session
			if($user->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The session was added.";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the session, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add the session.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
	 
<!-- HTML form for adding a session -->
<form action='add_user.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>First Name</td>
			<td><input type='text' name='fname' class='form-control' required value="<?php echo isset($_POST['fname']) ? htmlspecialchars($_POST['fname'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
        
        <tr>
			<td>Last Name</td>
			<td><input type='text' name='lname' class='form-control' required value="<?php echo isset($_POST['lname']) ? htmlspecialchars($_POST['lname'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
        
        <tr>
			<td>Email</td>
			<td><input type='text' name='email' class='form-control' required value="<?php echo isset($_POST['email']) ? htmlspecialchars($_POST['email'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
        
        <tr>
			<td>Password</td>
			<td><input type='password' name='password' class='form-control' required value="<?php echo isset($_POST['password']) ? htmlspecialchars($_POST['password'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add user
				</button>
                 <a href='view_users.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>