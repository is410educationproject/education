<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}

// set properties of preference to be added
$preference->memberID = $memberID;
$department->activeuserID = $activeuserID;
$position->activeuserID = $activeuserID;
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if($_POST['departmentID']==0){ 
			echo "<div class='alert alert-danger'>Please select a department preference.</div>";
		}
		
		else if($_POST['positionID']==0){ 
			echo "<div class='alert alert-danger'>Please select a teaching position preference.</div>";
		}

		
		else{
			// set preference property values
			$preference->departmentID = $_POST['departmentID'];
			$preference->positionID = $_POST['positionID'];

			// add the contact event
			if($preference->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "<span class='glyphicon glyphicon-thumbs-up'></span> Preference was added. <a href='view_member_info.php?memberID={$memberID}'>Return to Member Profile</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the contact event, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add preference.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
	 
<!-- HTML form for adding a contact event -->
<div class="row">
<form action='add_preference.php?<?php echo "memberID={$memberID}"; ?>' method='post'>
	<table class='table table-hover table-responsive table-bordered no-margin-bottom'>
		<tr>
			<td>Department</td>
			<td>
				<?php
                // read the departments from the database
                $stmt = $department->read();
                
                // put them in a select drop-down
                echo "<select class='form-control' name='departmentID'>";
                    echo "<option>Select department...</option>";
                    
                    while ($row_department = $stmt->fetch(PDO::FETCH_ASSOC)){
                        extract($row_department);
                        echo "<option value='{$departmentID}'>{$departmentdesc}</option>";
                    }
                    
                echo "</select>";
                ?>
			</td>
		</tr>
		 
		<tr>
			<td>Position</td>
			<td>
				<?php
                // read the staff positions from the database
                $stmt = $position->read();
                
                // put them in a select drop-down
                echo "<select class='form-control' name='positionID'>";
                    echo "<option>Select staff position...</option>";
                    
                    while ($row_position = $stmt->fetch(PDO::FETCH_ASSOC)){
                        extract($row_position);
                        echo "<option value='{$positionID}'>{$positionDesc}</option>";
                    }
                    
                echo "</select>";
                ?>
			</td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add Preference
				</button>
                 <a href='view_member_info.php?memberID=<?php echo "{$memberID}"; ?>' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>