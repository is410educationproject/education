<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
?>
<div class='container row margin-bottom-1em'>

	<!--<form role="search" action='search_members.php'>
		<div class="input-group col-md-3 pull-left margin-right-1em">
			<input type="text" class="form-control" placeholder="Type member name..." name="s" id="srch-term" required <?php echo isset($search_term) ? "value='$search_term'" : ""; ?> />
			<div class="input-group-btn">
				<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			</div>
		</div>
	</form>-->
	
	<!--<form role="search" action='../search_products_by_date_range.php'>
		<div class="input-group col-md-3 pull-left">
			<input type="text" class="form-control" placeholder="Date from..." name="date_from" id="date-from" required 
				<?php echo isset($date_from) ? "value='$date_from'" : ""; ?> />
			<span class="input-group-btn" style="width:0px;"></span>
			
			<input type="text" class="form-control" placeholder="Date to..." name="date_to" id="date-to" 
				required <?php echo isset($date_to) ? "value='$date_to'" : ""; ?> />
			<div class="input-group-btn">
				<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			</div>
		</div>
	</form>-->
	
	<!-- create member button -->
	<a href='add_contactevent.php' class="btn btn-primary pull-right margin-bottom-1em">
		<span class="glyphicon glyphicon-plus"></span> Add Member
	</a>
	
	<!-- export members to CSV -->
	<a href='includes/helpers/export_csv.php' class="btn btn-info pull-right margin-right-1em">
		<span class="glyphicon glyphicon-download-alt"></span> Export CSV
	</a>
	
	<!-- delete selected records -->
	<!--<button class="btn btn-danger pull-right margin-bottom-1em margin-right-1em" id="delete-selected">
		<span class="glyphicon glyphicon-remove-circle"></span> Delete Selected
	</button>-->
	
</div>
<?php 
// display the members if there are any
if($num>0){

	// order opposite of the current order
	$reverse_order=isset($order) && $order=="asc" ? "desc" : "asc";
	
	// field name
	$field=isset($field) ? $field : "";
	
	// field sorting arrow
	$field_sort_html="";
	
	if(isset($field_sort) && $field_sort==true){
		$field_sort_html.="<span class='badge'>";
			$field_sort_html.=$order=="asc" 
					? "<span class='glyphicon glyphicon-arrow-up'></span>"
					: "<span class='glyphicon glyphicon-arrow-down'></span>";
		$field_sort_html.="</span>";
	}
	
	echo "<table class='table table-hover table-responsive table-bordered'>";
		echo "<tr>";
			//echo "<th class='text-align-center'><input type='checkbox' id='checker' /></th>";
			echo "<th style='width:20%;'>";
				echo "<a href='read_products_sorted_by_fields.php?field=name&order={$reverse_order}'>";
					echo "Contact Date";
					echo $field=="fname" ? $field_sort_html : "";
				echo "</a>";
			echo "</th>";
            echo "<th style='width:30%;'>";
				echo "<a href='read_products_sorted_by_fields.php?field=description&order={$reverse_order}'>";
					echo "Method";
					echo $field=="phone" ? $field_sort_html : "";
				echo "</a>";
			echo "</th>";
			echo "<th style='width:30%;'>";
				echo "<a href='read_products_sorted_by_fields.php?field=description&order={$reverse_order}'>";
					echo "Status";
					echo $field=="phone" ? $field_sort_html : "";
				echo "</a>";
			echo "</th>";
			/*echo "<th>";
				echo "<a href='read_products_sorted_by_fields.php?field=created&order={$reverse_order}'>";
					echo "Created ";
					echo $field=="created" ? $field_sort_html : "";
				echo "</a>";
			echo "</th>";*/
			echo "<th>Actions</th>";
		echo "</tr>";
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		
			extract($row);
			
			echo "<tr>";
				//echo "<td class='text-align-center'><input type='checkbox' name='item[]' class='checkboxes' value='{$memberID}' /></td>";
				echo "<td>{$contactdate}</td>";
				//echo "<td>&#36;" . number_format($price, 2) . "</td>";
				echo "<td>{$cm_methodDesc}</td>";
				echo "<td>{$st_statusDesc}</td>";
				//echo "<td>{$created}</td>";
				echo "<td>";
					
					// edit member button
					echo "<a href='update_member.php?memberID={$memberID}' class='btn btn-sm btn-info right-margin'>";
						echo "<span class='glyphicon glyphicon-edit'></span>";
					echo "</a>";
					
					// contact events button
					echo "<a href='view_contactevents.php?memberID={$memberID}' class='btn btn-sm btn-success right-margin'>";
						echo "<span class='glyphicon glyphicon-earphone'></span>";
					echo "</a>";
					
					// delete member button
					echo "<a delete-id='{$memberID}' delete-file='includes/members_delete.inc.php' class='btn btn-sm btn-danger delete-object'>";
						echo "<span class='glyphicon glyphicon-remove'></span>";
					echo "</a>";
					
				echo "</td>";
				
			echo "</tr>";
			
		}
		
	echo "</table>";
	
	// needed for paging
/*	$total_rows=0;
	
	if($page_url=="view_contactevents.php?"){
		$total_rows=$contact->countAll();
	}
	
	else if(isset($category_id) && $page_url=="category.php?id={$category_id}&"){
		$total_rows=$contact->countAll_ByCategory();
	}
	
	else if(isset($search_term) && $page_url=="search_members.php?s={$search_term}&"){
		$total_rows=$contact->countAll_BySearch($search_term);
	}
	
	else if(isset($field) && isset($order) && $page_url=="read_products_sorted_by_fields.php?field={$field}&order={$order}&"){
		$total_rows=$contact->countAll();
	}*/
	
	// search by date range
	/*else if(isset($date_from) && isset($date_to) 
				&& $page_url=="search_products_by_date_range.php?date_from={$date_from}&date_to={$date_to}&"){
		$total_rows=$contact->countSearchByDateRange($date_from, $date_to);	
	}
	*/
	// paging buttons
	//include_once '../phpoopcrud/includes/helpers/paging.php';
}

// tell the user there are no members
else{
	echo "<div class=\"alert alert-danger alert-dismissable\">";
		echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		echo "No contact events found.";
	echo "</div>";
}
?>