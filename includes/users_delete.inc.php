<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/user.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare user.php object
	$userobject = new UserObject($db);
	
	// set userID to be deleted
	$userobject->userID = $_POST['object_id'];
	
	// delete the user
	if($userobject->delete()){
		echo "The user was deleted.";
	}
	
	// if unable to delete the user
	else{
		echo "Unable to delete the user.";	
	}
}
?>