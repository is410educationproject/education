<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/staffassign.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare contactevent.php object
	$assignment = new StaffAssignment($db);
	
	// set contacteventID to be deleted
	$assignment->assignmentID = $_POST['object_id'];
	
	// delete the contact event
	if($assignment->delete()){
		echo "The staff assignment was deleted.";
	}
	
	// if unable to delete the family position
	else{
		echo "Unable to delete the staff assignment.";	
	}
}
?>