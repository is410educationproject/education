<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}

// view statuses button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_statuses.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View statuses";
//	echo "</a>";
//echo "</div>";
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['positionDesc'])){ 
			echo "<div class='alert alert-danger'>Please enter a staff position description.</div>";
		}
		
		else{
			// set status property values
			$position->positionDesc = $_POST['positionDesc'];
			$position->activeuserID = $activeuserID;
			
			// add the staff position
			if($position->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The staff position was added. <a href='view_staffpositions.php'>Return to View Staff Positions</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the staff position, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add the staff position.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
<div class="row no-margin-bottom">
<!-- HTML form for adding a staff position -->
<form action='add_staffposition.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Description</td>
			<td><input type='text' name='positionDesc' class='form-control' required value="<?php echo isset($_POST['positionDesc']) ? htmlspecialchars($_POST['positionDesc'], 
			ENT_QUOTES) : "";  ?>"></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add staff position
				</button>
                 <a href='view_staffpositions.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>