<?php
// check for term
//term_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view terms button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_terms.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View terms";
//	echo "</a>";
//echo "</div>";
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['termDesc'])){ 
			echo "<div class='alert alert-danger'>Please enter a term description.</div>";
		}
		
		else{
			// set term property values
			$term->termDesc = $_POST['termDesc'];
			
			// add the term
			if($term->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The term was added. <a href='view_terms.php'>Return to View Terms</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the term, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add the term.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
<div class="row no-margin-bottom">
<!-- HTML form for adding a term -->
<form action='add_term.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>term Description</td>
			<td><input type='text' name='termDesc' class='form-control' required value="<?php echo isset($_POST['termDesc']) ? htmlspecialchars($_POST['termDesc'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add term
				</button>
                 <a href='view_terms.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>