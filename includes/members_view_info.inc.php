<?php
// view all members button
echo "<div class='row overflow-hidden add-margin-top'>";
	echo "<a href='view_members.php' class='btn btn-primary pull-left hidden-sm hidden-md hidden-lg'>";
		echo "<span class='glyphicon glyphicon-chevron-left'></span> Go back";
	echo "</a>";
	echo "<a href='view_members.php' style='display:inline;' class='btn btn-primary pull-left hidden-xs'>";
		echo "<span class='glyphicon glyphicon-chevron-left'></span> View All Members";
	echo "</a>";


?>
<div class="row">
	<!-- create member button -->
	<div class='no-margin-bottom overflow-hidden'>
    <a href='add_contactevent.php?<?php echo "memberID={$memberID}"; ?>' class="btn btn-primary pull-right no-margin-bottom">
		<span class="glyphicon glyphicon-plus"></span> Add Contact Event
	</a>

	<!-- create preference button -->
	<a href='add_preference.php?<?php echo "memberID={$memberID}"; ?>' class="btn btn-primary pull-right margin-right-1em no-margin-bottom">
		<span class="glyphicon glyphicon-plus"></span> Add Preference
	</a>
</div>
</div>

<div class="row">
<?php
// get ID of the member to be edited
$memberID = isset($_GET['memberID']) ? $_GET['memberID'] : die('ERROR: missing ID.');

// set ID property of member to be edited
$member->memberID = $memberID;

// read the details of the member for the profile information
$member->readOne();

?>
	
<!-- HTML form for updating a member -->

	<div class = "col-lg-6 col-md-6 no-padding" >
    <table class='table table-hover table-responsive table-bordered'>
    
	 
		<tr>
			<td class="info-page-left-column">Full Name</td>
			<td class="info-page-right-column"><?php echo htmlspecialchars($member->fname, ENT_QUOTES, 'UTF-8'); ?> <?php echo htmlspecialchars($member->lname, ENT_QUOTES, 'UTF-8'); ?></td>
		</tr>
		 
		<tr>
			<td class="info-page-left-column">Primary Phone</td>
			<td class="info-page-right-column"><?php echo "<a href='tel:{$member->phone}'>" . htmlspecialchars($member->phone, ENT_QUOTES, 'UTF-8') . "</a>"; ?></td>
		</tr>
        
        <?php if($member->phcell != NULL){ ?>
        <tr>
			<td class="info-page-left-column">Cell Phone</td>
			<td class="info-page-right-column"><?php echo "<a href='tel:{$member->phcell}'>" . htmlspecialchars($member->phcell, ENT_QUOTES, 'UTF-8') . "</a>"; ?></td>
		</tr>
        <?php } ?>
        
        <?php if($member->phwork != NULL){ ?>
        <tr>
			<td class="info-page-left-column">Work Phone</td>
			<td class="info-page-right-column"><?php echo "<a href='tel:{$member->phwork}'>" . htmlspecialchars($member->phwork, ENT_QUOTES, 'UTF-8') . "</a>"; ?></td>
		</tr>
        <?php } ?>
        
        <?php if($member->email != NULL){ ?>
        <tr>
			<td class="info-page-left-column">Email</td>
			<td class="info-page-right-column"><?php echo "<a href='mailto:{$member->email}'>" . htmlspecialchars($member->email, ENT_QUOTES, 'UTF-8') . "</a>"; ?></td>
		</tr>
        <?php } ?>
        <tr>
			<td class="info-page-left-column">Family Position</td>
			<td class="info-page-right-column"><?php echo htmlspecialchars($member->f_familypositiondesc, ENT_QUOTES, 'UTF-8'); ?></td>
		</tr>
        
        <tr>
			<td class="info-page-left-column">Date of Birth</td>
			<td class="info-page-right-column"><?php echo date('F j, Y', strtotime($member->dob)); ?></td>
		</tr>
        
        <tr>
            <td class="info-page-left-column">Preferences</td>
            <td class="info-page-right-column">
            <?php
            	if($numPR > 0){

					while ($row = $stmtPR->fetch(PDO::FETCH_ASSOC)){
						extract($row);
						echo "{$d_departmentdesc} <span class='badge'>{$sp_positionDesc}</span>";
						// delete contact event button
						echo "<a delete-id='{$p_preferenceID}' delete-file='includes/preferences_delete.inc.php' class='btn btn-xs btn-danger  pull-right delete-object'>";
							echo "<span class= 'glyphicon glyphicon-trash'></span>";
						echo "</a>";
						
						if($numPR > 1){
							echo "<br><br>";
						}

					}
				}
				else{
					echo "None";
				}
                ?>
             </td>
		</tr>
        
		 
	</table></div>
    
    
<div class="col-lg-6 col-md-6 no-padding" >   
    <?php 
// display the member involvement and class involved in 
if($numSA>0){
	
	echo "<table class='table table-hover table-responsive table-bordered'>";
		echo "<tr>";
			//echo "<th class='text-align-center'><input type='checkbox' id='checker' /></th>";
			echo "<th style='width:40%;'>";
				echo "Class / Grade Level";
			echo "</th>";
			echo "<th style='width:40%;'>";
				echo "Time";
			echo "</th>";
			echo "<th style='width:20%;'>";
				echo "Position";
			echo "</th>";
		echo "</tr>";
		
		while ($row = $stmtSA->fetch(PDO::FETCH_ASSOC)){
		
			extract($row);
			
			echo "<tr>";
				echo "<td>{$c_classname} <span class='badge'>{$c_gradelevel}</span></td>";
				echo "<td class='hidden-xs'>{$t_timeDesc}</td>";
				echo "<td class='hidden-xs'>{$sp_positionDesc}</td>";
			echo "</tr>";
		}
	echo "</table>";
}

// tell the user there is no teacher involvement.
else{
	echo "<div class=\"alert alert-danger\">";
		//echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		echo "<span class='glyphicon glyphicon-exclamation-sign'></span> &nbsp; No staff assignments found for this term.";
	echo "</div>";
}
?> </div>
</div>

<div class="row no-margin-bottom">

	<!--<form role="search" action='search_members.php'>
		<div class="input-group col-md-3 pull-left margin-right-1em">
			<input type="text" class="form-control" placeholder="Type member name..." name="s" id="srch-term" required <?php echo isset($search_term) ? "value='$search_term'" : ""; ?> />
			<div class="input-group-btn">
				<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			</div>
		</div>
	</form>-->
	
	<!--<form role="search" action='../search_products_by_date_range.php'>
		<div class="input-group col-md-3 pull-left">
			<input type="text" class="form-control" placeholder="Date from..." name="date_from" id="date-from" required 
				<?php echo isset($date_from) ? "value='$date_from'" : ""; ?> />
			<span class="input-group-btn" style="width:0px;"></span>
			
			<input type="text" class="form-control" placeholder="Date to..." name="date_to" id="date-to" 
				required <?php echo isset($date_to) ? "value='$date_to'" : ""; ?> />
			<div class="input-group-btn">
				<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			</div>
		</div>
	</form>-->
<?php 
// display the members if there are any
if($numCE>0){
	
	echo "<table class='table table-hover table-responsive table-bordered no-margin-bottom'>";
		echo "<tr>";
			//echo "<th class='text-align-center'><input type='checkbox' id='checker' /></th>";
			echo "<th style='width:30%;'>";
					echo "Contact Date";
			echo "</th>";
            echo "<th class='hidden-xs' style='width:20%;'>";
					echo "Method";
			echo "</th>";
			echo "<th class='hidden-xs' style='width:30%;'>";
					echo "Status";
			echo "</th>";
			echo "<th style='width:30%;'>Actions</th>";
		echo "</tr>";
		
		while ($row = $stmtCE->fetch(PDO::FETCH_ASSOC)){
		
			extract($row);
			
			echo "<tr>";
				//echo "<td class='text-align-center'><input type='checkbox' name='item[]' class='checkboxes' value='{$memberID}' /></td>";
				echo "<td>". date('F j, Y', strtotime($contactdate))."</td>"; 
				//echo "<td>&#36;" . number_format($price, 2) . "</td>";
				echo "<td class='hidden-xs'>{$cm_methodDesc}</td>";
				echo "<td class='hidden-xs'>{$st_statusDesc}</td>";
				//echo "<td>{$created}</td>";
				echo "<td>";
					
					// edit contact event button
					echo "<a href='update_contactevent.php?memberID={$memberID}&contacteventID={$contacteventID}' class='btn btn-sm btn-info right-margin'>";
						echo "<span class='glyphicon glyphicon-pencil'></span>";
					echo "</a>";
					
					// view contact events button
					echo "<a data-toggle='modal' data-target='#{$contacteventID}' class='btn btn-sm btn-success right-margin'>";
						echo "<span class='glyphicon glyphicon-eye-open'></span>";
					echo "</a>";
					?>
					<!-- View Member Modal -->
                    <div class="modal fade" 
                        id="<?php echo "{$contacteventID}"; ?>" 
                        tabindex="-1" 
                        role="dialog" 
                        aria-labelledby="viewMemberModalLabel<?php echo "{$contacteventID}"; ?>" 
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title" id="viewMemberModalLabel<?php echo "{$contacteventID}"; ?>">
                                        View Contact Event for <h3><?php echo "{$m_fname} {$m_lname}"; ?></h3>
                                    </h4>
                                </div>                  
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="contactdate">Contact Date</label>
                                            <br />
                                            <?php echo date('F j, Y', strtotime($contactdate));?>
                                            <br />
                                            <br />
                                            <label for="methodID">Method of Contact</label>
                                            <br />
                                            <?php echo "{$cm_methodDesc}"; ?>
                                            <br />
                                            <br />
                                            <label for="notes">Notes</label>
                                            <br />
                                            <?php if($notes==null) {
												echo "No notes found.";
											}
											else
												{
												echo "{$notes}";} ?>
                                            <br />
                                            <br />
                                            <label for="statusID">Status</label>
                                            <br />
                                            <?php echo "{$st_statusDesc}"; ?>

                                            <?php //echo $member['m_lname']; ?>
                                        </div>
									</form>              
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
					// delete contact event button
					echo "<a delete-id='{$contacteventID}' delete-file='includes/contactevents_delete.inc.php' class='btn btn-sm btn-danger delete-object'>";
						echo "<span class='glyphicon glyphicon-trash'></span>";
					echo "</a>";
				echo "</td>";
			echo "</tr>";
		}
	echo "</table>";

}
// tell the user there are no contact events.
else{
	echo "<div class=\"alert alert-danger\">";
		//echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		echo "<span class='glyphicon glyphicon-exclamation-sign'></span> &nbsp; No contact events found for this member.";
	echo "</div>";
}
echo "</div>";
?>
</div>