<?php
// check for session
//session_start();

include_once 'login/class.user.php';
$user = new User($db);
if (!$user->logged_in()){
	header("location:../login.php");
}

// set ID property of user to be edited
$userobject->activeuserID = $_SESSION['activeuserID'];
$activetermID = $userobject->readActiveTerm();

// if the form was submitted
if($_POST && $termselector==true){
	
	try{
		$userobject->termID = $_POST['termID'];
		
		// update the active term
		if($userobject->updateTermID()){
			$userobject->readActiveTerm();
			$termchanged = true;
		}
		
		// if unable to update the active session, tell the user
		else{
			$termchanged = false;
		}
		
		// values to fill up our form
		$termID = $_POST['termID'];

	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
	$userobject->readActiveTerm();
}
else{
	// read the active term
	$userobject->readActiveTerm();
}

?>