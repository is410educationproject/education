<?php
// view all departments button
echo "<div class='row margin-bottom-1em overflow-hidden add-margin-top'>";
	echo "<a href='view_departments.php' class='btn btn-primary pull-left hidden-md hidden-lg'>";
		echo "<span class='glyphicon glyphicon-chevron-left'></span> Go back";
	echo "</a>";
	echo "<a href='view_departments.php' style='display:inline;' class='btn btn-primary pull-left hidden-xs hidden-sm'>";
		echo "<span class='glyphicon glyphicon-chevron-left'></span> View All Departments";
	echo "</a>";


?>

	<!-- create member button -->
	<div class='no-margin-bottom overflow-hidden'>
	<a href='add_class.php?departmentID=<?php echo "{$departmentID}"; ?>' class="btn btn-primary pull-right no-margin-bottom">
		<span class="glyphicon glyphicon-plus"></span> Add Class
	</a>
	
	<!-- export members to CSV -->
	<a href='helpers/export_csv.php' class="btn btn-info pull-right margin-right-1em hidden-xs hidden-sm hidden-md">
		<span class="glyphicon glyphicon-download-alt"></span> Export CSV
	</a>
</div>
</div>
<?php

// set ID property of member to be edited
$department->departmentID = $departmentID;

// read the details of the member for the profile information
$department->readOne();

?>
	    
<div class="row no-margin-bottom">
<div class="no-padding-left col-lg-6 col-md-6">   
    <?php 
// display the member involvement and class involved in 
if($numPO>0){

	
	echo "<table class='table table-hover table-responsive table-bordered'>";
		echo "<tr>";
			//echo "<th class='text-align-center'><input type='checkbox' id='checker' /></th>";
			echo "<th style='width:30%;'>";
					echo "Staff Position";
			echo "</th>";

			echo "<th style='width:30%;'>Number of Staff</th>";
			
			echo "<th style='width:30%;'>View</th>";
		echo "</tr>";
		
		while ($row = $stmtPO->fetch(PDO::FETCH_ASSOC)){
		
			extract($row);
			
			echo "<tr>";
				//echo "<td class='text-align-center'><input type='checkbox' name='item[]' class='checkboxes' value='{$memberID}' /></td>";
				echo "<td>{$positionDesc}</td>";
				echo "<td>{$saCount}</td>";
				echo "<td>";
					
					echo "<a href='view_staffassignments.php?route=1&positionID={$positionID}&departmentID={$departmentID}' class='btn btn-sm btn-success right-margin'>";
						echo "<span class='glyphicon glyphicon-eye-open'></span>";
					echo "</a>";

				echo "</td>";
			echo "</tr>";
		}
	echo "</table>";
}

// tell the user there is no teacher involvement.
else{
	echo "<div class=\"alert alert-danger\">";
		echo "<span class='glyphicon glyphicon-exclamation-sign'></span> &nbsp; No staff positions found in {$userobject->term_termDesc}.";
	echo "</div>";
}
?> </div>
    
<div class="no-padding-right col-lg-6 col-md-6">   

<?php 
// display the classes if there are any
if($num>0){

	
	echo "<table class='table table-hover table-responsive table-bordered'>";
		echo "<tr>";
			echo "<th style='width:40%;'>";
					echo "Class/Grade Level";
			echo "</th>";
			echo "<th>";
					echo "Time";
			echo "</th>";
			echo "<th style='width:25%;'>Actions</th>";
		echo "</tr>";
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		
			extract($row);
			
			echo "<tr>";
				echo "<td>{$classname} <span class='badge'>{$gl_gradelevelDesc}</span></td>";
				echo "<td>{$t_timeDesc}</td>";
				echo "<td>";
					
					// edit class button
					echo "<a href='update_class.php?classID={$classID}&departmentID={$departmentID}' class='btn btn-sm btn-info right-margin'>";
						echo "<span class='glyphicon glyphicon-pencil'></span>";
					echo "</a>";
					
					// view class info button
					echo "<a href='view_class_info.php?route=2&classID={$classID}&departmentID={$departmentID}' class='btn btn-sm btn-success right-margin'>";
						echo "<span class='glyphicon glyphicon-eye-open'></span>";
					echo "</a>";
					
					// delete class button
					echo "<a delete-id='{$classID}' delete-file='includes/classes_delete.inc.php' class='btn btn-sm btn-danger delete-object'>";
						echo "<span class='glyphicon glyphicon-trash'></span>";
					echo "</a>";
					
				echo "</td>";
				
			echo "</tr>";
			
		}
		
	echo "</table>";
	
}

// tell the user there are no classes
else{
	echo "<div class=\"alert alert-danger alert-dismissable\">";
		echo "No classes found in the <strong>{$department->departmentdesc}</strong> department for <strong>{$userobject->term_termDesc}</strong>.";
	echo "</div>";
}
?></div>