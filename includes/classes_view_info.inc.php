<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view department profile button
echo "<div class='row no-margin-bottom'>";
	echo "<a href='view_department_info.php?departmentID={$departmentID}' class='btn btn-primary pull-left hidden-md hidden-lg'>";
		echo "<span class='glyphicon glyphicon-chevron-left'></span> Go back";
	echo "</a>";
	echo "<a href='view_department_info.php?departmentID={$departmentID}' class='btn btn-primary pull-left hidden-xs hidden-sm'>";
		echo "<span class='glyphicon glyphicon-chevron-left'></span> View All Classes";
	echo "</a>";


?>

<!-- assign staff button -->
	<a href='add_staffassignment.php?<?php echo "classID={$classID}&departmentID={$departmentID}"; ?>' class="btn btn-primary pull-right margin-bottom-1em">
		<span class="glyphicon glyphicon-plus"></span> Assign a Staff Member
	</a>
    </div>
<?php    

// set ID property of member to be edited
$class->classID = $classID;

// read the details of the member for the profile information
$class->readOne();

?>
	
<!-- HTML form for updating a member -->

	<table class='table table-hover table-responsive table-bordered no-margin-bottom'>
	 
		<tr>
			<td>Class Name</td>
			<td><?php echo htmlspecialchars($class->classname, ENT_QUOTES, 'UTF-8'); ?></td>
		</tr>
		 
		<tr>
			<td>Grade level</td>
			<td><?php echo htmlspecialchars($class->gl_gradelevelDesc, ENT_QUOTES, 'UTF-8'); ?></td>
		</tr>
        
        <tr>
			<td>Room Number</td>
			<td><?php echo htmlspecialchars($class->room, ENT_QUOTES, 'UTF-8'); ?></td>
		</tr>
        
        <tr>
			<td>Class time</td>
			<td><?php echo htmlspecialchars($class->t_timeDesc, ENT_QUOTES, 'UTF-8'); ?></td>
		</tr>
        
        <tr>
			<td>Term</td>
			<td><?php echo htmlspecialchars($class->term_termDesc, ENT_QUOTES, 'UTF-8'); ?></td>
		</tr>
        	 
	</table>

<div class='row'>

	<!--<form role="search" action='search_members.php'>
		<div class="input-group col-md-3 pull-left margin-right-1em">
			<input type="text" class="form-control" placeholder="Type member name..." name="s" id="srch-term" required <?php //echo isset($search_term) ? "value='$search_term'" : ""; ?> />
			<div class="input-group-btn">
				<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			</div>
		</div>
	</form>-->
	
	<!--<form role="search" action='../search_products_by_date_range.php'>
		<div class="input-group col-md-3 pull-left">
			<input type="text" class="form-control" placeholder="Date from..." name="date_from" id="date-from" required 
				<?php //echo isset($date_from) ? "value='$date_from'" : ""; ?> />
			<span class="input-group-btn" style="width:0px;"></span>
			
			<input type="text" class="form-control" placeholder="Date to..." name="date_to" id="date-to" 
				required <?php // echo isset($date_to) ? "value='$date_to'" : ""; ?> />
			<div class="input-group-btn">
				<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			</div>
		</div>
	</form>-->
</div>
<div class="row no-margin-bottom">
<?php 
// display the members if there are any
if($num>0){
	
	echo "<table class='table table-hover table-responsive table-bordered'>";
		echo "<tr>";
			//echo "<th class='text-align-center'><input type='checkbox' id='checker' /></th>";
			echo "<th style='width:30%;'>";
					echo "Member Name";
			echo "</th>";
            echo "<th class='hidden-xs' style='width:20%;'>";
					echo "Staff Position";
			echo "</th>";
			echo "<th style='width:30%;'>Actions</th>";
		echo "</tr>";
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		
			extract($row);
			
			echo "<tr>";
				//echo "<td class='text-align-center'><input type='checkbox' name='item[]' class='checkboxes' value='{$memberID}' /></td>";
				echo "<td>{$m_fname} {$m_lname}</td>";
				//echo "<td>&#36;" . number_format($price, 2) . "</td>";
				echo "<td class='hidden-xs'>{$sp_positionDesc}</td>";
				//echo "<td class='hidden-xs'>{$st_statusDesc}</td>";
				//echo "<td>{$created}</td>";
				echo "<td>";
					
					// edit contact event button
					echo "<a href='update_staffassignment.php?classID={$classID}&departmentID={$departmentID}&assignmentID={$assignmentID}&route=2' class='btn btn-sm btn-info 
					right-margin'>";
						echo "<span class='glyphicon glyphicon-pencil'></span>";
					echo "</a>";
					
					// view contact events button
					echo "<a data-toggle='modal' data-target='#{$assignmentID}' class='btn btn-sm btn-success'>";
						echo "<span class='glyphicon glyphicon-eye-open'></span>";
					echo "</a>";
					?>
					<!-- View Member Modal -->
                    <div class="modal fade" 
                        id="<?php echo "{$assignmentID}"; ?>" 
                        tabindex="-1" 
                        role="dialog" 
                        aria-labelledby="viewStaffModalLabel<?php echo "{$assignmentID}"; ?>" 
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title" id="viewStaffModalLabel<?php echo "{$assignmentID}"; ?>">
                                        View Assigned Staff for <h3><?php echo "{$c_classname}"; ?></h3>
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="Name">Full Name</label>
                                            <br />
                                            <?php echo "{$m_fname} {$m_lname}"; ?>
                                            <br />
                                            <br />
                                            <label for="Position">Staff Position</label>
                                            <br />
                                            <?php echo "{$sp_positionDesc}"; ?>
                                            <br />
                                            <br />
                                            <?php if($m_phone != null) { ?>
                                            <label for="Phone">Primary Phone</label>
                                            <br />
                                            <?php echo "{$m_phone}"; ?>
                                            <br />
                                            <br />
                                            <?php } ?>
                                            <?php if($m_phcell != null) { ?>
                                            <label for="Cell Phone">Cell Phone</label>
                                            <br />
                                            <?php echo "{$m_phcell}"; ?>
                                            <br />
                                            <br />
                                            <?php } ?>
                                            <?php if($m_phwork != null) { ?>
                                            <label for="Work Phone">Work Phone</label>
                                            <br />
                                            <?php echo "{$m_phwork}"; ?>
                                            <br />
                                            <br />
                                            <?php } ?>
                                            <?php if($m_email != null) { ?>
                                            <label for="Email">Email</label>
                                            <br />
                                            <?php echo "{$m_email}"; ?>
                                            <br />
                                            <br />
                                            <?php } ?>

                                        </div>
									</form>              
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
					// delete contact event button
					echo "<a delete-id='{$assignmentID}' delete-file='includes/staffassignment_delete.inc.php' class='btn btn-sm btn-danger delete-object'>";
						echo "<span class='glyphicon glyphicon-trash'></span>";
					echo "</a>";
				echo "</td>";
			echo "</tr>";
		}
	echo "</table>";
}

// tell the user there are no contact events.
else{
	echo "<div class=\"alert alert-danger\">";
		//echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		echo "<span class='glyphicon glyphicon-exclamation-sign'></span> &nbsp; No members are currently assigned to this class.";
	echo "</div>";
}
?>
</div>