<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/status.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare status.php object
	$status = new Status($db);
	
	// set statusID to be deleted
	$status->statusID = $_POST['object_id'];
	
	// delete the status
	if($status->delete()){
		echo "The status was deleted.";
	}
	
	// if unable to delete the status
	else{
		echo "Unable to delete the status.";	
	}
}
?>