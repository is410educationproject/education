    </div>
    <!-- /container -->
     
<!-- jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-2.0.3.min.js"></script>

<!-- bootstrap JavaScript -->
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-multiselect.js"></script>
<script src="js/holder.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- jquery ui -->
<script src="js/jquery-ui.min.js"></script>
<script src="js/bootstrap-switch.js"></script>

<script src="js/bootstrap-typeahead.min.js"></script>

<script>
$(function() {
    $('#slider').bootstrapToggle();
  });
  

$(function() {
                function displayResult(item) {
                    $('.alert').show().html('You selected <strong>' + item.value + '</strong>: <strong>' + item.text + '</strong>');
                }
                $('#demo1').typeahead({
					ajax: {
						method: 'post',
                        url: 'helpers/member_staffassign.php',
						displayField: "fname"
						
						
                    },
					displayField: "fname",
                    onSelect: displayResult
                });
				/*// Mock an AJAX request
                $.mockjax({
                    url: 'helpers/member_staffassign.php',
                    response: function() {
                        this.responseText = [
                            {id: 1, name: 'Toronto'},
                            {id: 2, name: 'Montreal'},
                            {id: 3, name: 'New York'},
                            {id: 4, name: 'Buffalo'},
                            {id: 5, name: 'Boston'},
                            {id: 6, name: 'Columbus'},
                            {id: 7, name: 'Dallas'},
                            {id: 8, name: 'Vancouver'},
                            {id: 9, name: 'Seattle'},
                            {id: 10, name: 'Los Angeles'}
                        ];
                    }
                });*/
				});



  
  $(document).ready(function(){
	// jquery ui date picker
	$( "#date-from" ).datepicker({ dateFormat: 'yy-mm-dd' });
	$( "#date-to" ).datepicker({ dateFormat: 'yy-mm-dd' });
	
	//check/uncheck script
	$(document).on('click', '#checker', function(){
		$('.checkboxes').prop('checked', $(this).is(':checked'));
	});
		
	
	// delete selected records	
	$(document).on('click', '#delete-selected', function(){

		var at_least_one_was_checked = $('.checkboxes:checked').length > 0;
			
		if(at_least_one_was_checked){
		
			var answer = confirm('Are you sure?');
			
			if (answer){
					
					// get converts it to an array
					var del_checkboxes = $('.checkboxes:checked').map(function(i,n) {
						return $(n).val();
					}).get();

					if(del_checkboxes.length==0) {
						del_checkboxes = "none"; 
					}  
					
					$.post("delete_selected.php", {'del_checkboxes[]': del_checkboxes}, 
						function(response) {
						
							if(response==""){
								// refresh
								location.reload();
							}else{
								// tell the user there's a problem
								alert(response);
							}
						
					});

			}
		}
		
		else{
			alert('Please select at least one record to delete.');
		}
	});
		
	// delete record
	$(document).on('click', '.delete-object', function(){
	 
		// php file used for deletion
		var delete_file = $(this).attr('delete-file');
		
		var id = $(this).attr('delete-id');
		var q = confirm("Are you sure?");
	 
		if (q == true){
	 
			$.post(delete_file, {
				object_id: id
			}, function(data){
				location.reload();
			}).fail(function() {
				alert('Unable to delete.');
			});
	 
		}
		return false;
	});
});

var selectedoption = "none";
						$(document).ready(function() { //when the page is ready
							$('#filter-select').multiselect({ //do the things here for the filter-select ID
								onChange: function(option, checked, select) {
									selectedoption = $(option).val();
									console.log(selectedoption);
									//console.log(test);
									var values = [];
                    				$('#filter-select option').each(function() {
                        				if ($(this).val() !== option.val()) {
                            				values.push($(this).val());
                        				}
                    				});
 
                    				$('#filter-select').multiselect('deselect', values);
									
								},
								onDropdownHide: function(event) {
									console.log('Dropdown closed.');
									urls = "view_members.php?filter="+selectedoption;
									$.ajax({
									   type: "POST",
									   url: "view_members.php?filter="+selectedoption,
									   data: $('#filter-select').serialize(),
									   success: function(msg){
										   console.log("ajax works");
										   //document.location.href = 'view_members.php?filter=selectedoption';
										   $(window.location).attr('href', urls);
										},
									   error: function(){
										 alert("error");
									   } 
									 });//close ajax
								}
							});
							
							
						});
						
						//var options = validator.getFieldElements('#filter-select').val();
									 

$(document).ready(function() { //when the page is ready
	$("[name='isTeacher']").on('switchChange.bootstrapSwitch', function(event, state) {
	  console.log(this); // DOM element
	  console.log(event); // jQuery event
	  console.log(state); // true | false	
	});
	
/*$("[name='isSub']").bootstrapSwitch('switchChange', function(event, state){
	console.log(this);
	console.log(event);
	console.log(state);
	});*/
});
//$("[name='isHelper']").bootstrapSwitch();

/*jQuery(function($) {
      $.mask.definitions['~']='[+-]';
      $('#PrimaryPhone').mask('(999) 999-9999');
	});*/

/*$('form').on('submit', function(){
    $(this).find('input[type="tel"]').each(function(){
        $(this).val() = $(this).val().replace(/[\s().+-]/g, '');
    });
    $(this).find('input[name="ssn"]').each(function(){
        $(this).val() = $(this).val().replace(/-/g, '');
    });
});*/
</script>

</body>
</html>