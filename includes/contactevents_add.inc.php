<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view members button
//echo "<div class='overflow-hidden'>";
//	echo "<a href='view_member_info.php?memberID={$memberID}' class='btn btn-primary pull-left'>";
//		echo "<span class='glyphicon glyphicon-list'></span> Back to Member Profile";
//	echo "</a>";
//echo "</div>";
	
// get ID of the member to be edited
$memberID = isset($_GET['memberID']) ? $_GET['memberID'] : die('ERROR: missing ID.');

// set ID property of member to be edited
$contact->memberID = $memberID;
echo "<div class='row no-margin-bottom'>";

// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['contactdate'])){ 
			echo "<div class='alert alert-danger'>Please select a date of contact.</div>";
		}
		
		else if($_POST['methodID']==0){ 
			echo "<div class='alert alert-danger'>Please select a method of contact from the dropdown.</div>";
		}
		
		/*else if(empty($_POST['notes'])){
			echo "<div class='alert alert-danger'>Price cannot be empty.</div>";
		}*/
		
		/*else if(!is_numeric($_POST['price'])){
			echo "<div class='alert alert-danger'>Price must be a number.</div>";
		}*/
		
		else if($_POST['statusID']==0){
			echo "<div class='alert alert-danger'>Please select a status from the dropdown.</div>";
		}
		
		else{
			// set contact event property values
			$contact->memberID = $_POST['memberID'];
			$contact->contactdate = $_POST['contactdate'];
			$contact->methodID = $_POST['methodID'];
			$contact->notes = $_POST['notes'];
			$contact->statusID = $_POST['statusID'];
			
			// add the contact event
			if($contact->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "<span class='glyphicon glyphicon-thumbs-up'></span>   Contact event was added. <a href='view_member_info.php?memberID={$memberID}'>Return to Member Profile</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the contact event, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add contact event.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
	 
<!-- HTML form for adding a contact event -->
<form action='add_contactevent.php?<?php echo "memberID={$memberID}"; ?>' method='post'>
	<table class='table table-hover table-responsive table-bordered no-margin-bottom'>
		<input type='hidden' name='memberID' class='form-control' value="<?php echo "$contact->memberID"; ?>" />
        
        
        <tr>
			<td>Contact Date</td>
			<td><input type='date' name='contactdate' class='form-control' placeholder='<?php echo date('Y-m-d'); ?>' required value="<?php echo isset($_POST['contactdate']) ? htmlspecialchars($_POST['contactdate'], ENT_QUOTES) : date('Y-m-d');  ?>"></td>
		</tr>
		 
		<tr>
			<td>Method</td>
			<td>
				<?php
                // read the family positions from the database
                include_once 'objects/contactmethod.php';
    
                $contactmethod = new ContactMethod($db);
                $stmt = $contactmethod->read();
                
                // put them in a select drop-down
                echo "<select class='form-control' name='methodID'>";
                    echo "<option>Select contact method...</option>";
                    
                    while ($row_contactmethod = $stmt->fetch(PDO::FETCH_ASSOC)){
                        extract($row_contactmethod);
                        echo "<option value='{$methodID}'>{$methodDesc}</option>";
                    }
                    
                echo "</select>";
                ?>
			</td>
		</tr>
		 
		<tr>
			<td>Notes</td>
			<td><textarea name='notes' class='form-control'><?php echo isset($_POST['notes']) ? htmlspecialchars($_POST['notes'], ENT_QUOTES) : "";  ?></textarea></td>
		</tr>
		 
		<tr>
			<td>Status</td>
			<td>
				<?php
                // read the status descriptions from the database
                include_once 'objects/status.php';
    
                $status = new Status($db);
                $stmt = $status->read();
                
                // put them in a select drop-down
                echo "<select class='form-control' name='statusID'>";
                    echo "<option>Select status...</option>";
                    
                    while ($row_status = $stmt->fetch(PDO::FETCH_ASSOC)){
                        extract($row_status);
                        echo "<option value='{$statusID}'>{$statusDesc}</option>";
                    }
                    
                echo "</select>";
                ?>
			</td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add Contact Event
				</button>
                 <a href='view_member_info.php?memberID=<?php echo "{$memberID}"; ?>' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>