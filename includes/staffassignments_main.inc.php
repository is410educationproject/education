<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
?>
<div class='row'>

	<a href='view_department_info.php?departmentID=<?php echo "{$departmentID}"; ?>' class="btn btn-primary pull-left add-margin-right hidden-xs hidden-sm">
		<span class="glyphicon glyphicon-chevron-left"></span> View Department Profile
	</a>
    <a href='view_department_info.php?departmentID=<?php echo "{$departmentID}"; ?>' class="btn btn-primary pull-left add-margin-right hidden-md hidden-lg">
		<span class="glyphicon glyphicon-chevron-left"></span> Go back
	</a>

<?php
if($num>0){ ?>
	<form role="search" action="search_staffassignments.php?departmentID=<?php echo '{$departmentID}&positionID={$positionID}' ; ?>">
		<div class="input-group col-xs-7 col-sm-4 col-md-3 col-lg-3 pull-left">
			<input type="text" class="form-control" placeholder="Type member name..." name="s" id="srch-term" required <?php echo isset($search_term) ? "value='{$search_term}'"
			: ""; ?> />
            <input type="hidden" class="form-control" name="departmentID" id="positionID" required <?php echo isset($departmentID) ? "value='{$departmentID}'" : ""; ?> />
            <input type="hidden" class="form-control" name="positionID" id="positionID" required <?php echo isset($positionID) ? "value='{$positionID}'" : ""; ?> />
            <input type="hidden" class="form-control" name="route" id="route" required <?php echo "value='1'"; ?> />
            <input type="hidden" class="form-control" name="search" id="search" required <?php echo "value='true'"; ?> />
			<div class="input-group-btn">
				<button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-search"></i></button>
			</div>
		</div>
	</form>
	
	<!-- export members to CSV -->
	<a href='helpers/export_csv.php' class="btn btn-info pull-right hidden-xs hidden-sm">
		<span class="glyphicon glyphicon-download-alt"></span> Export CSV
	</a>
	
<?php } ?>
</div>
<?php 
echo "<div class='row no-margin-bottom'>";
// display the members if there are any
if($num>0){

	// order opposite of the current order
	$reverse_order=isset($order) && $order=="asc" ? "desc" : "asc";
	
	// field name
	$field=isset($field) ? $field : "";
	
	// field sorting arrow
	$field_sort_html="";
	
	if(isset($field_sort) && $field_sort==true){
		$field_sort_html.="<span class='badge'>";
			$field_sort_html.=$order=="asc" 
					? "<span class='glyphicon glyphicon-arrow-up'></span>"
					: "<span class='glyphicon glyphicon-arrow-down'></span>";
		$field_sort_html.="</span>";
	}
	
	echo "<table class='table table-hover table-responsive table-bordered'>";
		echo "<tr>";
			echo "<th style='width:20%;'>";
					echo "Full Name";
			echo "</th>";
            echo "<th style='width:30%;'>";
					echo "Class/Grade Level";
			echo "</th>";
			echo "<th>";
					echo "Time";
				echo "</a>";
			echo "</th>";
			echo "<th>Actions</th>";
		echo "</tr>";
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		
			extract($row);
			
			echo "<tr>";
				echo "<td>{$m_lname}, {$m_fname}</td>";
				echo "<td>{$c_classname}</td>";
				echo "<td class='hidden-xs'>{$t_timeDesc}</td>";
				echo "<td>";
					
					// edit staff assignment button
					echo "<a href='update_staffassignment.php?assignmentID={$assignmentID}&positionID={$positionID}&departmentID={$departmentID}";
					if(isset($search) && $search=="true"){
						echo "&search={$search}&s={$search_term}";
					}
						echo "&route=1";
						echo "' class='btn btn-sm btn-info right-margin'>";
						echo "<span class='glyphicon glyphicon-pencil'></span>";
					echo "</a>";
					
					// view staff assignment button
					echo "<a data-toggle='modal' data-target='#{$assignmentID}' class='btn btn-sm btn-success'>";
						echo "<span class='glyphicon glyphicon-eye-open'></span>";
					echo "</a>";
					?>
					<!-- View staff assignment Modal -->
                    <div class="modal fade" 
                        id="<?php echo "{$assignmentID}"; ?>" 
                        tabindex="-1" 
                        role="dialog" 
                        aria-labelledby="viewAssignmentModalLabel<?php echo "{$assignmentID}"; ?>" 
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title" id="viewAssignmentModalLabel<?php echo "{$assignmentID}"; ?>">
                                        View Staff Assignment for <h3><?php echo "{$c_classname}"; ?></h3>
                                    </h4>
                                </div>                  
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <label for="Name">Full Name</label>
                                            <br />
                                            <?php echo "{$m_fname} {$m_lname}"; ?>
                                            <br />
                                            <br />
                                            <?php if($m_phone != null) { ?>
                                            <label for="Phone">Primary Phone</label>
                                            <br />
                                            <?php echo "{$m_phone}"; ?>
                                            <br />
                                            <br />
                                            <?php } ?>
                                            <?php if($m_phcell != null) { ?>
                                            <label for="Cell Phone">Cell Phone</label>
                                            <br />
                                            <?php echo "{$m_phcell}"; ?>
                                            <br />
                                            <br />
                                            <?php } ?>
                                            <?php if($m_phwork != null) { ?>
                                            <label for="Work Phone">Work Phone</label>
                                            <br />
                                            <?php echo "{$m_phwork}"; ?>
                                            <br />
                                            <br />
                                            <?php } ?>
                                            <?php if($m_email != null) { ?>
                                            <label for="Email">Email</label>
                                            <br />
                                            <?php echo "{$m_email}"; ?>
                                            <br />
                                            <br />
                                            <?php } ?>
                                        </div>
									</form>              
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

					<?php
					// delete class button
					echo "<a delete-id='{$assignmentID}' delete-file='includes/staffassignment_delete.inc.php' class='btn btn-sm btn-danger delete-object'>";
						echo "<span class='glyphicon glyphicon-trash'></span>";
					echo "</a>";
					
				echo "</td>";
				
			echo "</tr>";
		}
	echo "</table>";
	
	// needed for paging
	$total_rows=0;
	
	if($page_url=="view_classes.php?"){
		$total_rows=$class->countAll();
	}
	
	else if(isset($category_id) && $page_url=="category.php?id={$category_id}&"){
		$total_rows=$class->countAll_ByCategory();
	}
	
	else if(isset($search_term) && $page_url=="search_classes.php?s={$search_term}&"){
		$total_rows=$class->countAll_BySearch($search_term);
	}
	
	else if(isset($field) && isset($order) && $page_url=="read_products_sorted_by_fields.php?field={$field}&order={$order}&"){
		$total_rows=$class->countAll();
	}
	
	// search by date range
	else if(isset($date_from) && isset($date_to) 
				&& $page_url=="search_products_by_date_range.php?date_from={$date_from}&date_to={$date_to}&"){
		$total_rows=$class->countSearchByDateRange($date_from, $date_to);	
	}
	
	// paging buttons
	include_once 'helpers/paging.php';
}

// tell the user there are no classes
else{
	echo "<div class=\"alert alert-danger alert-dismissable\">";
		echo "No staff assignments found for the selected staff position in this department.";
	echo "</div>";
}
?>
</div>