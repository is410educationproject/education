<?php
// check for term
//term_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view term button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_terms.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View terms";
//	echo "</a>";
//echo "</div>";

// get ID of the term to be edited
$termID = isset($_GET['termID']) ? $_GET['termID'] : die('ERROR: missing ID.');

// set ID property of time to be edited
$term->termID = $termID;

// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['termDesc'])){ 
			echo "<div class='alert alert-danger'>The term description cannot be empty.</div>";
		}
		
		else{
			
			// set time property values
			$term->termDesc = $_POST['termDesc'];

			// update the famil yposition
			if($term->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The term was updated. <a href='view_terms.php'>Return to View Terms</a>";
				echo "</div>";
			}
			
			// if unable to update the term, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the term.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$termDesc = $_POST['termDesc'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of term to be edited
	$term->readOne();
}
?>
<div class="row no-margin-bottom">	
<!-- HTML form for updating a time -->
<form action='update_term.php?termID=<?php echo $termID; ?>' method='post'>
 
	<table class='table table-hover table-responsive table-bordered'>
	 
		<tr>
			<td>term Description</td>
			<td><input type='text' name='termDesc' value="<?php echo htmlspecialchars($term->termDesc, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update term
				</button>
                 <a href='view_terms.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
		 
	</table>
</form>
</div>