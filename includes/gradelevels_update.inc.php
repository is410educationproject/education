<?php
// view gradelevels button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_gradelevels.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View gradelevels";
//	echo "</a>";
//echo "</div>";

// get ID of the gradelevel to be edited
$gradelevelID = isset($_GET['gradelevelID']) ? $_GET['gradelevelID'] : die('ERROR: missing ID.');

// set ID property of gradelevel to be edited
$gradelevel->gradelevelID = $gradelevelID;

// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['gradelevelDesc'])){ 
			echo "<div class='alert alert-danger'>The gradelevel description cannot be empty.</div>";
		}
		
		else{
			
			// set gradelevel property values
			$gradelevel->gradelevelDesc = $_POST['gradelevelDesc'];

			// update the gradelevel
			if($gradelevel->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The grade level was updated. <a href='view_gradelevels.php'>Return to View grade levels</a>";
				echo "</div>";
			}
			
			// if unable to update the gradelevel, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the grade level.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$gradelevelDesc = $_POST['gradelevelDesc'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of gradelevel to be edited
	$gradelevel->readOne();
}
?>
<div class="row no-margin-bottom">
<!-- HTML form for updating a time -->
<form action='update_gradelevel.php?gradelevelID=<?php echo $gradelevelID; ?>' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Grade Level description</td>
			<td><input type='text' name='gradelevelDesc' value="<?php echo htmlspecialchars($gradelevel->gradelevelDesc, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required
            ></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update grade level
				</button>
                 <a href='view_gradelevels.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr> 
	</table>
</form>
</div>