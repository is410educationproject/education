<?php
// check for session
//session_start();
/*include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}*/
// view contact methods button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_contactmethods.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View contact methods";
//	echo "</a>";
//echo "</div>";
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['methodDesc'])){ 
			echo "<div class='alert alert-danger'>Please enter a contact method.</div>";
		}
		
		else{
			// set contact method property values
			$contactmethod->methodDesc = $_POST['methodDesc'];
			
			// add the contact method
			if($contactmethod->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The contact method was added. <a href='view_contactmethods.php'>Return to View Contact Methods</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the contact method, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\"&times;</button>";
					echo "Unable to add the contact method.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
<div class="row no-margin-bottom"> 
<!-- HTML form for adding a contactmethod -->
<form action='add_contactmethod.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Contact method description</td>
			<td><input type='text' name='methodDesc' class='form-control' required value="<?php echo isset($_POST['methodDesc']) ? htmlspecialchars($_POST['methodDesc'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add contact method
				</button>
                 <a href='view_contactmethods.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>