<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
?>
<div class='row'>

	
	<!-- add time button -->
	<a href='add_time.php' class="btn btn-primary pull-right">
		<span class="glyphicon glyphicon-plus"></span> Add time
	</a>
	
	
</div>
<div class="row no-margin-bottom">
<?php 
// display the times if there are any
if($num>0){
	
	echo "<table class='table table-hover table-responsive table-bordered'>";
		echo "<tr>";
			echo "<th style='width:20%;'>";
					echo "Class Time";
			echo "</th>";
			echo "<th>Actions</th>";
		echo "</tr>";
		
		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		
			extract($row);
			
			echo "<tr>";
				echo "<td>{$timeDesc}</td>";
				echo "<td>";
					
					// edit time button
					echo "<a href='update_time.php?timeID={$timeID}' class='btn btn-sm btn-info right-margin'>";
						echo "<span class='glyphicon glyphicon-pencil'></span>";
					echo "</a>";
					
					// delete time button
					echo "<a delete-id='{$timeID}' delete-file='includes/times_delete.inc.php' class='btn btn-sm btn-danger delete-object'>";
						echo "<span class='glyphicon glyphicon-trash'></span>";
					echo "</a>";
					
				echo "</td>";
				
			echo "</tr>";
			
		}
		
	echo "</table>";

	
}

// tell the user there are no times
else{
	echo "<div class=\"alert alert-danger alert-dismissable\">";
		echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
		echo "No times found.";
	echo "</div>";
}
?>
</div>