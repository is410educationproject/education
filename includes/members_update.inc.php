<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
echo "<div class='row add-margin-top'>";
// view members button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='view_members.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> View Members";
	echo "</a>";
echo "</div>";
echo "</div>";


// get ID of the member to be edited
$memberID = isset($_GET['memberID']) ? $_GET['memberID'] : die('ERROR: missing ID.');

// set ID property of member to be edited
$member->memberID = $memberID;

echo "<div class='row no-margin-bottom'>";
// if the form was submitted
if($_POST){

	try{		
		// server-side data validation
		if(empty($_POST['fname'])){ 
			echo "<div class='alert alert-danger'>First Name cannot be empty.</div>";
		}
		
		else if(empty($_POST['lname'])){ 
			echo "<div class='alert alert-danger'>Last Name cannot be empty.</div>";
		}
		
		else if($_POST['familypositionID']==0){
			echo "<div class='alert alert-danger'>Please select a family position.</div>";
		}
		
		else{
			
			$isTeacher = 0;
			$isSub = 0;
			$isHelper = 0;
			if(isset($_POST['isTeacher'])) {
				$isTeacher = $_POST['isTeacher'];
			}
			if(isset($_POST['isSub'])) {
				$isSub = $_POST['isSub'];
			}
			if(isset($_POST['isHelper'])) {
				$isHelper = $_POST['isHelper'];
			}
			
			// set member property values
			$member->fname = $_POST['fname'];
			$member->lname = $_POST['lname'];
			$member->phone = $_POST['phone'];
			$member->phcell = $_POST['phcell'];
			$member->phwork = $_POST['phwork'];
			$member->email = $_POST['email'];
			$member->familypositionID = $_POST['familypositionID'];
			$member->isTeacher = $isTeacher;
			$member->isSub = $isSub;
			$member->isHelper = $isHelper;
			
			// update the member
			if($member->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "<strong>$member->fname $member->lname</strong> was updated. <a href='view_members.php'>Return to View Members</a>";
					
				echo "</div>";
			}
			
			// if unable to update the member, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update <strong>$member->fname $member->lname</strong>.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$fname = $_POST['fname'];
		$lname = $_POST['lname'];
		$phone = $_POST['phone'];
		$phcell = $_POST['phcell'];
		$phwork = $_POST['phwork'];
		$email = $_POST['email'];
		$familypositionID = $_POST['familypositionID'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
	// read the details of member that was just edited
	$member->readOne();
}

else{
	// read the details of member to be edited
	$member->readOne();
}

?>
	
<!-- HTML form for updating a member -->
<form action='update_member.php?memberID=<?php echo $memberID; ?>' method='post'>
	<table class='table table-hover table-responsive table-bordered no-margin-bottom'>
    	<tr>
			<td>First Name</td>
			<td><input type='text' name='fname' value="<?php echo htmlspecialchars($member->fname, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
        <tr>
			<td>Last Name</td>
			<td><input type='text' name='lname' value="<?php echo htmlspecialchars($member->lname, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
        <tr>
			<td>Primary Phone</td>
			<td><input type='tel' name='phone' value="<?php echo htmlspecialchars($member->phone, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
        <tr>
			<td>Cell Phone</td>
			<td><input type='tel' name='phcell' value="<?php echo htmlspecialchars($member->phcell, ENT_QUOTES, 'UTF-8'); ?>" class='form-control'></td>
		</tr>
        <tr>
			<td>Work Phone</td>
			<td><input type='tel' name='phwork' value="<?php echo htmlspecialchars($member->phwork, ENT_QUOTES, 'UTF-8'); ?>" class='form-control'></td>
		</tr>
        <tr>
			<td>Email</td>
			<td><input type='email' name='email' value="<?php echo htmlspecialchars($member->email, ENT_QUOTES, 'UTF-8'); ?>" class='form-control'></td>
		</tr>
        <tr>
			<td>Family Position</td>
			<td><?php
            $stmt = $familyposition->read();
            
            // put them in a select drop-down
            echo "<select class='form-control' name='familypositionID'>";
            
                echo "<option>Please select...</option>";
                while ($row_familyposition = $stmt->fetch(PDO::FETCH_ASSOC)){
                    extract($row_familyposition);
                    
                    // current family position of the member must be selected
                    if($member->familypositionID==$familypositionID){
                        echo "<option value='$familypositionID' selected>";
                    }else{
                        echo "<option value='$familypositionID'>";
                    }
                    
                    echo "$familypositiondesc</option>";
                }
            echo "</select>";
            ?></td>
		</tr>
        <tr>
        	<td>Teacher?</td>
            <td>
				<input data-label-width='50px' data-handle-width='50px' data-on='Yes' data-off='No' type="checkbox" data-toggle="toggle" name="isTeacher" value=1 <?php if($member->isTeacher == 1){ echo "checked";}?> />
			</td>
        </tr>
        <tr>
        	<td>Sub?</td>
            <td>
				<input data-label-width='50px' data-handle-width='50px' data-on='Yes' data-off='No' type="checkbox" data-toggle="toggle" name="isSub" value=1 <?php if($member->isSub == 1){ echo "checked";}?> />
			</td>
        </tr>
        <tr>
        	<td>Helper?</td>
            <td>
				<input data-label-width='50px' data-handle-width='50px' data-on='Yes' data-off='No' type="checkbox" data-toggle="toggle" name="isHelper" value=1 <?php if($member->isHelper == 1){ echo "checked";}?> />
			</td>
        </tr>
        
        <tr>
        	<td></td>
            <td>
            	<button type="submit" class="btn btn-primary add-margin-right">
                	<span class='glyphicon glyphicon-edit'></span> Update
            	</button>
            	<a href='view_members.php' class="btn btn-danger" >
					<span class='glyphicon glyphicon-remove'></span> Cancel
				</a>
            </td>
        </tr>
    </table>
</form>
<?php echo "</div>"; ?>