<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view departments button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_departments.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View Departments";
//	echo "</a>";
//echo "</div>";

// get ID of the department to be edited
$departmentID = isset($_GET['departmentID']) ? $_GET['departmentID'] : die('ERROR: missing ID.');

// set ID property of department to be edited
$department->departmentID = $departmentID;

echo "<div class='row no-margin-bottom'>";
// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['departmentdesc'])){ 
			echo "<div class='alert alert-danger'>The department description cannot be empty.</div>";
		}
		
		else{
			
			// set department property values
			$department->departmentdesc = $_POST['departmentdesc'];

			// update the department
			if($department->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The department was updated. <a href='view_departments.php'>Return to View Departments</a>";
				echo "</div>";
			}
			
			// if unable to update the department, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the department.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$departmentdesc = $_POST['departmentdesc'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of department to be edited
	$department->readOne();
}
?>
	
<!-- HTML form for updating a department -->
<form action='update_department.php?departmentID=<?php echo $departmentID; ?>' method='post'>
 
	<table class="table table-hover table-responsive table-bordered no-margin-bottom">
	 
		<tr>
			<td>Department Description</td>
			<td><input type='text' name='departmentdesc' value="<?php echo htmlspecialchars($department->departmentdesc, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update department
				</button>
                 <a href='view_departments.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
				</a>
			</td>
		</tr>
		 
	</table>
</form>
</div>