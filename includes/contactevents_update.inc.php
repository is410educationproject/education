<?php
// view family positions button
echo "<div class='row add-margin-top overflow-hidden'>";
	echo "<a href='view_member_info.php?memberID={$memberID}' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> Back to Member Profile";
	echo "</a>";
echo "</div>";

// get ID of the family position to be edited
$contacteventID = isset($_GET['contacteventID']) ? $_GET['contacteventID'] : die('ERROR: missing ID.');

// set ID property of family position to be edited
$contact->contacteventID = $contacteventID;

// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['contactdate'])){ 
			echo "<div class='alert alert-danger'>Please select a date of contact.</div>";
		}
		
		else if($_POST['methodID']==0){ 
			echo "<div class='alert alert-danger'>Please select a method of contact from the dropdown.</div>";
		}
		
		/*else if(empty($_POST['notes'])){
			echo "<div class='alert alert-danger'>Price cannot be empty.</div>";
		}*/
		
		/*else if(!is_numeric($_POST['price'])){
			echo "<div class='alert alert-danger'>Price must be a number.</div>";
		}*/
		
		else if($_POST['statusID']==0){
			echo "<div class='alert alert-danger'>Please select a status from the dropdown.</div>";
		}
		
		else{
			
			// set contact event property values
			$contact->memberID = $_POST['memberID'];
			$contact->contactdate = $_POST['contactdate'];
			$contact->methodID = $_POST['methodID'];
			$contact->notes = $_POST['notes'];
			$contact->statusID = $_POST['statusID'];

			// update the contact event
			if($contact->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The contact event was updated.";
				echo "</div>";
			}
			
			// if unable to update the contact event, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the contact event.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$memberID = $_POST['memberID'];
		$contactdate = $_POST['contactdate'];
		$methodID = $_POST['methodID'];
		$notes = $_POST['notes'];
		$statusID = $_POST['statusID'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of family position to be edited
	$contact->readOne();
}
?>
<div class="row no-margin-bottom">
<!-- HTML form for updating a family position -->
<form action='update_contactevent.php?memberID=<?php echo $memberID; ?>&contacteventID=<?php echo $contacteventID; ?>' method='post'>
 
	<table class='table table-hover table-responsive table-bordered no-margin-bottom'>
	 
     <input type='hidden' name='memberID' class='form-control' value="<?php echo "$contact->memberID"; ?>" />
        
        
        <tr>
			<td>Contact Date</td>
			<td><input type='date' name='contactdate' placeholder='<?php echo $contact->contactdate ?>' class='form-control' required value="<?php echo htmlspecialchars($contact->contactdate, ENT_QUOTES, 'UTF-8'); ?>" /></td>
		</tr>
		 
		<tr>
			<td>Method</td>
			<td>
				<?php
				$stmt = $method->read();
				
				// put them in a select drop-down
				echo "<select class='form-control' name='methodID'>";
				
					echo "<option>Please select...</option>";
					while ($row_method = $stmt->fetch(PDO::FETCH_ASSOC)){
						extract($row_method);
						
						// current status of the contactevent must be selected
						if($contact->methodID==$methodID){
							echo "<option value='$methodID' selected>";
						}else{
							echo "<option value='$methodID'>";
						}
						
						echo "$methodDesc</option>";
					}
				echo "</select>";
				?>
				
				
				<?php
                // read the family positions from the database
                /*include_once '../phpoopcrud/includes/objects/contactmethod.php';
    
                $contactmethod = new ContactMethod($db);
                $stmt = $contactmethod->read();
                
                // put them in a select drop-down
                echo "<select class='form-control' name='methodID'>";
                    echo "<option>Select contact method...</option>";
                    
                    while ($row_contactmethod = $stmt->fetch(PDO::FETCH_ASSOC)){
                        extract($row_contactmethod);
                        echo "<option value='{$methodID}'>{$methodDesc}</option>";
                    }
                    
                echo "</select>";*/
                ?>
			</td>
		</tr>
		 
		<tr>
			<td>Notes</td>
			<td><textarea name='notes' class='form-control'><?php echo htmlspecialchars($contact->notes, ENT_QUOTES, 'UTF-8'); ?></textarea></td>
		</tr>
		 
		<tr>
			<td>Status</td>
			<td>
				<?php
				$stmt = $status->read();
				
				// put them in a select drop-down
				echo "<select class='form-control' name='statusID'>";
				
					echo "<option>Please select...</option>";
					while ($row_status = $stmt->fetch(PDO::FETCH_ASSOC)){
						extract($row_status);
						
						// current status of the contactevent must be selected
						if($contact->statusID==$statusID){
							echo "<option value='$statusID' selected>";
						}else{
							echo "<option value='$statusID'>";
						}
						
						echo "$statusDesc</option>";
					}
				echo "</select>";
				?>
			</td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-edit"></span> Update Contact Event
				</button>
                   <a href='view_member_info.php?memberID=<?php echo "{$memberID}"; ?>' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>