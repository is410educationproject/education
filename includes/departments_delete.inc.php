<?php
// check for session
session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/department.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare department.php object
	$department = new Department($db);
	
	// set departmentID to be deleted
	$department->departmentID = $_POST['object_id'];
	
	// delete the department
	if($department->delete()){
		echo "The department was deleted.";
	}
	
	// if unable to delete the department
	else{
		echo "Unable to delete the department.";	
	}
}
?>