<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// check if value was posted
if($_POST){

	// include database and object file
	include_once 'db_connect.php';
	include_once '../objects/staffposition.php';

	// get database connection
	$database = new Database();
	$db = $database->getConnection();

	// prepare staffposition.php object
	$position = new StaffPosition($db);
	
	// set staff positionID to be deleted
	$position->positionID = $_POST['object_id'];
	
	// delete the status
	if($position->delete()){
		echo "The staff position was deleted.";
	}
	
	// if unable to delete the staff position
	else{
		echo "Unable to delete the staff position.";	
	}
}
?>