<?php
// check for session
//session_start();
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
// view classes button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_classes.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View Classes";
//	echo "</a>";
//echo "</div>";

$gradelevel->activeuserID= $activeuserID;
$time->activeuserID= $activeuserID;
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['classname'])){ 
			echo "<div class='alert alert-danger'>Class name cannot be empty.</div>";
		}
		
		else if($_POST['gradelevelID']==0){ 
			echo "<div class='alert alert-danger'>Grade level cannot be empty.</div>";
		}
		
		else if(empty($_POST['room'])){
			echo "<div class='alert alert-danger'>Room number cannot be empty.</div>";
		}
		
		/*else if(!is_numeric($_POST['price'])){
			echo "<div class='alert alert-danger'>Price must be a number.</div>";
		}*/
		
		else if($_POST['timeID']==0){
			echo "<div class='alert alert-danger'>Please select a class time.</div>";
		}
				
		else if(!isset($departmentID)){
			echo "<div class='alert alert-danger'>A department was not set.</div>";
		}
		
		else if(!isset($userobject->activetermID)){
			echo "<div class='alert alert-danger'>A term was not set.</div>";
		}
		
		else{
			// set class property values
			$class->classname = $_POST['classname'];
			$class->gradelevelID = $_POST['gradelevelID'];
			$class->room = $_POST['room'];
			$class->timeID = $_POST['timeID'];
			$class->departmentID = $departmentID;
			$class->termID = $userobject->activetermID;
			
			// add the class
			if($class->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Class was added. <a href='view_department_info.php?departmentID={$departmentID}'>Return to View Department</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the class, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add class.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
	 
<!-- HTML form for adding a class -->
<form action='add_class.php?departmentID=<?php echo "{$departmentID}"; ?>' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Class name</td>
			<td><input type='text' name='classname' class='form-control' required value="<?php echo isset($_POST['classname']) ? htmlspecialchars($_POST['classname'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td>Grade level</td>
            <td>
			<?php
			// read the grade levels from the database
			$stmt = $gradelevel->read();
			
			// put them in a select drop-down
			echo "<select class='form-control' name='gradelevelID'>";
				echo "<option>Select grade level...</option>";
				
				while ($row_gradelevel= $stmt->fetch(PDO::FETCH_ASSOC)){
					extract($row_gradelevel);
					echo "<option value='{$gradelevelID}'>{$gradelevelDesc}</option>";
				}
			echo "</select>";
			?>
			</td>
		</tr>
		 
		<tr>
			<td>Room Number</td>
			<td><input type='text' name='room' class='form-control' required value="<?php echo isset($_POST['room']) ? htmlspecialchars($_POST['room'], ENT_QUOTES) : "";  ?>" /></td>
		</tr>
		 
		<tr>
			<td>Class time</td>
			<td>
			<?php
			// read the class times from the database
			$stmt = $time->read();
			
			// put them in a select drop-down
			echo "<select class='form-control' name='timeID'>";
				echo "<option>Select class time...</option>";
				
				while ($row_time = $stmt->fetch(PDO::FETCH_ASSOC)){
					extract($row_time);
					echo "<option value='{$timeID}'>{$timeDesc}</option>";
				}
				
			echo "</select>";
			?>
			</td>
		</tr>
        		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add Class
				</button>
                 <a href='view_department_info.php?departmentID=<?php echo "{$departmentID}"; ?>' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>