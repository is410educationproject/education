<?php
	//$documentation->readDocumentation($page_url);
?>

<!DOCTYPE html>
<html lang="en">
<head>
 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
     
    <title><?php echo $page_title; ?></title>
	
    <!-- Bootstrap CSS -->
	<link href="css/bootstrap.css" rel="stylesheet" media="screen" />
    <link rel="stylesheet" href="css/bootstrap-switch.css" />
	<!-- jquery ui css -->
	<link rel="stylesheet" href="css/jquery-ui.min.css" />        
	<!-- some custom CSS -->
    <link rel="stylesheet" href="css/custom.css" />
	<style>
	.padding-bottom-2em{
		padding-bottom:2em;
	}
	
	.width-30-pct{
		width:30%;
	}
	
	.width-40-pct{
		width:40%;
	}
	
	.overflow-hidden{
		overflow:hidden;
	}
	
	.margin-right-1em{
		margin-right:1em;
	}
	
	.margin-left-1em{
		margin-left:1em;
	}
	
	.right-margin{
		margin:0 .5em 0 0;
	}
	
	.margin-bottom-1em {
		margin-bottom:1em;
	}
	
	.margin-zero{
		margin:0;
	}
	
	.text-align-center{
		text-align:center;
	}
	</style>
</head>
<body>

	<?php //include_once "../phpoopcrud/includes/navigation.php"; ?>
	
    <!-- container -->
    <div class="container outercontainer">
			<div class="bottom-line" style="margin-bottom: 15px;">
                <div class="col-xs-8 col-sm-8 col-md-9 col-lg-9 no-padding">
                    <!-- show page header-->
                    <div class='page-header'>
                        <h1><?php echo $page_title . "</h1>"; ?>
                        <?php if(!isset($globalpage)){ ?>
                            <h4 style="display:inline; vertical-align:middle;"><?php echo $userobject->term_termDesc; ?> </h4>
                            <?php if($termselector==true){ ?>
                                <h5 style="display:inline"><a data-toggle='modal' data-target='#termmodal' class='btn btn-xs btn-primary'>Change</a></h5>
                            <?php }
                                elseif($termselector==false){ ?>
                                <h5 style="display:inline"><button type='button' class='btn btn-xs btn-primary' disabled='disabled'>Change</button></h5>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-3 no-padding">
                <div class='page-header-buttons'>
                    <a data-toggle='modal' data-target='#helpmodal' class='btn btn-primary pull-right no-margin-bottom margin-left-1em visible-xs hidden-sm hidden-md hidden-lg'
                    >
                        <span class='glyphicon glyphicon-question-sign'></span></a>
                    <a data-toggle='modal' data-target='#helpmodal' class='btn btn-primary pull-right no-margin-bottom margin-left-1em hidden-xs visible-sm visible-md 
                    visible-lg'>
                        <span class='glyphicon glyphicon-question-sign'></span> Get Help</a>
                    <a href='parameters.php' class='btn btn-primary pull-right no-margin-bottom margin-left-1em'>
                        <span class='glyphicon glyphicon-home'></span></a>
                </div>
            </div><br>&nbsp;
        </div>

        <div class="modal fade" 
                        id="helpmodal" 
                        tabindex="-1" 
                        role="dialog" 
                        aria-labelledby="helpmodalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 class="modal-title">
                                        Need help with <?php echo "{$page_title}";?>?
                                    </h4>
                                </div>                  
                                <div class="modal-body">
                                    <form>
                                        <div class="form-group">
                                            <?php echo $documentation->readDocumentation($page_url); ?>
                                        </div>
									</form>
                                </div>             
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
        <?php if(isset($termselector) && !isset($globalpage)){ ?>
        <div class="modal fade" 
            id="termmodal" 
            tabindex="-1" 
            role="dialog" 
            aria-labelledby="termmodal"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">
                            Change the active term
                        </h4>
                    </div>                  
                    <div class="modal-body">
                        <form action='' method='post' class='form-inline'>
                            <div class='form-group'>
                                <?php
                                $stmt = $term->read();
                                echo "<label for='termID'>Active term&nbsp;&nbsp;&nbsp;</label>";
                                // put them in a select drop-down
                                echo "<select class='form-control' name='termID'>";
                                
                                    echo "<option>Please select...</option>";
                                    while ($row_term = $stmt->fetch(PDO::FETCH_ASSOC)){
                                        extract($row_term);
                                        
                                        // current session of the logged in user must be selected
                                        if($userobject->activetermID==$termID){
                                            echo "<option value='$termID' selected>";
                                        }else{
                                            echo "<option value='$termID'>";
                                        }
                                        
                                        echo "$termDesc</option>";
                                    }
                                echo "</select>";
                                ?>
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>                