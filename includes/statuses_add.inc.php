<?php
// view statuses button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_statuses.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View statuses";
//	echo "</a>";
//echo "</div>";
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['statusDesc'])){ 
			echo "<div class='alert alert-danger'>Please enter a status.</div>";
		}
		
		else{
			// set status property values
			$status->statusDesc = $_POST['statusDesc'];
			
			// add the status
			if($status->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The class status was added. <a href='view_statuses.php'>Return to View Statuses</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			
			// if unable to add the status, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add the class status.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
<div class="row no-margin-bottom">
<!-- HTML form for adding a time -->
<form action='add_status.php' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Status Description</td>
			<td><input type='text' name='statusDesc' class='form-control' required value="<?php echo isset($_POST['statusDesc']) ? htmlspecialchars($_POST['statusDesc'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add status
				</button>
                 <a href='view_statuses.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>