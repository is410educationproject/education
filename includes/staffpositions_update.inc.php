<?php
// view statuses button
//echo "<div class='margin-bottom-1em overflow-hidden'>";
//	echo "<a href='view_statuses.php' class='btn btn-primary pull-right'>";
//		echo "<span class='glyphicon glyphicon-list'></span> View statuses";
//	echo "</a>";
//echo "</div>";

// get ID of the staff position to be edited
$positionID = isset($_GET['positionID']) ? $_GET['positionID'] : die('ERROR: missing ID.');

// set ID property of staff position to be edited
$position->positionID = $positionID;

// if the form was submitted
if($_POST){

	try{
		// server-side data validation
		if(empty($_POST['positionDesc'])){ 
			echo "<div class='alert alert-danger'>The staff position description cannot be empty.</div>";
		}
		
		else{
			
			// set staff position property values
			$position->positionDesc = $_POST['positionDesc'];

			// update the staff position
			if($position->update()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "The staff position was updated. <a href='view_staffpositions.php'>Return to View Staff Positions</a>";
				echo "</div>";
			}
			
			// if unable to update the staff position, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to update the staff position.";
				echo "</div>";
			}
		}
		
		// values to fill up our form
		$positionDesc = $_POST['positionDesc'];
	}
	
	// show errors, if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}

else{
	// read the details of staff position to be edited
	$position->readOne();
}
?>
<div class="row no-margin-bottom">
<!-- HTML form for updating a staff position -->
<form action='update_staffposition.php?positionID=<?php echo $positionID; ?>' method='post'>
	<table class='table table-hover table-responsive table-bordered'>
		<tr>
			<td>Description</td>
			<td><input type='text' name='positionDesc' value="<?php echo htmlspecialchars($position->positionDesc, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' required></td>
		</tr>
		 
		<tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class='glyphicon glyphicon-edit'></span> Update staff position
				</button>
                 <a href='view_staffpositions.php' class='btn btn-danger'>
				<span class='glyphicon glyphicon-remove'></span> Cancel
			</a>
			</td>
		</tr>
	</table>
</form>
</div>