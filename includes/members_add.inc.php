<?php
// check for session
include_once 'login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:../login.php");
}
echo "<div class='row add-margin-top'>";
// view members button
echo "<div class='margin-bottom-1em overflow-hidden'>";
	echo "<a href='view_members.php' class='btn btn-primary pull-right'>";
		echo "<span class='glyphicon glyphicon-list'></span> View Members";
	echo "</a>";
echo "</div>";
echo "</div>";

echo "<div class='row no-margin-bottom'>";
	
// if the form was submitted
if($_POST){

	try{
		// data validation
		if(empty($_POST['fname'])){ 
			echo "<div class='alert alert-danger'>Name cannot be empty.</div>";
		}
		
		else if(empty($_POST['lname'])){ 
			echo "<div class='alert alert-danger'>Description cannot be empty.</div>";
		}
		
		else if(empty($_POST['phone'])){
			echo "<div class='alert alert-danger'>Price cannot be empty.</div>";
		}
		
		else if($_POST['familypositionID']==0){
			echo "<div class='alert alert-danger'>Please select family position</div>";
		}
		
		else{
			$isTeacher = 0;
			$isSub = 0;
			$isHelper = 0;
			if(isset($_POST['isTeacher'])) {
				$isTeacher = $_POST['isTeacher'];
			}
			if(isset($_POST['isSub'])) {
				$isSub = $_POST['isSub'];
			}
			if(isset($_POST['isHelper'])) {
				$isHelper = $_POST['isHelper'];
			}
			
			// set member property values
			$member->fname = $_POST['fname'];
			$member->lname = $_POST['lname'];
			$member->phone = $_POST['phone'];
			$member->phcell = $_POST['phcell'];
			$member->phwork = $_POST['phwork'];
			$member->email = $_POST['email'];
			$member->familypositionID = $_POST['familypositionID'];
			$member->isTeacher = $isTeacher;
			$member->isSub = $isSub;
			$member->isHelper = $isHelper;
			
			// add the member
			if($member->add()){
				echo "<div class=\"alert alert-success alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "<span class='glyphicon glyphicon-thumbs-up'></span> Member was added. <a href='view_members.php'>Return to View Members</a>";
				echo "</div>";
				
				// empty post array
				$_POST=array();
			}
			// if unable to add the member, tell the user
			else{
				echo "<div class=\"alert alert-danger alert-dismissable\">";
					echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
					echo "Unable to add member.";
				echo "</div>";
			}
		}
	}
	
	// show error if any
	catch(PDOException $exception){
		die('ERROR: ' . $exception->getMessage());
	}
}
?>
	 
<!-- HTML form for adding a member -->
<form action='add_member.php' method='post'>
	<table class='table table-hover table-responsive table-bordered no-margin-bottom'>
		<tr>
			<td>First Name</td>
			<td><input type='text' name='fname' class='form-control' required value="<?php echo isset($_POST['fname']) ? htmlspecialchars($_POST['fname'], ENT_QUOTES) : "";  ?>"></td>
		</tr>
		 
		<tr>
			<td>Last Name</td>
			<td><input type='text' name='lname' class='form-control' required value="<?php echo isset($_POST['lname']) ? htmlspecialchars($_POST['lname'], ENT_QUOTES) : "";  ?>" /></td>
		</tr>
		 
		<tr>
			<td>Phone Number</td>
			<td><input type='text' name='phone' class='form-control' value="<?php echo isset($_POST['phone']) ? htmlspecialchars($_POST['phone'], ENT_QUOTES) : "";  ?>" /></td>
		</tr>
        
        <tr>
			<td>Cell Number</td>
			<td><input type='text' name='phcell' value="<?php echo htmlspecialchars($member->phcell, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' /></td>
		</tr>
        
        <tr>
			<td>Work Number</td>
			<td><input type='text' name='phwork' value="<?php echo htmlspecialchars($member->phwork, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' /></td>
		</tr>
        
        <tr>
			<td>Email</td>
			<td><input type='text' name='email' value="<?php echo htmlspecialchars($member->email, ENT_QUOTES, 'UTF-8'); ?>" class='form-control' /></td>
		</tr>
		 
		<tr>
			<td>Family Position</td>
			<td>
			<?php
			// read the family positions from the database
			include_once 'objects/familyposition.php';

			$familyposition = new FamilyPosition($db);
			$stmt = $familyposition->read();
			
			// put them in a select drop-down
			echo "<select class='form-control' name='familypositionID'>";
				echo "<option>Select family position...</option>";
				
				while ($row_familyposition = $stmt->fetch(PDO::FETCH_ASSOC)){
					extract($row_familyposition);
					echo "<option value='{$familypositionID}'>{$familypositiondesc}</option>";
				}
				
			echo "</select>";
			?>
			</td>
		</tr>
		
        <tr>
        	<td>Teacher?</td>
        	<td>
       			<!-- Bootstrap toggle can be found at www.bootstraptoggle.com -->
        		<input data-label-width='50px' data-handle-width='50px' data-on='YES' data-off='NO' type="checkbox" data-toggle="toggle" name="isTeacher" value=1 <?php if(
				$member->isTeacher == 1){ echo "checked";}?> />
			</td>
        </tr>
        
        <tr>
        	<td>Substitute?</td>
        	<td>
        		<input data-label-width='50px' data-handle-width='50px' data-on='YES' data-off='NO' type="checkbox" data-toggle="toggle" name="isSub" value=1 <?php if($member->
				isSub == 1){ echo "checked"; } ?> />
			</td>
        </tr>
        
        <tr>
        	<td>Helper?</td>
        	<td>
				<input data-label-width='50px' data-handle-width='50px' data-on='YES' data-off='NO' type="checkbox" data-toggle="toggle" name="isHelper" value=1 <?php if(
				$member->isHelper == 1){ echo "checked"; } ?> />
			</td>
        </tr>
        
        <tr>
			<td></td>
			<td>
				<button type="submit" class="btn btn-primary">
					<span class="glyphicon glyphicon-plus"></span> Add Member
				</button>
                
                <a href='view_members.php' class='btn btn-danger'>
					<span class='glyphicon glyphicon-remove'></span> Cancel
				</a>
			</td>
		</tr>
	</table>
</form>
<?php echo "</div>"; ?>