<?php
// check for session
session_start();
include_once 'includes/login/class.user.php';
$user = new User();
if (!$user->logged_in()){
	header("location:login.php");
}

// set resources
$pagination = false;
$termselector = false;
$globalpage = true;
$page_url = 'add_status.php?';

// set active userID in a session cookie
$activeuserID = $_SESSION['activeuserID'];

// get database connection
include_once 'helpers/config.php';
include_once 'includes/db_connect.php';
include_once 'objects/status.php';
include_once 'objects/documentation.php';
include_once 'objects/user.php';
include_once 'objects/term.php';

// get database connection
$database = new Database();
$db = $database->getConnection();

// instantiate status.php object
$status = new Status($db);
$userobject = new UserObject($db);
$term = new Term($db);

//get documentation 
$documentation = new Documentation($page_url);

// set page headers
$page_title = "Add Status";
include_once "includes/header_param.php";

include_once "includes/statuses_add.inc.php";

include_once "includes/footer.php";
?>