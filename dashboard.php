<?php
	session_start();
    include_once 'includes/login/class.user.php';
    $user = new User();

    $userID = $_SESSION['activeuserID'];

    if (!$user->get_session()){
       header("location:login.php");
    }

    if (isset($_GET['q'])){
        $user->user_logout();
        header("location:index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!--<meta charset="utf-8">-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="stylesheet" type="text/css" href="css/site.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/hover.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <!-- some custom CSS -->
    <style>
    /* Remove the jumbotron's default bottom margin + leather*/ 
     .jumbotron {
      margin-bottom: 0;
	  background-image:url("../live12-11/images/leather.jpg");
	  background-repeat: repeat;
	  font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
	  font-weight: 300;
	  color:white; 
    }
	
    body{
		font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
		font-weight: 300;
    	/*font-family:Arial, Helvetica, sans-serif;*/
    }
	
    h1{
		font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
		font-weight: 300;
    	/*font-family:'Georgia', Times New Roman, serif;*/
    }
	</style>
    <title> Education Dashboard </title>
</head>

<body>
<!-- Bootstrap Grid Version -->
<div class="jumbotron">
    <div class="container text-center">
        <h1><?php $user->get_firstname($userID); ?>, tap or click an icon to get started.</h1>      
        <p>When you are finished, tap or click the logout icon.</p>
    </div>
</div>
<div class="container">    
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 hovereffect">
        <div class="panel panel" align="center" onclick="location.href='registration.php';" style="cursor: pointer">
            <div class="panel-heading">Add Administrator</div>
            <div class="panel-body"><img src="images/grey-icons/add-user-128.png" alt="View Members"></div>
            <div class="panel-footer">Here you can add adminstrator accounts to manage the site.</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 hovereffect">
        <div class="panel panel" align="center" onclick="location.href='view_members.php';" style="cursor: pointer">
            <div class="panel-heading">View Members</div>
            <div class="panel-body"><img src="images/grey-icons/conference-128.png" alt="View Members"></div>
            <div class="panel-footer">View individual members, their profiles, and update those member's profiles.</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 hovereffect">
        <div class="panel panel" align="center" onclick="location.href='view_departments.php';" style="cursor: pointer">
            <div class="panel-heading">View Departments</div>
            <div class="panel-body"><img src="images/grey-icons/list-ingredients-128.png" alt="View Departments"></div>
            <div class="panel-footer">Here you can view a list of departments and make edits to them.</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-lg-offset-2 hovereffect">
        <div class="panel panel" align="center" onclick="location.href='parameters.php';" style="cursor: pointer">
            <div class="panel-heading">View Parameters</div>
            <div class="panel-body"><img src="images/grey-icons/services-128.png" alt="Parameters"></div>
            <div class="panel-footer">Here you can view various parameters to the site and make edits to specific ones.</div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 hovereffect">
        <div class="panel panel" align="center" onclick="location.href='dashboard.php?q=logout';" style="cursor: pointer">
            <div class="panel-heading">Logout</div>
            <div class="panel-body"><img src="images/grey-icons/logout-128.png" alt="Logout"></div>
            <div class="panel-footer">When you are finished, please click or tap here to logout of the site.</div>
        </div>
    </div>
</div><br><br>
</body>
</html>